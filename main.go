// Copyright: (c) 2023 Mathias Weber
//
// SPDX-License-Identifier: MIT

package main

import "gitlab.com/mathweb/gitlab-pipeline-analyzer/cmd"

func main() {
	cmd.Execute()
}
