// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package dataupdater

import (
	"time"

	"gitlab.com/mathweb/gitlab-pipeline-analyzer/data"
)

func UpdatePipelineData(p *data.PipelineDataCollection, days int, f dataFetcher) error {
	lastUpdate := time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC)
	if len(p.Pipelines.RawData) > 0 {
		lastUpdate = p.Pipelines.RawData[len(p.Pipelines.RawData)-1].StartedAt
	}
	rangeStart, rangeEnd := calculateDateRange(days, lastUpdate)
	if err := updateProjectData(&p.Project, f); err != nil {
		return err
	}

	filterPipelineIDs := []string{}
	if p.FilterScheduleID > 0 {
		ids, err := f.GetProjectRESTDataFetcher(p.Project).GetScheduledPipelineIds(p.FilterScheduleID, rangeStart, rangeEnd)
		if err != nil {
			return err
		}
		filterPipelineIDs = ids
	}

	pipelines, jobs, err := f.GetBuildEntries(p.Project.FullPath, p.Project.DefaultBranch, rangeEnd, rangeStart)
	if err != nil {
		return err
	}

	if len(filterPipelineIDs) > 0 {
		pipelines, jobs = filterToPipelineIDs(pipelines, jobs, filterPipelineIDs)
	}

	p.AddPipelineData(pipelines)
	p.AddJobData(jobs, f.GetProjectRESTDataFetcher(p.Project))

	p.ProcessAndCleanUpData()

	p.Updated = getUpdatedDate()
	return nil
}

func filterToPipelineIDs(pipelines []data.PipelineEntry, jobs []data.JobEntry, ids []string) ([]data.PipelineEntry, []data.JobEntry) {
	rpipelines := make([]data.PipelineEntry, 0, len(pipelines))
	rjobs := make([]data.JobEntry, 0, len(jobs))

	pipelineIDMap := make(map[int]string, len(pipelines))
	for _, p := range pipelines {
		pipelineIDMap[p.ID] = p.PublicID
		for _, id := range ids {
			if p.PublicID == id {
				rpipelines = append(rpipelines, p)
				break
			}
		}
	}

	for _, j := range jobs {
		for _, id := range ids {
			if pipelineIDMap[j.PipelineID] == id {
				rjobs = append(rjobs, j)
				break
			}
		}
	}

	return rpipelines, rjobs
}
