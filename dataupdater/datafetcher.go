// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package dataupdater

import (
	"time"

	"gitlab.com/mathweb/gitlab-pipeline-analyzer/data"
	"gitlab.com/mathweb/gitlab-pipeline-analyzer/gitlab"
)

type dataFetcher interface {
	GetProject(fullPath string, ignoreJobTrace bool) (*data.Project, error)
	GetJobEntries(fullPath string, jobName string, before time.Time, after time.Time) ([]data.JobEntry, error)
	GetBuildEntries(fullPath string, branch string, before time.Time, after time.Time) ([]data.PipelineEntry, []data.JobEntry, error)
	GetProjectRESTDataFetcher(data.Project) *gitlab.ProjectRESTDataFetcher
}
