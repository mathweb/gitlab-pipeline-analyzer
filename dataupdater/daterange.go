// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package dataupdater

import "time"

func calculateDateRange(days int, lastUpdate time.Time) (time.Time, time.Time) {
	y, m, d := time.Now().Date()
	today := time.Date(y, m, d, 0, 0, 0, 0, time.UTC)
	after := today.AddDate(0, 0, -days)
	if lastUpdate.After(after) {
		after = lastUpdate
	}

	return after, today
}

func getUpdatedDate() time.Time {
	y, m, d := time.Now().Date()
	return time.Date(y, m, d, 0, 0, 0, 0, time.UTC)
}
