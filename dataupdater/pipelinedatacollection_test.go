// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package dataupdater

import (
	"testing"

	"gitlab.com/mathweb/gitlab-pipeline-analyzer/data"
	"gitlab.com/mathweb/gitlab-pipeline-analyzer/internal/testhelper"
)

func Test_filterToPipelineIDs(t *testing.T) {
	type args struct {
		pipelines []data.PipelineEntry
		jobs      []data.JobEntry
		ids       []string
	}
	type want struct {
		pipelines []data.PipelineEntry
		jobs      []data.JobEntry
	}
	tests := []struct {
		name string
		args args
		want want
	}{
		{
			name: "filter with all empty list",
			args: args{
				pipelines: []data.PipelineEntry{},
				jobs:      []data.JobEntry{},
				ids:       []string{},
			},
			want: want{
				pipelines: []data.PipelineEntry{},
				jobs:      []data.JobEntry{},
			},
		},
		{
			name: "filter with empty list id list",
			args: args{
				pipelines: []data.PipelineEntry{
					{ID: 111, PublicID: "2111"},
					{ID: 112, PublicID: "2112"},
					{ID: 113, PublicID: "2113"},
				},
				jobs: []data.JobEntry{
					{ID: 421, PipelineID: 111},
					{ID: 422, PipelineID: 112},
					{ID: 423, PipelineID: 113},
					{ID: 424, PipelineID: 111},
					{ID: 425, PipelineID: 112},
					{ID: 426, PipelineID: 113},
				},
				ids: []string{},
			},
			want: want{
				pipelines: []data.PipelineEntry{},
				jobs:      []data.JobEntry{},
			},
		},
		{
			name: "filter with given id list",
			args: args{
				pipelines: []data.PipelineEntry{
					{ID: 111, PublicID: "2111"},
					{ID: 112, PublicID: "2112"},
					{ID: 113, PublicID: "2113"},
				},
				jobs: []data.JobEntry{
					{ID: 421, PipelineID: 111},
					{ID: 422, PipelineID: 112},
					{ID: 423, PipelineID: 113},
					{ID: 424, PipelineID: 111},
					{ID: 425, PipelineID: 112},
					{ID: 426, PipelineID: 113},
				},
				ids: []string{"2111", "2113", "2114"},
			},
			want: want{
				pipelines: []data.PipelineEntry{
					{ID: 111, PublicID: "2111"},
					{ID: 113, PublicID: "2113"},
				},
				jobs: []data.JobEntry{
					{ID: 421, PipelineID: 111},
					{ID: 423, PipelineID: 113},
					{ID: 424, PipelineID: 111},
					{ID: 426, PipelineID: 113},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			pipelines, jobs := filterToPipelineIDs(tt.args.pipelines, tt.args.jobs, tt.args.ids)
			testhelper.DeepEqual(t, pipelines, tt.want.pipelines)
			testhelper.DeepEqual(t, jobs, tt.want.jobs)
		})
	}
}
