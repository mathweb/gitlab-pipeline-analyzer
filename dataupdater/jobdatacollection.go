// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package dataupdater

import (
	"fmt"
	"time"

	"gitlab.com/mathweb/gitlab-pipeline-analyzer/data"
)

func UpdateJobData(j *data.JobDataCollection, days int, f dataFetcher) error {
	lastUpdate := time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC)
	if len(j.RawData) > 0 {
		lastUpdate = j.RawData[len(j.RawData)-1].StartedAt
	}
	rangeStart, rangeEnd := calculateDateRange(days, lastUpdate)
	if err := updateProjectData(&j.Project, f); err != nil {
		return err
	}
	if err := updateJobData(j, rangeEnd, rangeStart, f); err != nil {
		return err
	}
	j.Updated = getUpdatedDate()
	j.Job.UpdateSummaries()
	// this numbers are a first estimate and they probably need to be adjustable
	// for different jobs a job with 100 of runs per day need higher numbers
	// then a job rarely run.
	j.Job.CleanUp(1000, 30, 8, 12)
	return nil
}

func updateJobData(j *data.JobDataCollection, before time.Time, after time.Time, f dataFetcher) error {
	jobs, err := f.GetJobEntries(j.FullPath, j.JobName, before, after)
	if err != nil {
		return err
	}
	for _, job := range jobs {
		j.Job.AddEntry(job, f.GetProjectRESTDataFetcher(j.Project))
	}
	return nil
}

func updateProjectData(p *data.Project, f dataFetcher) error {
	np, err := f.GetProject(p.FullPath, p.IgnoreJobTrace)
	if err != nil {
		return fmt.Errorf("updateProjectData got error: %v", err)
	}
	p.ID = np.ID
	p.Name = np.Name
	p.Server = np.Server
	p.DefaultBranch = np.DefaultBranch
	return nil
}
