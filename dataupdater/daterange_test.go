// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package dataupdater

import (
	"testing"
	"time"
)

func Test_calculateDateRange(t *testing.T) {
	y, m, d := time.Now().Date()
	today := time.Date(y, m, d, 0, 0, 0, 0, time.UTC)

	type timeRange struct {
		after  time.Time
		before time.Time
	}
	type args struct {
		days       int
		lastUpdate time.Time
	}
	tests := []struct {
		name string
		args args
		want timeRange
	}{
		{
			name: "get date for the last 5 days",
			args: args{
				days:       5,
				lastUpdate: time.Time{},
			},
			want: timeRange{
				after:  today.AddDate(0, 0, -5),
				before: today,
			},
		},
		{
			name: "get date for the last 5 days, since last update is older",
			args: args{
				days:       5,
				lastUpdate: today.AddDate(0, 0, -10),
			},
			want: timeRange{
				after:  today.AddDate(0, 0, -5),
				before: today,
			},
		},
		{
			name: "get date for the last 4 days, since last update is newer",
			args: args{
				days:       5,
				lastUpdate: today.AddDate(0, 0, -4),
			},
			want: timeRange{
				after:  today.AddDate(0, 0, -4),
				before: today,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			after, before := calculateDateRange(tt.args.days, tt.args.lastUpdate)
			if !after.Equal(tt.want.after) {
				t.Errorf("calculateDateRange() got wrong after date got: %v, want: %v", after, tt.want.after)
			}
			if !before.Equal(tt.want.before) {
				t.Errorf("calculateDateRange() got wrong before date got: %v, want: %v", before, tt.want.before)
			}
		})
	}
}
