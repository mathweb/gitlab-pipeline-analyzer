// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package gitlab

import (
	"context"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/hasura/go-graphql-client"
	"gitlab.com/mathweb/gitlab-pipeline-analyzer/data"
	"golang.org/x/oauth2"
)

type DataFetcher struct {
	server     string
	apiKey     string
	client     *graphql.Client
	httpClient *http.Client
}

func NewDataFetcher(server string, apiKey string) *DataFetcher {
	httpClient := &http.Client{}
	if apiKey != "" {

		src := oauth2.StaticTokenSource(
			&oauth2.Token{AccessToken: apiKey},
		)
		httpClient = oauth2.NewClient(context.Background(), src)
	}
	client := graphql.NewClient(fmt.Sprintf("https://%s/api/graphql", server), httpClient)

	return &DataFetcher{
		server:     server,
		apiKey:     apiKey,
		client:     client,
		httpClient: httpClient,
	}
}

func (d *DataFetcher) GetProjectRESTDataFetcher(p data.Project) *ProjectRESTDataFetcher {
	elements := strings.Split(p.ID, "/")
	pid := elements[len(elements)-1]

	return &ProjectRESTDataFetcher{
		server:         d.server,
		httpClient:     d.httpClient,
		projectID:      pid,
		ignoreJobTrace: p.IgnoreJobTrace,
	}
}

type projectQuery struct {
	Project struct {
		ID         graphql.ID
		Name       graphql.String
		Repository struct {
			RootRef graphql.String
		}
	} `graphql:"project(fullPath: $path)"`
}

func (d *DataFetcher) GetProject(fullPath string, ignoreJobTrace bool) (*data.Project, error) {
	var q projectQuery
	variables := map[string]interface{}{
		"path": graphql.ID(fullPath),
	}

	err := d.client.Query(context.Background(), &q, variables)
	if err != nil {
		return nil, fmt.Errorf("QML Query error: %v", err)
	}
	return data.NewProject(string(q.Project.ID), d.server, fullPath, string(q.Project.Name), string(q.Project.Repository.RootRef), ignoreJobTrace), nil
}

type buildEntriesQuery struct {
	Project struct {
		Pipelines struct {
			Count    graphql.Int
			PageInfo pageInfo
			Nodes    []pipeline
		} `graphql:"pipelines(first: 4, after: $afterKey, ref: $branch, status: $status, updatedAfter: $afterDate, updatedBefore: $beforeDate)"`
	} `graphql:"project(fullPath: $path)"`
}

type pipeline struct {
	ID                graphql.ID
	IID               graphql.String
	Status            graphql.String
	StartedAt         graphql.String
	Duration          graphql.Int
	Coverage          *graphql.Float
	Path              graphql.String
	TestReportSummary struct {
		TestSuites struct {
			Count graphql.Int
			Nodes []testReportSummary
		}
		Total struct {
			Count   graphql.Int
			Error   graphql.Int
			Failed  graphql.Int
			Skipped graphql.Int
			Success graphql.Int
			Time    graphql.Float
		}
	}
	Jobs struct {
		Count graphql.Int
		Nodes []job
	}
}

type testReportSummary struct {
	BuildIds     []graphql.ID
	Name         graphql.String
	SuccessCount graphql.Int
	SkippedCount graphql.Int
	FailedCount  graphql.Int
	ErrorCount   graphql.Int
	TotalCount   graphql.Int
	TotalTime    graphql.Float
}

type artifact struct {
	Name         graphql.String
	DownloadPath graphql.String
	Size         graphql.String
}

type job struct {
	ID             graphql.ID
	Name           graphql.String
	Coverage       *graphql.Float
	Status         graphql.String
	CreatedAt      graphql.String
	StartedAt      graphql.String
	FinishedAt     graphql.String
	Duration       graphql.Int
	QueuedDuration graphql.Float
	Retried        graphql.Boolean
	WebPath        graphql.String
	RefName        graphql.String
	RunnerManager  struct {
		Runner struct {
			Description graphql.String
		}
	}
	Artifacts struct {
		Nodes []artifact
	}
}

func (d *DataFetcher) GetBuildEntries(fullPath string, branch string, before time.Time, after time.Time) ([]data.PipelineEntry, []data.JobEntry, error) {
	type PipelineStatusEnum string

	variables := map[string]interface{}{
		"path":       graphql.ID(fullPath),
		"branch":     branch,
		"status":     PipelineStatusEnum(data.StatusSuccess),
		"afterDate":  after,
		"beforeDate": before,
		"afterKey":   "",
	}

	getMore := true
	pipelines := make([]data.PipelineEntry, 0)
	jobs := make([]data.JobEntry, 0)
	for getMore {
		var qSuccess buildEntriesQuery
		if err := d.client.Query(context.Background(), &qSuccess, variables); err != nil {
			return nil, nil, fmt.Errorf("graphql query successful build entries failed with err: %v", err)
		}

		tmpPips, tmpJobs, err := parseBuildEntryQueryResult(qSuccess, branch)
		if err != nil {
			return nil, nil, fmt.Errorf("parse build successful entries failed: %v", err)
		}
		pipelines = append(pipelines, tmpPips...)
		jobs = append(jobs, tmpJobs...)

		getMore = bool(qSuccess.Project.Pipelines.PageInfo.HasNextPage)
		variables["afterKey"] = string(qSuccess.Project.Pipelines.PageInfo.EndCursor)
	}

	getMore = true
	variables["afterKey"] = ""
	for getMore {
		var qFailed buildEntriesQuery
		variables["status"] = PipelineStatusEnum(data.StatusFailed)
		if err := d.client.Query(context.Background(), &qFailed, variables); err != nil {
			return nil, nil, fmt.Errorf("graphql query failed build entries failed with err: %v", err)
		}

		tmpPips, tmpJobs, err := parseBuildEntryQueryResult(qFailed, branch)
		if err != nil {
			return nil, nil, err
		}

		pipelines = append(pipelines, tmpPips...)
		jobs = append(jobs, tmpJobs...)

		getMore = bool(qFailed.Project.Pipelines.PageInfo.HasNextPage)
		variables["afterKey"] = string(qFailed.Project.Pipelines.PageInfo.EndCursor)
	}

	return pipelines, jobs, nil
}

type jobsQuery struct {
	Project struct {
		Jobs struct {
			Count    graphql.Int
			PageInfo pageInfo
			Nodes    []job
		} `graphql:"jobs(first: 50, after: $afterKey)"`
	} `graphql:"project(fullPath: $path)"`
}

type pageInfo struct {
	StartCursor     graphql.String
	EndCursor       graphql.String
	HasNextPage     graphql.Boolean
	HasPreviousPage graphql.Boolean
}

func (d *DataFetcher) GetJobEntries(fullPath string, jobName string, before time.Time, after time.Time) ([]data.JobEntry, error) {
	getMore := true

	variables := map[string]interface{}{
		"path":     graphql.ID(fullPath),
		"afterKey": "",
	}
	jobs := make([]data.JobEntry, 0)

	for getMore {
		var query jobsQuery
		if err := d.client.Query(context.Background(), &query, variables); err != nil {
			return nil, err
		}

		tmpJobs, err := parseJobEntryQueryResult(query, jobName)
		if err != nil {
			return nil, err
		}
		getMore = bool(query.Project.Jobs.PageInfo.HasNextPage)

		for _, j := range tmpJobs {
			if j.StartedAt.Before(before) && j.StartedAt.After(after) {
				jobs = append(jobs, j)
			} else if j.StartedAt.Before(after) {
				getMore = false
			}
		}
		variables["afterKey"] = string(query.Project.Jobs.PageInfo.EndCursor)
	}
	return jobs, nil
}

func parseBuildEntryQueryResult(q buildEntriesQuery, branch string) ([]data.PipelineEntry, []data.JobEntry, error) {
	pipelines := make([]data.PipelineEntry, 0, len(q.Project.Pipelines.Nodes))
	jobs := make([]data.JobEntry, 0, len(q.Project.Pipelines.Nodes))
	for _, p := range q.Project.Pipelines.Nodes {
		pipeline, err := parsePipelineEntryNode(p, branch)
		if err != nil {
			return nil, nil, err
		}
		pipelines = append(pipelines, pipeline)

		reports := make(map[int]*data.TestReport, len(p.TestReportSummary.TestSuites.Nodes))
		for _, rep := range p.TestReportSummary.TestSuites.Nodes {
			bid, report, err := parseTestReportSummaryNode(rep)
			if err != nil {
				return nil, nil, err
			}
			for _, id := range bid {
				reports[id] = report
			}
		}

		for _, j := range p.Jobs.Nodes {
			job, err := parseJobNode(pipeline.ID, j, reports)
			if err != nil {
				return nil, nil, err
			}
			jobs = append(jobs, job)
		}
	}
	return pipelines, jobs, nil
}

func parseJobEntryQueryResult(q jobsQuery, jobname string) ([]data.JobEntry, error) {
	jobs := make([]data.JobEntry, 0)
	for _, j := range q.Project.Jobs.Nodes {
		if j.Name == graphql.String(jobname) {
			job, err := parseJobNodeOnly(j)
			if err != nil {
				return nil, err
			}
			jobs = append(jobs, job)
		}
	}
	return jobs, nil
}

func parsePipelineEntryNode(p pipeline, branch string) (data.PipelineEntry, error) {
	startedAt := time.Time{}
	if p.StartedAt != "" {
		var err error
		if startedAt, err = time.Parse(time.RFC3339, string(p.StartedAt)); err != nil {
			return data.PipelineEntry{}, err
		}
	}
	pID, err := strconv.ParseInt(string(p.IID), 10, 64)
	if err != nil {
		return data.PipelineEntry{}, fmt.Errorf("failed to parse IID as a 32bit int: %v", err)
	}
	coverage := 0.0
	if p.Coverage != nil {
		coverage = float64(*p.Coverage)
	}
	elements := strings.Split(string(p.ID), "/")
	publicID := "ERR"
	if len(elements) > 0 {
		publicID = elements[len(elements)-1]
	}
	totalArtifactSize := 0.0
	for _, j := range p.Jobs.Nodes {
		for _, a := range j.Artifacts.Nodes {
			if a.Name == "artifacts.zip" {
				value, err := strconv.Atoi(string(a.Size))
				if err != nil {
					// ignore parsing error of artifact size and use 0
					value = 0
				}
				totalArtifactSize += float64(value)
			}
		}
	}

	return data.PipelineEntry{
		ID:            int(pID),
		PublicID:      publicID,
		Branch:        branch,
		Status:        strings.ToLower(string(p.Status)),
		StartedAt:     startedAt,
		Duration:      float64(p.Duration),
		Coverage:      coverage,
		WebURL:        string(p.Path),
		ArtifactsSize: totalArtifactSize,
		PipelineTestReport: data.TestReport{
			Duration: float64(p.TestReportSummary.Total.Time),
			Count:    int64(p.TestReportSummary.Total.Count),
			Success:  int64(p.TestReportSummary.Total.Success),
			Failed:   int64(p.TestReportSummary.Total.Failed),
			Skipped:  int64(p.TestReportSummary.Total.Skipped),
			Error:    int64(p.TestReportSummary.Total.Error),
		},
	}, nil
}

func parseTestReportSummaryNode(rep testReportSummary) ([]int, *data.TestReport, error) {
	ids := make([]int, 0)
	r := data.TestReport{
		Duration: float64(rep.TotalTime),
		Success:  int64(rep.SuccessCount),
		Count:    int64(rep.TotalCount),
		Failed:   int64(rep.FailedCount),
		Error:    int64(rep.ErrorCount),
		Skipped:  int64(rep.SkippedCount),
	}
	for _, bid := range rep.BuildIds {
		id, err := strconv.ParseInt(string(bid), 10, 64)
		if err != nil {
			return []int{}, nil, fmt.Errorf("failed to parse build id as an int, %v", err)
		}
		ids = append(ids, int(id))
	}

	return ids, &r, nil
}

func parseJobNodeOnly(j job) (data.JobEntry, error) {
	cov := 0.
	if j.Coverage != nil {
		cov = float64(*j.Coverage)
	}
	jobStartedAt, err := time.Parse(time.RFC3339, string(j.StartedAt))
	if err != nil {
		// use created time since it might never have been started
		jobStartedAt, err = time.Parse(time.RFC3339, string(j.CreatedAt))
		if err != nil {
			// an error since a job must have been created at some point
			return data.JobEntry{}, fmt.Errorf("the parsed job node does not have a valid start or create date: %s", string(j.ID))
		}
	}

	jobLogWebURL := ""
	artifactSize := 0.0
	for _, a := range j.Artifacts.Nodes {
		switch a.Name {
		case "job.log":
			jobLogWebURL = string(a.DownloadPath)
		case "artifacts.zip":
			value, err := strconv.Atoi(string(a.Size))
			if err != nil {
				// ignore parsing error of artifact size and use 0
				value = 0
			}
			artifactSize += float64(value)
		}
	}

	elements := strings.Split(string(j.ID), "/")
	id, err := strconv.ParseInt(elements[len(elements)-1], 10, 64)
	if err != nil {
		return data.JobEntry{}, fmt.Errorf("failed to parse job id as an int, got: %v", err)
	}

	return data.JobEntry{
		ID:             int(id),
		Ref:            string(j.RefName),
		Status:         string(j.Status),
		Name:           string(j.Name),
		Coverage:       cov,
		StartedAt:      jobStartedAt,
		Duration:       float64(j.Duration),
		QueuedDuration: float64(j.QueuedDuration),
		Retried:        bool(j.Retried),
		WebURL:         string(j.WebPath),
		LogWebURL:      jobLogWebURL,
		ArtifactsSize:  artifactSize,
		RunnerName:     string(j.RunnerManager.Runner.Description),
	}, nil
}

func parseJobNode(pid int, j job, reports map[int]*data.TestReport) (data.JobEntry, error) {
	job, err := parseJobNodeOnly(j)
	if err != nil {
		return job, err
	}
	job.PipelineID = pid
	job.TestReport = reports[job.ID]
	return job, err
}
