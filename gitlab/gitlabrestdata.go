// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package gitlab

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"time"
)

type ProjectRESTDataFetcher struct {
	server         string
	httpClient     *http.Client
	projectID      string
	ignoreJobTrace bool
}

func (r ProjectRESTDataFetcher) GetJobTrace(jobID int) (string, error) {
	traceURL := fmt.Sprintf("https://%s/api/v4/projects/%s/jobs/%d/trace", r.server, r.projectID, jobID)
	data, err := r.httpClient.Get(traceURL)
	if err != nil {
		return "", err
	}
	if data.StatusCode != http.StatusOK {
		return "", fmt.Errorf("GetJobTrace(): failed to get http data, got: %s (%s)", data.Status, traceURL)
	}
	trace, err := io.ReadAll(data.Body)
	return string(trace), err
}

func (r ProjectRESTDataFetcher) IgnoreJobTrace() bool {
	return r.ignoreJobTrace
}

func (r ProjectRESTDataFetcher) GetScheduledPipelineIds(scheduleID int, createdAfter time.Time, createdBefore time.Time) ([]string, error) {
	// get first page to get total count since there is no inverse or filter available
	_, count, err := r.getScheduledPipelineIds(scheduleID, 1, 0)
	if err != nil {
		return []string{}, err
	}
	ids := []string{}
	page := (count / 100) + 1
	for page > 0 {
		pipelines, _, err := r.getScheduledPipelineIds(scheduleID, 100, page)
		page--
		if err != nil {
			return []string{}, err
		}

		// iterate over elements backwards since the REST API only allows to get
		// the elements from oldest to newest
		pcount := len(pipelines) - 1
		for i := range pipelines {
			if pipelines[pcount-i].createdAt.Before(createdAfter) {
				return ids, nil
			}
			if !pipelines[pcount-i].createdAt.After(createdBefore) {
				ids = append(ids, pipelines[pcount-i].id)
			}
		}
	}

	return ids, err
}

type scheduledPipeline struct {
	id        string
	createdAt time.Time
}

func (r ProjectRESTDataFetcher) getScheduledPipelineIds(scheduleID int, count int, page int) ([]scheduledPipeline, int, error) {
	traceURL := fmt.Sprintf("https://%s/api/v4/projects/%s/pipeline_schedules/%d/pipelines?per_page=%d&page=%d", r.server, r.projectID, scheduleID, count, page)

	data, err := r.httpClient.Get(traceURL)
	if err != nil {
		return []scheduledPipeline{}, 0, err
	}
	if data.StatusCode != http.StatusOK {
		return []scheduledPipeline{}, 0, fmt.Errorf("getScheduledPipelineIds(): failed to get http data (%s), got: %s", traceURL, data.Status)
	}
	totalPipelines, err := strconv.Atoi(data.Header.Get("x-total"))
	elements := make([]struct {
		ID        int    `json:"id"`
		CreatedAt string `json:"created_at"` //nolint:tagliatelle // defined by gitlab api
	}, 0)
	if err := json.NewDecoder(data.Body).Decode(&elements); err != nil {
		return nil, 0, err
	}

	pipelines := make([]scheduledPipeline, len(elements))
	for i := range elements {
		createdAt := time.Time{}
		if elements[i].CreatedAt != "" {
			var err error
			if createdAt, err = time.Parse(time.RFC3339, elements[i].CreatedAt); err != nil {
				return []scheduledPipeline{}, 0, err
			}
		}
		pipelines[i] = scheduledPipeline{
			id:        fmt.Sprintf("%d", elements[i].ID),
			createdAt: createdAt,
		}
	}

	return pipelines, totalPipelines, err
}
