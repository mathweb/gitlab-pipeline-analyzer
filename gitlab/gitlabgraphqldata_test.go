// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package gitlab

import (
	"testing"
	"time"

	"github.com/hasura/go-graphql-client"
	"gitlab.com/mathweb/gitlab-pipeline-analyzer/data"
	"gitlab.com/mathweb/gitlab-pipeline-analyzer/internal/testhelper"
)

func Test_parseJobNode(t *testing.T) {
	coverage := graphql.Float(13.)
	type args struct {
		pid     int
		j       job
		reports map[int]*data.TestReport
	}
	tests := []struct {
		name    string
		args    args
		want    data.JobEntry
		wantErr bool
	}{
		{
			name: "Parse a full JobEntry",
			args: args{
				pid: 42,
				j: job{
					ID:             graphql.ID("11"),
					Name:           "Job11",
					Coverage:       &coverage,
					Status:         "success",
					StartedAt:      "2022-12-19T16:39:57-08:00",
					Duration:       120,
					QueuedDuration: 111,
					FinishedAt:     "2022-12-19T16:42:57-08:00",
					WebPath:        "path/to/job11",
					RefName:        "main",
				},
				reports: map[int]*data.TestReport{
					11: {
						Count:    10,
						Duration: 11,
						Success:  10,
						Failed:   0,
						Skipped:  0,
						Error:    0,
					},
				},
			},
			want: data.JobEntry{
				ID:             11,
				Ref:            "main",
				Status:         "success",
				Name:           "Job11",
				StartedAt:      time.Date(2022, 12, 20, 00, 39, 57, 0, time.UTC),
				Duration:       120,
				Coverage:       13.,
				WebURL:         "path/to/job11",
				PipelineID:     42,
				QueuedDuration: 111,
				TestReport: &data.TestReport{
					Count:    10,
					Duration: 11,
					Success:  10,
					Failed:   0,
					Skipped:  0,
					Error:    0,
				},
			},
			wantErr: false,
		},
		{
			name: "Parse a JobEntry without coverage",
			args: args{
				pid: 42,
				j: job{
					ID:             graphql.ID("11"),
					Name:           "Job11",
					Coverage:       nil,
					Status:         "success",
					StartedAt:      "2022-12-19T16:39:57-08:00",
					Duration:       120,
					QueuedDuration: 111,
					FinishedAt:     "2022-12-19T16:42:57-08:00",
					WebPath:        "path/to/job11",
					RefName:        "main",
				},
				reports: map[int]*data.TestReport{
					11: {
						Count:    10,
						Duration: 11,
						Success:  10,
						Failed:   0,
						Skipped:  0,
						Error:    0,
					},
				},
			},
			want: data.JobEntry{
				ID:             11,
				Ref:            "main",
				Status:         "success",
				Name:           "Job11",
				StartedAt:      time.Date(2022, 12, 20, 00, 39, 57, 0, time.UTC),
				Duration:       120,
				Coverage:       0.,
				WebURL:         "path/to/job11",
				PipelineID:     42,
				QueuedDuration: 111,
				TestReport: &data.TestReport{
					Count:    10,
					Duration: 11,
					Success:  10,
					Failed:   0,
					Skipped:  0,
					Error:    0,
				},
			},
			wantErr: false,
		},
		{
			name: "Parse a JobEntry with no valid start time but valid created at time",
			args: args{
				pid: 42,
				j: job{
					ID:             graphql.ID("11"),
					Name:           "Job11",
					Coverage:       &coverage,
					Status:         "success",
					StartedAt:      "",
					CreatedAt:      "2022-12-19T16:39:57-08:00",
					Duration:       120,
					QueuedDuration: 111,
					FinishedAt:     "",
					WebPath:        "path/to/job11",
					RefName:        "main",
				},
				reports: map[int]*data.TestReport{
					11: {
						Count:    10,
						Duration: 11,
						Success:  10,
						Failed:   0,
						Skipped:  0,
						Error:    0,
					},
				},
			},
			want: data.JobEntry{
				ID:             11,
				Ref:            "main",
				Status:         "success",
				Name:           "Job11",
				StartedAt:      time.Date(2022, 12, 20, 00, 39, 57, 0, time.UTC),
				Duration:       120,
				Coverage:       13.,
				WebURL:         "path/to/job11",
				PipelineID:     42,
				QueuedDuration: 111,
				TestReport: &data.TestReport{
					Count:    10,
					Duration: 11,
					Success:  10,
					Failed:   0,
					Skipped:  0,
					Error:    0,
				},
			},
			wantErr: false,
		},
		{
			name: "Parse a JobEntry with no valid start time or created at time",
			args: args{
				pid: 42,
				j: job{
					ID:             graphql.ID("11"),
					Name:           "Job11",
					Coverage:       &coverage,
					Status:         "success",
					StartedAt:      "",
					CreatedAt:      "",
					Duration:       120,
					QueuedDuration: 111,
					FinishedAt:     "",
					WebPath:        "path/to/job11",
					RefName:        "main",
				},
				reports: map[int]*data.TestReport{
					11: {
						Count:    10,
						Duration: 11,
						Success:  10,
						Failed:   0,
						Skipped:  0,
						Error:    0,
					},
				},
			},
			want:    data.JobEntry{},
			wantErr: true,
		},
		{
			name: "Fail to parse job id",
			args: args{
				pid: 42,
				j: job{
					ID:             graphql.ID("11/asdf"),
					Name:           "Job11",
					Coverage:       &coverage,
					Status:         "success",
					StartedAt:      "2022-12-19T16:39:57-08:00",
					Duration:       120,
					QueuedDuration: 111,
					FinishedAt:     "2022-12-19T16:42:57-08:00",
					WebPath:        "path/to/job11",
				},
				reports: map[int]*data.TestReport{
					11: {
						Count:    10,
						Duration: 11,
						Success:  10,
						Failed:   0,
						Skipped:  0,
						Error:    0,
					},
				},
			},
			want:    data.JobEntry{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := parseJobNode(tt.args.pid, tt.args.j, tt.args.reports)
			if (err != nil) != tt.wantErr {
				t.Errorf("parseJobNode() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			testhelper.DeepEqual(t, got, tt.want)
		})
	}
}

func Test_parseTestReportSummaryNode(t *testing.T) {
	type args struct {
		rep testReportSummary
	}
	type want struct {
		ids     []int
		reports *data.TestReport
	}
	tests := []struct {
		name    string
		args    args
		want    want
		wantErr bool
	}{
		{
			name: "parse complete valid reports",
			args: args{
				rep: testReportSummary{
					BuildIds:     []graphql.ID{graphql.ID("11"), graphql.ID("12")},
					Name:         "DemoReport",
					SuccessCount: 10,
					SkippedCount: 0,
					FailedCount:  12,
					ErrorCount:   11,
					TotalCount:   33,
					TotalTime:    12,
				},
			},
			want: want{
				ids: []int{11, 12},
				reports: &data.TestReport{
					Duration: 12,
					Count:    33,
					Success:  10,
					Failed:   12,
					Skipped:  0,
					Error:    11,
				},
			},
		},
		{
			name: "parse complete invalid reports (invalid build id)",
			args: args{
				rep: testReportSummary{
					BuildIds:     []graphql.ID{graphql.ID("11"), graphql.ID("12-asdf")},
					Name:         "DemoReport",
					SuccessCount: 10,
					SkippedCount: 0,
					FailedCount:  12,
					ErrorCount:   11,
					TotalCount:   33,
					TotalTime:    12,
				},
			},
			want: want{
				ids:     []int{},
				reports: nil,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ids, reports, err := parseTestReportSummaryNode(tt.args.rep)
			if (err != nil) != tt.wantErr {
				t.Errorf("parseTestReportSummaryNode() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			testhelper.DeepEqual(t, ids, tt.want.ids)
			testhelper.DeepEqual(t, reports, tt.want.reports)
		})
	}
}

func Test_parsePipelineEntryNode(t *testing.T) {
	coverage := graphql.Float(15.)
	type args struct {
		p      pipeline
		branch string
	}
	tests := []struct {
		name    string
		args    args
		want    data.PipelineEntry
		wantErr bool
	}{
		{
			name: "parse complete pipeline entry",
			args: args{
				p: pipeline{
					ID:        graphql.ID("graph://more/other/223"),
					IID:       "123",
					Status:    "success",
					StartedAt: "2022-12-19T16:39:57-08:00",
					Duration:  120,
					Coverage:  &coverage,
					Path:      "/add/more/url",
					TestReportSummary: struct {
						TestSuites struct {
							Count graphql.Int
							Nodes []testReportSummary
						}
						Total struct {
							Count   graphql.Int
							Error   graphql.Int
							Failed  graphql.Int
							Skipped graphql.Int
							Success graphql.Int
							Time    graphql.Float
						}
					}{
						Total: struct {
							Count   graphql.Int
							Error   graphql.Int
							Failed  graphql.Int
							Skipped graphql.Int
							Success graphql.Int
							Time    graphql.Float
						}{
							Count:   10,
							Success: 5,
							Failed:  3,
							Error:   2,
							Skipped: 0,
							Time:    12,
						},
					},
				},
				branch: "main",
			},
			want: data.PipelineEntry{
				ID:        123,
				PublicID:  "223",
				Status:    "success",
				Branch:    "main",
				StartedAt: time.Date(2022, 12, 20, 00, 39, 57, 0, time.UTC),
				Duration:  120,
				Coverage:  15.,
				WebURL:    "/add/more/url",
				PipelineTestReport: data.TestReport{
					Duration: 12.,
					Count:    10,
					Success:  5,
					Failed:   3,
					Error:    2,
					Skipped:  0,
				},
			},
		},
		{
			name: "parse complete pipeline entry with invalid startedAt value",
			args: args{
				p: pipeline{
					ID:        graphql.ID("graph://more/other/223"),
					IID:       "123",
					Status:    "success",
					StartedAt: "2022444",
					Duration:  120,
					Coverage:  &coverage,
					Path:      "/add/more/url",
				},
				branch: "master",
			},
			want:    data.PipelineEntry{},
			wantErr: true,
		},
		{
			name: "parse complete pipeline entry with invalid iid value",
			args: args{
				p: pipeline{
					ID:        graphql.ID("graph://more/other/223"),
					IID:       "123-asdf",
					Status:    "success",
					StartedAt: "2022-12-19T16:39:57-08:00",
					Duration:  120,
					Coverage:  &coverage,
					Path:      "/add/more/url",
				},
				branch: "master",
			},
			want:    data.PipelineEntry{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := parsePipelineEntryNode(tt.args.p, tt.args.branch)
			if (err != nil) != tt.wantErr {
				t.Errorf("parsePipelineEntryNode() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			testhelper.DeepEqual(t, got, tt.want)
		})
	}
}

func Test_parseBuildEntryQueryResult(t *testing.T) {
	coverage := graphql.Float(16.0)
	type args struct {
		q      buildEntriesQuery
		branch string
	}
	type want struct {
		pipelines  []data.PipelineEntry
		jobEntries []data.JobEntry
	}
	tests := []struct {
		name    string
		args    args
		want    want
		wantErr bool
	}{
		{
			name: "parse full pipeline with all substructures",
			args: args{
				q: buildEntriesQuery{
					Project: struct {
						Pipelines struct {
							Count    graphql.Int
							PageInfo pageInfo
							Nodes    []pipeline
						} "graphql:\"pipelines(first: 4, after: $afterKey, ref: $branch, status: $status, updatedAfter: $afterDate, updatedBefore: $beforeDate)\""
					}{
						Pipelines: struct {
							Count    graphql.Int
							PageInfo pageInfo
							Nodes    []pipeline
						}{
							Count: 1,
							PageInfo: pageInfo{
								StartCursor:     "asdf",
								EndCursor:       "fdas",
								HasNextPage:     false,
								HasPreviousPage: false,
							},
							Nodes: []pipeline{
								{
									ID:        graphql.ID("graph://more/other/2234"),
									IID:       "1234",
									Status:    "success",
									StartedAt: "2022-12-19T16:39:57-08:00",
									Duration:  123,
									Coverage:  &coverage,
									Path:      "asdf/test",
									TestReportSummary: struct {
										TestSuites struct {
											Count graphql.Int
											Nodes []testReportSummary
										}
										Total struct {
											Count   graphql.Int
											Error   graphql.Int
											Failed  graphql.Int
											Skipped graphql.Int
											Success graphql.Int
											Time    graphql.Float
										}
									}{
										TestSuites: struct {
											Count graphql.Int
											Nodes []testReportSummary
										}{
											Count: 2,
											Nodes: []testReportSummary{
												{
													BuildIds:     []graphql.ID{"7"},
													Name:         "One",
													SuccessCount: 10,
													SkippedCount: 2,
													FailedCount:  3,
													ErrorCount:   3,
													TotalCount:   16,
													TotalTime:    12,
												},
												{
													BuildIds:     []graphql.ID{"8"},
													Name:         "Two",
													SuccessCount: 11,
													SkippedCount: 2,
													FailedCount:  2,
													ErrorCount:   3,
													TotalCount:   16,
													TotalTime:    13,
												},
											},
										},
										Total: struct {
											Count   graphql.Int
											Error   graphql.Int
											Failed  graphql.Int
											Skipped graphql.Int
											Success graphql.Int
											Time    graphql.Float
										}{
											Count:   32,
											Error:   6,
											Failed:  5,
											Skipped: 4,
											Success: 21,
											Time:    25,
										},
									},
									Jobs: struct {
										Count graphql.Int
										Nodes []job
									}{
										Count: 2,
										Nodes: []job{
											{
												ID:             graphql.ID("7"),
												Name:           "TestResult1",
												Coverage:       &coverage,
												Status:         "success",
												StartedAt:      "2022-12-19T16:39:57-08:00",
												Duration:       123,
												QueuedDuration: 111,
												WebPath:        "job/1",
												RefName:        "main",
											},
											{
												ID:             graphql.ID("8"),
												Name:           "TestResult2",
												Coverage:       &coverage,
												Status:         "success",
												StartedAt:      "2022-12-19T16:39:57-08:00",
												Duration:       123,
												QueuedDuration: 111,
												WebPath:        "job/2",
												RefName:        "main",
											},
										},
									},
								},
							},
						},
					},
				},
				branch: "master",
			},
			want: want{
				pipelines: []data.PipelineEntry{
					{
						ID:        1234,
						PublicID:  "2234",
						Status:    "success",
						Branch:    "master",
						StartedAt: time.Date(2022, 12, 20, 00, 39, 57, 0, time.UTC),
						Duration:  123,
						Coverage:  16.,
						WebURL:    "asdf/test",
						PipelineTestReport: data.TestReport{
							Duration: 25,
							Count:    32,
							Success:  21,
							Failed:   5,
							Skipped:  4,
							Error:    6,
						},
					},
				},
				jobEntries: []data.JobEntry{
					{
						ID:             7,
						PipelineID:     1234,
						Status:         "success",
						Ref:            "main",
						Name:           "TestResult1",
						StartedAt:      time.Date(2022, 12, 20, 00, 39, 57, 0, time.UTC),
						Duration:       123,
						Coverage:       16.0,
						QueuedDuration: 111,
						WebURL:         "job/1",
						TestReport: &data.TestReport{
							Duration: 12,
							Count:    16,
							Success:  10,
							Skipped:  2,
							Failed:   3,
							Error:    3,
						},
					},
					{
						ID:             8,
						PipelineID:     1234,
						Status:         "success",
						Ref:            "main",
						Name:           "TestResult2",
						StartedAt:      time.Date(2022, 12, 20, 00, 39, 57, 0, time.UTC),
						Duration:       123,
						Coverage:       16.0,
						QueuedDuration: 111,
						WebURL:         "job/2",
						TestReport: &data.TestReport{
							Duration: 13,
							Count:    16,
							Success:  11,
							Skipped:  2,
							Failed:   2,
							Error:    3,
						},
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p, j, err := parseBuildEntryQueryResult(tt.args.q, tt.args.branch)
			if (err != nil) != tt.wantErr {
				t.Errorf("parseBuildEntryQueryResult() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			testhelper.DeepEqual(t, p, tt.want.pipelines)
			testhelper.DeepEqual(t, j, tt.want.jobEntries)
		})
	}
}

func Test_parseJobEntryQueryResult(t *testing.T) {
	coverage := graphql.Float(16.0)
	type args struct {
		q       jobsQuery
		jobName string
	}
	tests := []struct {
		name    string
		args    args
		want    []data.JobEntry
		wantErr bool
	}{
		{
			name: "parse empty result list",
			args: args{
				q: jobsQuery{
					Project: struct {
						Jobs struct {
							Count    graphql.Int
							PageInfo pageInfo
							Nodes    []job
						} "graphql:\"jobs(first: 50, after: $afterKey)\""
					}{
						Jobs: struct {
							Count    graphql.Int
							PageInfo pageInfo
							Nodes    []job
						}{
							Count: 0,
							PageInfo: pageInfo{
								StartCursor:     graphql.String("asdf"),
								EndCursor:       graphql.String("qwert"),
								HasNextPage:     graphql.Boolean(true),
								HasPreviousPage: graphql.Boolean(false),
							},
							Nodes: []job{},
						},
					},
				},
				jobName: "test",
			},
			want:    []data.JobEntry{},
			wantErr: false,
		},
		{
			name: "parse list",
			args: args{
				q: jobsQuery{
					Project: struct {
						Jobs struct {
							Count    graphql.Int
							PageInfo pageInfo
							Nodes    []job
						} "graphql:\"jobs(first: 50, after: $afterKey)\""
					}{
						Jobs: struct {
							Count    graphql.Int
							PageInfo pageInfo
							Nodes    []job
						}{
							Count: 0,
							PageInfo: pageInfo{
								StartCursor:     graphql.String("asdf"),
								EndCursor:       graphql.String("qwert"),
								HasNextPage:     graphql.Boolean(true),
								HasPreviousPage: graphql.Boolean(false),
							},
							Nodes: []job{
								{
									ID:             graphql.ID("7"),
									Name:           "Job1",
									Coverage:       &coverage,
									Status:         "success",
									StartedAt:      "2022-12-19T16:39:57-08:00",
									Duration:       123,
									QueuedDuration: 111,
									WebPath:        "job/1",
									RefName:        "main",
								},
								{
									ID:             graphql.ID("8"),
									Name:           "Job1",
									Coverage:       &coverage,
									Status:         "success",
									StartedAt:      "2022-12-19T16:40:57-08:00",
									Duration:       124,
									QueuedDuration: 112,
									WebPath:        "job/1",
									RefName:        "branch/merge",
								},
								{
									ID:             graphql.ID("9"),
									Name:           "Job2",
									Coverage:       &coverage,
									Status:         "success",
									StartedAt:      "2022-12-19T16:41:57-08:00",
									Duration:       125,
									QueuedDuration: 113,
									WebPath:        "job/1",
									RefName:        "main",
								},
							},
						},
					},
				},
				jobName: "Job1",
			},
			want: []data.JobEntry{
				{
					ID:             7,
					PipelineID:     0,
					Status:         "success",
					Ref:            "main",
					Name:           "Job1",
					StartedAt:      time.Date(2022, 12, 20, 00, 39, 57, 0, time.UTC),
					Duration:       123,
					Coverage:       16.0,
					QueuedDuration: 111,
					WebURL:         "job/1",
					TestReport:     nil,
				},
				{
					ID:             8,
					PipelineID:     0,
					Status:         "success",
					Ref:            "branch/merge",
					Name:           "Job1",
					StartedAt:      time.Date(2022, 12, 20, 00, 40, 57, 0, time.UTC),
					Duration:       124,
					Coverage:       16.0,
					QueuedDuration: 112,
					WebURL:         "job/1",
					TestReport:     nil,
				},
			},
			wantErr: false,
		},
		{
			name: "parse list with no entries with a matching name",
			args: args{
				q: jobsQuery{
					Project: struct {
						Jobs struct {
							Count    graphql.Int
							PageInfo pageInfo
							Nodes    []job
						} "graphql:\"jobs(first: 50, after: $afterKey)\""
					}{
						Jobs: struct {
							Count    graphql.Int
							PageInfo pageInfo
							Nodes    []job
						}{
							Count: 0,
							PageInfo: pageInfo{
								StartCursor:     graphql.String("asdf"),
								EndCursor:       graphql.String("qwert"),
								HasNextPage:     graphql.Boolean(true),
								HasPreviousPage: graphql.Boolean(false),
							},
							Nodes: []job{
								{
									ID:             graphql.ID("7"),
									Name:           "Job1",
									Coverage:       &coverage,
									Status:         "success",
									StartedAt:      "2022-12-19T16:39:57-08:00",
									Duration:       123,
									QueuedDuration: 111,
									WebPath:        "job/1",
									RefName:        "main",
								},
								{
									ID:             graphql.ID("7"),
									Name:           "Job2",
									Coverage:       &coverage,
									Status:         "success",
									StartedAt:      "2022-12-19T16:39:57-08:00",
									Duration:       123,
									QueuedDuration: 111,
									WebPath:        "job/1",
									RefName:        "main",
								},
							},
						},
					},
				},
				jobName: "test",
			},
			want:    []data.JobEntry{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := parseJobEntryQueryResult(tt.args.q, tt.args.jobName)
			if (err != nil) != tt.wantErr {
				t.Errorf("parseJobEntryQueryResult() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			testhelper.DeepEqual(t, got, tt.want)
		})
	}
}
