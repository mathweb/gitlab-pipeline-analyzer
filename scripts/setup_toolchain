#!/bin/bash
# Copyright: (c) 2023 Mathias Weber 

set -e
set -u

installAll() {
  rm -rf build/ext/bin
  mkdir -p build/ext/bin

  checkInstall "Go" "1.21.4"
  checkInstall "GolangCILint" "1.55.2"
  checkInstall "Gotestsum" "1.11.0"
}

checkInstall() {
  name=$1
  version=$2

  install_path="build/ext"

  if [ -d "${install_path}" ] && [ -f "${install_path}/.${name}_${version}" ]; then
    "link${name}" "${install_path}" "${version}"
    return
  fi

  mkdir -p "${install_path}/tmp/"
  echo "Install ${name} - ${version}"
  rm -rf "${install_path}/.${name}_"*
  "install${name}" "${install_path}" "${version}"
  touch "${install_path}/.${name}_${version}"
  rm -rf "${install_path}/tmp"
}

installGo() {
  install_path=$1
  version=$2

  curl "https://dl.google.com/go/go${version}.linux-amd64.tar.gz" --output "${install_path}/tmp/go.linux-amd64.tar.gz"
  rm -rf "${install_path}/go"
  tar xf "${install_path}/tmp/go.linux-amd64.tar.gz" -C "${install_path}"

  linkGo "${install_path}"
}

linkGo() {
  install_path=$1

  ln -s "$(readlink -f "${install_path}/go/bin/go")" "${install_path}/bin/go"
}

installGolangCILint() {
  install_path=$1
  version=$2

  curl -L "https://github.com/golangci/golangci-lint/releases/download/v${version}/golangci-lint-${version}-linux-amd64.tar.gz" --output "${install_path}/tmp/golangci-lint.linux-amd64.tar.gz"
  rm -rf "${install_path}/golangci-lint"
  tar xf "${install_path}/tmp/golangci-lint.linux-amd64.tar.gz" -C "${install_path}"
  mv "${install_path}/golangci-lint-${version}-linux-amd64" "${install_path}/golangci-lint"

  linkGolangCILint "${install_path}"
}

linkGolangCILint() {
  install_path=$1

  ln -s "$(readlink -f "${install_path}/golangci-lint/golangci-lint")" "${install_path}/bin/golangci-lint"
}

installGotestsum() {
  install_path=$1
  version=$2

  curl -L "https://github.com/gotestyourself/gotestsum/releases/download/v${version}/gotestsum_${version}_linux_amd64.tar.gz" --output "${install_path}/tmp/gotestsum.linux-amd64.tar.gz"
  rm -rf "${install_path}/gotestsum"
  mkdir -p "${install_path}/gotestsum"
  tar xf "${install_path}/tmp/gotestsum.linux-amd64.tar.gz" -C "${install_path}/gotestsum/"

  linkGotestsum "${install_path}"
}

linkGotestsum() {
  install_path=$1

  ln -s "$(readlink -f "${install_path}/gotestsum/gotestsum")" "${install_path}/bin/gotestsum"
}

scriptPath=$(dirname "$(readlink -f "$0")")
cd ${scriptPath}/..
installAll

cd ${scriptPath}/..
export GOBIN="${scriptPath}/../build/bin"
export PATH=${scriptPath}/../build/ext/bin:${PATH}

go get github.com/boumenot/gocover-cobertura
