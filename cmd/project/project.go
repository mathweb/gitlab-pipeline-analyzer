// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package project

import (
	"github.com/spf13/cobra"
)

// ProjectCmd represents the project sub command with all the specific commands to
// handle pipeline data for a project.
var ProjectCmd = &cobra.Command{
	Use:   "project",
	Short: "handle project pipeline statistics and output",
	Long: `Provide the functionality to create/update/print pipeline statistics for a
project. For example:

  gitlab-pipeline-analyzer project update datafile.gba -k "TOKEN" -p "ch1011213/gitlab-pipeline-analyzer" -d 10
  gitlab-pipeline-analyzer project update datafile.gba -k "TOKEN"
  gitlab-pipeline-analyzer project html datafile.gba -o output.html`,
}

func init() { //nolint:gochecknoinits // defined by cobra framework
}
