// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package project

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/mathweb/gitlab-pipeline-analyzer/data"
	"gitlab.com/mathweb/gitlab-pipeline-analyzer/renderer"
	"gitlab.com/mathweb/gitlab-pipeline-analyzer/storage"
)

var (
	jobReportsFolder string
)

// htmlCmd represents the update command
var jobReportsCmd = &cobra.Command{
	Use:   "jobreports",
	Short: "Generate HTML reports for all jobs from project pipeline data file",
	Long: `Generate HTML reports for all jobs from project pipeline data file..
For example:

  gitlab-pipeline-analyzer project jobreports datafile.gba -o output`,
	Args: cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		if !storage.FileExists(args[0]) {
			fmt.Printf("Data file does not exist: %s", args[0])
			os.Exit(1)
		}

		collection, err := storage.ReadPipelineData(args[0])
		if err != nil {
			fmt.Printf("Failed to parse data file: %v\n", err)
			os.Exit(1)
		}
		if !storage.DirectoryExists(jobReportsFolder) {
			if err := os.MkdirAll(jobReportsFolder, os.ModePerm); err != nil {
				fmt.Printf("Failed to create folder to store the results: %s, error: %v", args[0], err)
				os.Exit(1)
			}
		}
		for name := range collection.Jobs {
			if err := createJobReport(collection, name, jobReportsFolder); err != nil {
				fmt.Printf("Failed to create html report: %v\n", err)
				os.Exit(1)
			}
		}
	},
}

func init() { //nolint:gochecknoinits // defined by cobra framework
	ProjectCmd.AddCommand(jobReportsCmd)

	jobReportsCmd.Flags().StringVarP(&jobReportsFolder, "output", "o", "", "Set folder to render the job reports.")
}

func createJobReport(c *data.PipelineDataCollection, jobName string, folderName string) error {
	fName := strings.ReplaceAll(jobName, " ", "_")
	fName = strings.ReplaceAll(fName, "/", "_")
	report := filepath.Join(folderName, fmt.Sprintf("job_%s.html", fName))
	f, err := os.Create(report)
	if err != nil {
		return fmt.Errorf("failed to create file to render the page: %s, error: %v", report, err)
	}
	defer f.Close()
	if err := renderer.CreateHTMLJobReportPipelineData(c, jobName, f); err != nil {
		return fmt.Errorf("failed to create html report: %v", err)
	}
	return nil
}
