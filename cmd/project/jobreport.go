// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package project

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/mathweb/gitlab-pipeline-analyzer/renderer"
	"gitlab.com/mathweb/gitlab-pipeline-analyzer/storage"
)

var (
	jobReportOutput string
	jobName         string
)

// htmlCmd represents the update command
var jobReportCmd = &cobra.Command{
	Use:   "jobreport",
	Short: "Generate a HTML report for a job from project pipeline data file",
	Long: `Generate a HTML report for a job from project pipeline data file..
For example:

  gitlab-pipeline-analyzer project jobreport datafile.gba -o output.html`,
	Args: cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		if !storage.FileExists(args[0]) {
			fmt.Printf("Data file does not exist: %s", args[0])
			os.Exit(1)
		}
		if jobName == "" {
			fmt.Printf("Job name must be specified to generate report.")
			os.Exit(1)
		}
		collection, err := storage.ReadPipelineData(args[0])
		if err != nil {
			fmt.Printf("Failed to parse data file: %v\n", err)
			os.Exit(1)
		}
		f, err := os.Create(jobReportOutput)
		if err != nil {
			fmt.Printf("Failed to create file to render the page: %s, error: %v", args[0], err)
			os.Exit(1)
		}
		defer f.Close()
		if err := renderer.CreateHTMLJobReportPipelineData(collection, jobName, f); err != nil {
			fmt.Printf("Failed to create html report: %v\n", err)
			os.Exit(1)
		}
	},
}

func init() { //nolint:gochecknoinits // defined by cobra framework
	ProjectCmd.AddCommand(jobReportCmd)

	jobReportCmd.Flags().StringVarP(&jobReportOutput, "output", "o", "", "Set filename to render the project.")
	jobReportCmd.Flags().StringVarP(&jobName, "job-name", "j", "", "Provide the job name to get the report for.")
}
