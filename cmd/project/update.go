// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package project

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/mathweb/gitlab-pipeline-analyzer/data"
	"gitlab.com/mathweb/gitlab-pipeline-analyzer/dataupdater"
	"gitlab.com/mathweb/gitlab-pipeline-analyzer/gitlab"
	"gitlab.com/mathweb/gitlab-pipeline-analyzer/storage"
)

var (
	apiKey         string
	url            string
	project        string
	days           int
	scheduleID     int
	ignoreJobTrace bool
)

// updateCmd represents the update command
var updateCmd = &cobra.Command{
	Use:   "update",
	Short: "Update a given raw data file for a project",
	Long: `Update a given raw data file with the latest data from the GitLab
server. For example:

  gitlab-pipeline-analyzer project update datafile.gba -k "TOKEN" -p "ch1011213/gitlab-pipeline-analyzer" -d 10

If the data file exists the project can be skipped and will be ignored:

  gitlab-pipeline-analyzer project update datafile.gba -k "TOKEN" -d 10`,
	Args: cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		collection := &data.PipelineDataCollection{
			Project: data.Project{
				FullPath:       project,
				IgnoreJobTrace: ignoreJobTrace,
			},
			FilterScheduleID: scheduleID,
		}

		if storage.FileExists(args[0]) {
			var err error
			collection, err = storage.ReadPipelineData(args[0])
			if err != nil {
				fmt.Printf("Failed to parse data file: %v\n", err)
				os.Exit(1)
			}
		}

		fetcher := gitlab.NewDataFetcher(
			url,
			apiKey,
		)

		if err := dataupdater.UpdatePipelineData(collection, days, fetcher); err != nil {
			fmt.Printf("Got error during data update: %v", err)
			os.Exit(1)
		}

		collection.AllDatesValid()
		if err := storage.SavePipelineData(args[0], collection); err != nil {
			fmt.Printf("Got error during save: %v\n", err)
			fmt.Printf("Collection:\n%+v\n", collection)
			fmt.Printf("Got error during save: %v\n", err)
			os.Exit(1)
		}

	},
}

func init() { //nolint:gochecknoinits // defined by cobra framework
	ProjectCmd.AddCommand(updateCmd)

	updateCmd.Flags().StringVarP(&apiKey, "api-key", "k", "", "Provide the api key to access the gitlab instance.")
	updateCmd.Flags().StringVarP(&url, "url", "u", "gitlab.org", "Provide the instance url to access.")
	updateCmd.Flags().StringVarP(&project, "project", "p", "", "Provide the project path to fetch.")
	updateCmd.Flags().IntVarP(&days, "days", "d", 10, "Set the number of days to fetch back from today.")
	updateCmd.Flags().BoolVarP(&ignoreJobTrace, "ignoreJobTrace", "i", false, "Ignore the job trace output (reduce the data to fetch).")
	updateCmd.Flags().IntVarP(&scheduleID, "filter-schedule", "s", 0, "Set the schedule ID to filter the pipeline results.")
}
