// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/mathweb/gitlab-pipeline-analyzer/cmd/job"
	"gitlab.com/mathweb/gitlab-pipeline-analyzer/cmd/project"
)

var version = "development"
var hash = ""

var showVersion = false

var rootCmd = &cobra.Command{
	Use:   "gitlab-pipeline-analyzer",
	Short: "Collect gitlab pipeline data and render some graphs and tables",
	Long: `Collect gitlab pipeline data and create graphs and tables  including 
historical data that are updated incrementally.`,
	Run: func(cmd *cobra.Command, args []string) {
		if showVersion {
			fmt.Printf("gitlab-pipeline-analyzer: %s (%s)\n", version, hash)
		}
	},
}

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() { //nolint:gochecknoinits // defined by cobra framework
	rootCmd.AddCommand(job.JobCmd, project.ProjectCmd)

	rootCmd.Flags().BoolVarP(&showVersion, "version", "v", false, "Show the version of the gitlab-pipeline-analyzer.")
}
