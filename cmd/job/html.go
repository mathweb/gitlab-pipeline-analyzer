// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package job

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/mathweb/gitlab-pipeline-analyzer/renderer"
	"gitlab.com/mathweb/gitlab-pipeline-analyzer/storage"
)

var (
	output string
)

// updateCmd represents the update command
var htmlCmd = &cobra.Command{
	Use:   "html",
	Short: "Generate a HTML report for a job",
	Long: `Generate a HTML report for a job with a given data file. 
For example:

  gitlab-pipeline-analyzer job html datafile.gba -o report.html`,
	Args: cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		if !storage.FileExists(args[0]) {
			fmt.Printf("Data file does not exist: %s", args[0])
			os.Exit(1)
		}
		collection, err := storage.ReadJobsData(args[0])
		if err != nil {
			fmt.Printf("Failed to parse data file: %v\n", err)
			os.Exit(1)
		}
		f, err := os.Create(output)
		if err != nil {
			fmt.Printf("Failed to create file to render the page: %s, error: %v", args[0], err)
			os.Exit(1)
		}
		defer f.Close()
		if err := renderer.CreateHTMLReportJobData(collection, f); err != nil {
			fmt.Printf("Failed to create html report: %v\n", err)
			os.Exit(1)
		}
	},
}

func init() { //nolint:gochecknoinits // defined by cobra framework
	JobCmd.AddCommand(htmlCmd)

	htmlCmd.Flags().StringVarP(&output, "output", "o", "", "Set filename to render the project.")
}
