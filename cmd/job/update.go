// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package job

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/mathweb/gitlab-pipeline-analyzer/data"
	"gitlab.com/mathweb/gitlab-pipeline-analyzer/dataupdater"
	"gitlab.com/mathweb/gitlab-pipeline-analyzer/gitlab"
	"gitlab.com/mathweb/gitlab-pipeline-analyzer/storage"
)

var (
	apiKey         string
	url            string
	project        string
	jobName        string
	days           int
	ignoreJobTrace bool
)

// updateCmd represents the update command
var updateCmd = &cobra.Command{
	Use:   "update",
	Short: "Update a given raw data file for a job",
	Long: `Update a given raw data file with the latest data from the GitLab
server. For example:

  gitlab-pipeline-analyzer job update datafile.gba -k "TOKEN" -p "ch1011213/gitlab-pipeline-analyzer" -j build -d 10

If the data file exists the project and job name can be skipped and will be ignored:

  gitlab-pipeline-analyzer job update datafile.gba -k "TOKEN" -d 10`,
	Args: cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		collection := &data.JobDataCollection{
			Project: data.Project{
				FullPath:       project,
				IgnoreJobTrace: ignoreJobTrace,
			},
			JobName: jobName,
		}

		if storage.FileExists(args[0]) {
			var err error
			collection, err = storage.ReadJobsData(args[0])
			if err != nil {
				fmt.Printf("Failed to parse data file: %v\n", err)
				os.Exit(1)
			}
		}

		fetcher := gitlab.NewDataFetcher(
			url,
			apiKey,
		)
		if err := dataupdater.UpdateJobData(collection, days, fetcher); err != nil {
			fmt.Printf("Got error during data update: %v", err)
			os.Exit(1)
		}

		if err := storage.SaveJobsData(args[0], collection); err != nil {
			fmt.Printf("Got error during save: %v", err)
			os.Exit(1)
		}
	},
}

func init() { //nolint:gochecknoinits // defined by cobra framework
	JobCmd.AddCommand(updateCmd)

	updateCmd.Flags().StringVarP(&apiKey, "api-key", "k", "", "Provide the api key to access the gitlab instance.")
	updateCmd.Flags().StringVarP(&url, "url", "u", "gitlab.com", "Provide the instance url to access.")
	updateCmd.Flags().StringVarP(&project, "project", "p", "", "Provide the project path to fetch.")
	updateCmd.Flags().StringVarP(&jobName, "job-name", "j", "", "Provide the job name to get the data for.")
	updateCmd.Flags().BoolVarP(&ignoreJobTrace, "ignoreJobTrace", "i", false, "Ignore the job trace output (reduce the data to fetch).")
	updateCmd.Flags().IntVarP(&days, "days", "d", 10, "Set the number of days to fetch back from today.")
}
