// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package job

import (
	"github.com/spf13/cobra"
)

// JobCmd represents the job sub command with all the specific commands to
// handle single job data.
var JobCmd = &cobra.Command{
	Use:   "job",
	Short: "handle job statistics and output",
	Long: `Provide the functionality to create/update/print job statistics for a
single job. For example:

  gitlab-pipeline-analyzer job update datafile.gba
  gitlab-pipeline-analyzer job html datafile.gba`,
}

func init() { //nolint:gochecknoinits // defined by cobra framework
}
