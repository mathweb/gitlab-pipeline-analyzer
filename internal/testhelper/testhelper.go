// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package testhelper

import (
	"testing"
	"time"

	"github.com/go-test/deep"
)

var TestDateDay1 = time.Date(2022, 12, 13, 0, 0, 0, 0, time.UTC)
var TestDateDay2 = time.Date(2022, 12, 14, 0, 0, 0, 0, time.UTC)
var TestDateDay3 = time.Date(2022, 12, 15, 0, 0, 0, 0, time.UTC)
var TestDateDay4 = time.Date(2022, 12, 16, 0, 0, 0, 0, time.UTC)

var TestDateWeek1 = time.Date(2022, 11, 21, 0, 0, 0, 0, time.UTC)
var TestDateWeek2 = time.Date(2022, 11, 28, 0, 0, 0, 0, time.UTC)
var TestDateWeek3 = time.Date(2022, 12, 5, 0, 0, 0, 0, time.UTC)
var TestDateWeek4 = time.Date(2022, 12, 12, 0, 0, 0, 0, time.UTC)

var TestDateMonth1 = time.Date(2022, 8, 1, 0, 0, 0, 0, time.UTC)
var TestDateMonth2 = time.Date(2022, 9, 1, 0, 0, 0, 0, time.UTC)
var TestDateMonth3 = time.Date(2022, 10, 1, 0, 0, 0, 0, time.UTC)
var TestDateMonth4 = time.Date(2022, 11, 1, 0, 0, 0, 0, time.UTC)

// DeepEqual tests if a and b are deep equal.
// Unexported fields are included. Differences are used to call `t.Error`.
func DeepEqual(t *testing.T, a any, b any) {
	t.Helper()

	deep.CompareUnexportedFields = true
	for _, d := range deep.Equal(a, b) {
		t.Error(d)
	}
}
