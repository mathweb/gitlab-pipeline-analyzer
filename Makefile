# Copyright (c) 2023 Mathias Weber

MKFILE_PATH=$(abspath $(lastword $(MAKEFILE_LIST)))
PROJECT_PATH := $(patsubst %/,%,$(dir $(MKFILE_PATH)))

include .env

GOBIN_PATH=$(PROJECT_PATH)/build/bin
SEARCH_PATH := $(GOBIN_PATH):$(PROJECTSEARCHPATH):$(PROJECT_PATH)/scripts:$(PATH)

export GOBIN=${GOBIN_PATH}
export PATH=${SEARCH_PATH}

all: build

clean:
	rm build -rf

build: setup
	go install ./...

setup:
	mkdir -p build && /bin/echo "// Intentionally empty to block go from accessing this directory" > build/go.mod
	./scripts/setup_toolchain

ci: build test lint

test:
	mkdir -p build/reports
	gotestsum --format pkgname -- -covermode=count -coverprofile build/reports/cover.out ./...
	go tool cover -func=build/reports/cover.out

test_xunit: build
	mkdir -p build/reports
	gotestsum --format standard-verbose --junitfile build/reports/junit.xml -- -covermode=count -coverprofile build/reports/cover.out ./...
	go tool cover -func=build/reports/cover.out
	gocover-cobertura < build/reports/cover.out | sed 's:gitlab.com/mathweb/gitlab-pipeline-analyzer/::' > build/reports/coverage.cobertura

lint: setup
	golangci-lint run --out-format tab
