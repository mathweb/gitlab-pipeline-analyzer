// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package renderer

import (
	"reflect" //nolint: depguard //allow reflect here since it is used to compare list from a map (they have random order)
	"sort"
	"strings"
	"testing"

	"gitlab.com/mathweb/gitlab-pipeline-analyzer/data"
	"gitlab.com/mathweb/gitlab-pipeline-analyzer/internal/testhelper"
)

func Test_getContinuousPipelineLinks(t *testing.T) {
	type args struct {
		p       data.Project
		entries []data.PipelineEntry
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "Empty Data",
			args: args{
				p: data.Project{
					Server:   "code.demoserver.com",
					FullPath: "path/name",
				},
				entries: []data.PipelineEntry{},
			},
			want: "",
		},
		{
			name: "Link is properly created from one pipeline entry",
			args: args{
				p: data.Project{
					Server:   "code.demoserver.com",
					FullPath: "path/name",
				},
				entries: []data.PipelineEntry{
					{
						WebURL: "more/good/-/pipeline/123",
					},
				},
			},
			want: "\"https://code.demoserver.com/more/good/-/pipeline/123\"",
		},
		{
			name: "Links are properly created from multiple pipeline entries",
			args: args{
				p: data.Project{
					Server:   "code.demoserver.com",
					FullPath: "path/name",
				},
				entries: []data.PipelineEntry{
					{
						WebURL: "more/good/-/pipeline/123",
					},
					{
						WebURL: "more/good/-/pipeline/231",
					},
					{
						WebURL: "more/good/-/pipeline/312",
					},
				},
			},
			want: "\"https://code.demoserver.com/more/good/-/pipeline/123\",\"https://code.demoserver.com/more/good/-/pipeline/231\",\"https://code.demoserver.com/more/good/-/pipeline/312\"",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getContinuousPipelineLinks(tt.args.p, tt.args.entries); got != tt.want {
				t.Errorf("getContinuousPipelineLinks() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getContinuousPipelineLabels(t *testing.T) {
	type args struct {
		entries []data.PipelineEntry
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "Empty Data",
			args: args{
				entries: []data.PipelineEntry{},
			},
			want: "",
		},
		{
			name: "Link is properly created from one pipeline entry",
			args: args{
				entries: []data.PipelineEntry{
					{
						PublicID: "123",
					},
				},
			},
			want: "123",
		},
		{
			name: "Links are properly created from multiple pipeline entries",
			args: args{
				entries: []data.PipelineEntry{
					{
						PublicID: "123",
					},
					{
						PublicID: "231",
					},
					{
						PublicID: "312",
					},
				},
			},
			want: "123,231,312",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getContinuousPipelineLabels(tt.args.entries); got != tt.want {
				t.Errorf("getContinuousPipelineLabels() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getContinuousPipelineGraphStatusDataSet(t *testing.T) {
	type args struct {
		entries []data.PipelineEntry
	}
	tests := []struct {
		name string
		args args
		want stackedGraphDataSets
	}{
		{
			name: "Empty list",
			args: args{
				entries: []data.PipelineEntry{},
			},
			want: stackedGraphDataSets{
				Label:     "Pipeline Failed",
				Data:      "",
				Type:      "bubble",
				PointSize: 10,
			},
		},
		{
			name: "Create entry with one entry (success)",
			args: args{
				entries: []data.PipelineEntry{
					{
						Status: "success",
					},
				},
			},
			want: stackedGraphDataSets{
				Label:     "Pipeline Failed",
				Data:      "null",
				Type:      "bubble",
				PointSize: 10,
			},
		},
		{
			name: "Create entry with one entry (fail)",
			args: args{
				entries: []data.PipelineEntry{
					{
						Status: "failed",
					},
				},
			},
			want: stackedGraphDataSets{
				Label:     "Pipeline Failed",
				Data:      "0",
				Type:      "bubble",
				PointSize: 10,
			},
		},
		{
			name: "Create entry with multiple entries",
			args: args{
				entries: []data.PipelineEntry{
					{
						Status: "failed",
					},
					{
						Status: "success",
					},
					{
						Status: "failed",
					},
					{
						Status: "success",
					},
				},
			},
			want: stackedGraphDataSets{
				Label:     "Pipeline Failed",
				Data:      "0,null,0,null",
				Type:      "bubble",
				PointSize: 10,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := getContinuousPipelineGraphStatusDataSet(tt.args.entries)
			testhelper.DeepEqual(t, got, tt.want)
		})
	}
}

func Test_getContinuousPipelineGraphDurationDataSet(t *testing.T) {
	type args struct {
		entries []data.PipelineEntry
	}
	tests := []struct {
		name string
		args args
		want stackedGraphDataSets
	}{
		{
			name: "Empty list",
			args: args{
				entries: []data.PipelineEntry{},
			},
			want: stackedGraphDataSets{
				Label:     "Duration [s]",
				Data:      "",
				Type:      "line",
				PointSize: 2,
			},
		},
		{
			name: "Create entry from one job entry",
			args: args{
				entries: []data.PipelineEntry{
					{
						Duration: 100.1,
					},
				},
			},
			want: stackedGraphDataSets{
				Label:     "Duration [s]",
				Data:      "100.10",
				Type:      "line",
				PointSize: 2,
			},
		},
		{
			name: "Create entries from multiple job entries",
			args: args{
				entries: []data.PipelineEntry{
					{
						Duration: 100.1,
					},
					{
						Duration: 101.1,
					},
					{
						Duration: 102.1,
					},
				},
			},
			want: stackedGraphDataSets{
				Label:     "Duration [s]",
				Data:      "100.10,101.10,102.10",
				Type:      "line",
				PointSize: 2,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := getContinuousPipelineGraphDurationDataSet(tt.args.entries)
			testhelper.DeepEqual(t, got, tt.want)
		})
	}
}

func Test_getContinuesJobDurationsPerPipeline(t *testing.T) {
	type args struct {
		entries map[int]int
		job     *data.Job
		jobName string
	}
	tests := []struct {
		name string
		args args
		want stackedGraphDataSets
	}{
		{
			name: "empty list",
			args: args{
				entries: map[int]int{},
				job:     &data.Job{},
				jobName: "DemoJob",
			},
			want: stackedGraphDataSets{
				Label:     "DemoJob",
				Type:      "bar",
				Data:      "",
				PointSize: 1,
			},
		},
		{
			name: "empty job list with not empty entries list",
			args: args{
				entries: map[int]int{
					12: 0,
					21: 1,
					32: 2,
				},
				job:     &data.Job{},
				jobName: "DemoJob",
			},
			want: stackedGraphDataSets{
				Label:     "DemoJob",
				Type:      "bar",
				Data:      ",,",
				PointSize: 1,
			},
		},
		{
			name: "job list with matching entries list",
			args: args{
				entries: map[int]int{
					12: 0,
					21: 1,
					32: 2,
				},
				job: &data.Job{
					RawData: []data.JobEntry{
						{
							Duration:   142.1,
							PipelineID: 32,
						},
						{
							Duration:   100.1,
							PipelineID: 21,
						},
					},
				},
				jobName: "DemoJob",
			},
			want: stackedGraphDataSets{
				Label:     "DemoJob",
				Type:      "bar",
				Data:      ",100.10,142.10",
				PointSize: 1,
			},
		},
		{
			name: "job list with not matching elements for the entries list (ignored)",
			args: args{
				entries: map[int]int{
					12: 0,
					21: 1,
					32: 2,
				},
				job: &data.Job{
					RawData: []data.JobEntry{
						{
							Duration:   142.1,
							PipelineID: 32,
						},
						{
							Duration:   100.1,
							PipelineID: 21,
						},
						{
							Duration:   23.1,
							PipelineID: 666,
						},
					},
				},
				jobName: "DemoJob",
			},
			want: stackedGraphDataSets{
				Label:     "DemoJob",
				Type:      "bar",
				Data:      ",100.10,142.10",
				PointSize: 1,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := getContinuesJobDurationsPerPipeline(tt.args.entries, tt.args.job, tt.args.jobName)
			testhelper.DeepEqual(t, got, tt.want)
		})
	}
}

func Test_getContinuesJobsDurationsPerPipeline(t *testing.T) {
	type args struct {
		entries []data.PipelineEntry
		jobs    map[string]*data.Job
	}
	tests := []struct {
		name string
		args args
		want []stackedGraphDataSets
	}{
		{
			name: "empty lists",
			args: args{
				entries: []data.PipelineEntry{},
				jobs:    map[string]*data.Job{},
			},
			want: []stackedGraphDataSets{},
		},
		{
			name: "empty jobs list but with pipeline entries",
			args: args{
				entries: []data.PipelineEntry{
					{ID: 11},
					{ID: 12},
				},
				jobs: map[string]*data.Job{},
			},
			want: []stackedGraphDataSets{},
		},
		{
			name: "jobs list with pipeline entries",
			args: args{
				entries: []data.PipelineEntry{
					{ID: 11},
					{ID: 12},
					{ID: 13},
				},
				jobs: map[string]*data.Job{
					"jobOne": {
						RawData: []data.JobEntry{
							{
								Duration:   142.1,
								PipelineID: 11,
							},
							{
								Duration:   100.1,
								PipelineID: 12,
							},
						},
					},
					"jobTwo": {
						RawData: []data.JobEntry{
							{
								Duration:   242.1,
								PipelineID: 11,
							},
							{
								Duration:   200.1,
								PipelineID: 13,
							},
						},
					},
					"jobThree": {
						RawData: []data.JobEntry{
							{
								Duration:   342.1,
								PipelineID: 12,
							},
						},
					},
				},
			},
			want: []stackedGraphDataSets{
				{
					Label:     "jobOne",
					Data:      "142.10,100.10,",
					Type:      "bar",
					PointSize: 1,
				},
				{
					Label:     "jobTwo",
					Data:      "242.10,,200.10",
					Type:      "bar",
					PointSize: 1,
				},
				{
					Label:     "jobThree",
					Data:      ",342.10,",
					Type:      "bar",
					PointSize: 1,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := getContinuesJobsDurationsPerPipeline(tt.args.entries, tt.args.jobs)

			// needs manual list matching since the map does not always get iterated in the same order
			if len(got) != len(tt.want) {
				t.Errorf("getContinuesJobsDurationsPerPipeline() returned list does not match expected list size, got: %d want: %d", len(got), len(tt.want))
			}
			matched := make([]bool, len(tt.want))
			for _, dataSet := range got {
				for i, wantSet := range tt.want {
					if matched[i] {
						// already found ignore
						continue
					}
					if reflect.DeepEqual(dataSet, wantSet) {
						matched[i] = true
					}
				}
			}
			for _, m := range matched {
				if !m {
					t.Errorf("getContinuesJobsDurationsPerPipeline() = %+v, want %+v", got, tt.want)
					break
				}
			}
		})
	}
}

func Test_createContinuousPipelineStatistics(t *testing.T) {
	type args struct {
		p    *data.Project
		jobs map[string]*data.Job
	}
	tests := []struct {
		name string
		args args
		want []*jobStatisticsRow
	}{
		{
			name: "empty table returns empty row",
			args: args{
				p:    &data.Project{},
				jobs: map[string]*data.Job{},
			},
			want: []*jobStatisticsRow{},
		},
		{
			name: "table with on entry returns expected row",
			args: args{
				p: &data.Project{},
				jobs: map[string]*data.Job{
					"JobOne": {
						RawData: []data.JobEntry{
							{
								Status: data.StatusSuccess,
							},
						},
					},
				},
			},
			want: []*jobStatisticsRow{
				{Name: "JobOne", TotalBuilds: 1},
			},
		},
		{
			name: "table with several entries returns expected rows",
			args: args{
				p: &data.Project{},
				jobs: map[string]*data.Job{
					"Job1": {},
					"Job2": {},
					"Job3": {},
				},
			},
			want: []*jobStatisticsRow{
				{Name: "Job1"},
				{Name: "Job2"},
				{Name: "Job3"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := createContinuousPipelineStatistics(tt.args.p, tt.args.jobs)
			sort.SliceStable(got, func(i, j int) bool {
				return strings.Compare(got[i].Name, got[j].Name) <= 0
			})
			testhelper.DeepEqual(t, got, tt.want)
		})
	}
}
