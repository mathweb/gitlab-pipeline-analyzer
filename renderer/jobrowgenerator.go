// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package renderer

import (
	"encoding/json"
	"fmt"
	"math"

	"gitlab.com/mathweb/gitlab-pipeline-analyzer/data"
)

type jobStatisticsRow struct {
	Name                   string  `json:"name"`
	TotalBuilds            int     `json:"totalBuilds"`
	FailedBuilds           int     `json:"failedBuilds"`
	FailedRate             float32 `json:"failedRate"`
	MinDurationSuccess     float64 `json:"minDurationSuccess"`
	MaxDurationSuccess     float64 `json:"maxDurationSuccess"`
	MaxDurationSuccessLink string  `json:"maxDurationSuccessLink"`
	AverageDurationSuccess float64 `json:"avgDurationSuccess"`
	MinDurationFailed      float64 `json:"minDurationFailed"`
	MaxDurationFailed      float64 `json:"maxDurationFailed"`
	MaxDurationFailedLink  string  `json:"maxDurationFailedLink"`
	AverageDurationFailed  float64 `json:"avgDurationFailed"`
	RetrySuccessRate       float64 `json:"retryRateSuccess"`
	AvgRetryOnFailedCnt    float64 `json:"avgRetryOnFailedCnt"`
	AvgQueueTime           float64 `json:"avgQueueTime"`
	MinQueueTime           float64 `json:"minQueueTime"`
	MaxQueueTime           float64 `json:"maxQueueTime"`
	MaxQueueTimeLink       string  `json:"maxQueueTimeLink"`
	MinArtifactSize        float64 `json:"minArtifactSize"`
	MaxArtifactSize        float64 `json:"maxArtifactSize"`
	MaxArtifactSizeLink    string  `json:"maxArtifactSizeLink"`
}

type jobRowGenerator struct {
	row       *jobStatisticsRow
	serverURL string

	totalDurationFailed  float64
	totalDurationSuccess float64
	totalQueueTime       float64
	minArtifactSize      float64
	maxArtifactSize      float64
	totalRetriedSuccess  map[int]int
	totalRetriedFailed   map[int]int
}

func createJobStatisticsTable(rows []*jobStatisticsRow, title string, name string) (string, error) {
	data := make([]string, 0, len(rows))
	for _, value := range rows {
		out, err := json.Marshal(value)
		if err != nil {
			return "", err
		}
		data = append(data, string(out))
	}

	return createTabulatorTable(tabulatorTableData{
		Title:    title,
		Name:     name,
		TitleRow: getJobRowTitleData(),
		Index:    "name",
		DataSets: data,
	})
}

func getJobRowTitleData() []tabulatorTitleEntry {
	return []tabulatorTitleEntry{
		{Title: "Name", Field: "name"},
		{Title: "Total Builds", Field: "totalBuilds"},
		{Title: "Failed Builds", Field: "failedBuilds"},
		{Title: "Failed Rate", Field: "failedRate"},
		{Title: "Successful Average Duration", Field: "avgDurationSuccess"},
		{Title: "Successful Minimum Duration", Field: "minDurationSuccess"},
		{Title: "Successful Maximum Duration", Field: "maxDurationSuccess"},
		{Title: "Successful Maximum Link", Field: "maxDurationSuccessLink", Formatter: "link"},
		{Title: "Failed Average Duration", Field: "avgDurationFailed"},
		{Title: "Failed Minimum Duration", Field: "minDurationFailed"},
		{Title: "Failed Maximum Duration", Field: "maxDurationFailed"},
		{Title: "Failed Maximum Link", Field: "maxDurationFailedLink", Formatter: "link"},
		{Title: "Successful with Retry Rate", Field: "retryRateSuccess"},
		{Title: "Avg Retry Cnt on Failed", Field: "avgRetryOnFailedCnt"},
		{Title: "Avg Queue Time", Field: "avgQueueTime"},
		{Title: "Min Queue Time", Field: "minQueueTime"},
		{Title: "Max Queue Time", Field: "maxQueueTime"},
		{Title: "Max Queue Time Link", Field: "maxQueueTimeLink", Formatter: "link"},
		{Title: "Min Artifact Size [MB]", Field: "minArtifactSize"},
		{Title: "Max Artifact Size [MB]", Field: "maxArtifactSize"},
		{Title: "Max Artifact Size Link", Field: "maxArtifactSizeLink", Formatter: "link"},
	}
}

func newJobRowGenerator(name string, serverURL string) *jobRowGenerator {
	return &jobRowGenerator{
		row: &jobStatisticsRow{
			Name:               name,
			MinDurationSuccess: math.Inf(1),
			MinDurationFailed:  math.Inf(1),
			MinQueueTime:       math.Inf(1),
		},

		serverURL: serverURL,

		totalDurationFailed:  0,
		totalDurationSuccess: 0,
		totalQueueTime:       0,
		minArtifactSize:      math.Inf(1),
		maxArtifactSize:      0,
		totalRetriedSuccess:  make(map[int]int, 0),
		totalRetriedFailed:   make(map[int]int, 0),
	}
}

func (j *jobRowGenerator) addEntry(e data.JobEntry) {
	switch e.Status {
	case data.StatusSuccess:
		if j.row.MaxDurationSuccess < e.Duration {
			j.row.MaxDurationSuccess = e.Duration
			j.row.MaxDurationSuccessLink = fmt.Sprintf("https://%s%s", j.serverURL, e.WebURL)
		}
		if j.row.MinDurationSuccess > e.Duration {
			j.row.MinDurationSuccess = e.Duration
		}
		if j.maxArtifactSize < e.ArtifactsSize {
			j.maxArtifactSize = e.ArtifactsSize
			j.row.MaxArtifactSizeLink = fmt.Sprintf("https://%s%s", j.serverURL, e.WebURL)
		}
		if j.minArtifactSize > e.ArtifactsSize {
			j.minArtifactSize = e.ArtifactsSize
		}
		j.totalRetriedSuccess[e.PipelineID]++
		j.totalDurationSuccess += e.Duration
	case data.StatusFailed:
		j.row.FailedBuilds++
		if j.row.MaxDurationFailed < e.Duration {
			j.row.MaxDurationFailed = e.Duration
			j.row.MaxDurationFailedLink = fmt.Sprintf("https://%s%s", j.serverURL, e.WebURL)
		}
		if j.row.MinDurationFailed > e.Duration {
			j.row.MinDurationFailed = e.Duration
		}
		if e.Retried {
			j.totalRetriedFailed[e.PipelineID]++
		}
		j.totalDurationFailed += e.Duration
	default:
		// ignore all others
		return
	}

	j.row.TotalBuilds++
	j.totalQueueTime += e.QueuedDuration

	if j.row.MinQueueTime > e.QueuedDuration {
		j.row.MinQueueTime = e.QueuedDuration
	}
	if j.row.MaxQueueTime < e.QueuedDuration {
		j.row.MaxQueueTime = e.QueuedDuration
		j.row.MaxQueueTimeLink = fmt.Sprintf("https://%s%s", j.serverURL, e.WebURL)
	}
}

func (j *jobRowGenerator) getFinalizedRowEntry() *jobStatisticsRow {
	if j.row.TotalBuilds > 0 {
		j.row.FailedRate = 100.0 / float32(j.row.TotalBuilds) * float32(j.row.FailedBuilds)
	}

	if j.totalQueueTime > 0 {
		j.row.AvgQueueTime = j.totalQueueTime / float64(j.row.TotalBuilds)
	} else {
		j.row.MinQueueTime = 0
	}

	if j.totalDurationSuccess != 0 && j.row.TotalBuilds != j.row.FailedBuilds {
		j.row.AverageDurationSuccess = j.totalDurationSuccess / float64(j.row.TotalBuilds-j.row.FailedBuilds)
	} else {
		j.row.MinDurationSuccess = 0
	}

	if j.totalDurationFailed != 0 && j.row.FailedBuilds != 0 {
		j.row.AverageDurationFailed = j.totalDurationFailed / float64(j.row.FailedBuilds)
	} else {
		j.row.MinDurationFailed = 0
	}

	if j.maxArtifactSize > 0 {
		j.row.MaxArtifactSize = j.maxArtifactSize / 1024. / 1024.
		j.row.MinArtifactSize = j.minArtifactSize / 1024. / 1024.
	}

	if len(j.totalRetriedFailed) > 0 {
		totalSuccessAfterRetry := 0
		totalRetriesOnFailed := 0
		for id := range j.totalRetriedFailed {
			_, ok := j.totalRetriedSuccess[id]
			if ok {
				totalSuccessAfterRetry++
			}
			totalRetriesOnFailed += j.totalRetriedFailed[id]
		}
		j.row.RetrySuccessRate = 100 / float64(totalRetriesOnFailed) * float64(totalSuccessAfterRetry)
		j.row.AvgRetryOnFailedCnt = float64(totalRetriesOnFailed) / float64(len(j.totalRetriedFailed))
	}

	return j.row
}
