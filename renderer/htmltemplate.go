// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package renderer

import (
	"io"
	"text/template"
)

const nullValue = "null"

const htmlReportTmpl = `<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<style>
		.summaries .graphgroup { 
			display: flex;
			flex-flow: wrap;
		}
		.graphgroup .graph {
			width: 50%;
		}
		</style>
		<script src=" https://cdn.jsdelivr.net/npm/echarts@5.4.3/dist/echarts.min.js "></script>
		<title>{{.Title}}</title>
	</head>
	<body>
		<h1>{{.Title}}</h1>
		<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
		<link href="https://unpkg.com/tabulator-tables@5.5.0/dist/css/tabulator.min.css" rel="stylesheet">
 		<script type="text/javascript" src="https://unpkg.com/tabulator-tables@5.5.2/dist/js/tabulator.min.js"></script>

		{{.Data}}

	</body>
</html>`

type pageReportData struct {
	Title string
	Data  string
}

func createHTMLReport(title string, data string, o io.Writer) error {
	pageTemplate, err := template.New("webpage").Parse(htmlReportTmpl)
	if err != nil {
		return err
	}

	pageData := pageReportData{
		Title: title,
		Data:  data,
	}

	return pageTemplate.Execute(o, pageData)
}
