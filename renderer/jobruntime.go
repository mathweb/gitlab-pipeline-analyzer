// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package renderer

import (
	"encoding/json"
	"fmt"
	"io"
	"math"
	"strings"

	"gitlab.com/mathweb/gitlab-pipeline-analyzer/data"
)

func CreateHTMLJobReportPipelineData(p *data.PipelineDataCollection, jobName string, o io.Writer) error {
	data, err := createHTMLJobReport(p.Project, p.Jobs[jobName], "")
	if err != nil {
		return err
	}

	title := fmt.Sprintf("%s - %s - Job Data", p.Project.Name, jobName)
	return createHTMLReport(title, data, o)
}

func CreateHTMLReportJobData(j *data.JobDataCollection, o io.Writer) error {
	data, err := createHTMLJobReport(j.Project, &j.Job, j.DefaultBranch)
	if err != nil {
		return err
	}

	title := fmt.Sprintf("%s - %s - Job Data", j.Project.Name, j.JobName)
	return createHTMLReport(title, data, o)
}

func createHTMLJobReport(p data.Project, j *data.Job, defaultBranch string) (string, error) {
	continuous, err := createContinuousJobGraph(p, j.RawData, defaultBranch)
	if err != nil {
		return "", err
	}
	continuousStatistics, err := createContinuousStatisticsTable(p, j.RawData)
	if err != nil {
		return "", err
	}
	continuousBuildStepsStatistics, err := createContinuousBuildStepsStatisticsTable(p, j.RawData)
	if err != nil {
		return "", err
	}
	daily, err := createSummaryRuntimeJobGraph(j.DaySummaries, "Daily", "Days")
	if err != nil {
		return "", err
	}
	dailyStatistics, err := createSummaryBuildStatisticJobGraph(j.DaySummaries, "Daily", "Day")
	if err != nil {
		return "", err
	}
	weekly, err := createSummaryRuntimeJobGraph(j.WeekSummaries, "Weekly", "Week")
	if err != nil {
		return "", err
	}
	weeklyStatistics, err := createSummaryBuildStatisticJobGraph(j.WeekSummaries, "Weekly", "Week")
	if err != nil {
		return "", err
	}
	monthly, err := createSummaryRuntimeJobGraph(j.MonthSummaries, "Monthly", "Month")
	if err != nil {
		return "", err
	}
	monthlyStatistics, err := createSummaryBuildStatisticJobGraph(j.MonthSummaries, "Monthly", "Month")
	if err != nil {
		return "", err
	}

	data := strings.Join([]string{continuous, continuousStatistics, continuousBuildStepsStatistics, daily, dailyStatistics, weekly, weeklyStatistics, monthly, monthlyStatistics}, "\n")
	return data, nil
}

func createContinuousJobGraph(p data.Project, entries []data.JobEntry, defaultBranch string) (string, error) {
	durationDataSet := getContinuousJobGraphDurationDataSet(entries)
	queueDurationDataSet := getContinuousJobQueueDurationDataSet(entries)
	sectionDurationDataSets := getContinuousJobSectionGraphDurationDataSet(entries)
	durationDefaultBranchDataSet := getContinuousJobGraphDefaultBranchDataSet(entries, defaultBranch)
	artifactsSizeDataSet := getContinuousJobGraphArtifactsDataSet(entries)
	statusDataSet := getContinuousJobGraphStatusDataSet(entries)
	links := getContinuousJobLinks(p, entries)
	labels := getContinuousJobLabels(entries)

	dataSets := []stackedGraphDataSets{durationDataSet, statusDataSet, durationDefaultBranchDataSet, artifactsSizeDataSet, queueDurationDataSet}
	dataSets = append(dataSets, sectionDurationDataSets...)

	pageData := stackedGraphData{
		Title:           "Continuous",
		XTitle:          "Job ID",
		YTitle:          "Duration [s]",
		Links:           links,
		Name:            "Continuous",
		ReferenceLabels: labels,
		Stacked:         true,
		DataSets:        dataSets,
		AdditionalYAxis: []yAxis{
			{Name: "ArtifactsSize", Title: "Artifacts Size [MB]", Stacked: false},
		},
	}

	return createStackedGraph(pageData)
}

func createContinuousStatisticsTable(p data.Project, entries []data.JobEntry) (string, error) {
	statistics := createContinuousStatistics(p, entries)

	return createJobStatisticsTable(statistics, "Continuous Statistics", "cstatistics")
}

func createSummaryRuntimeJobGraph(summaries []data.Summary, titleName string, xAxis string) (string, error) {
	durationDataSet := getCumulatedJobDurationGraphDataSet(summaries)
	labels := getCumulatedJobLabels(summaries)

	pageData := stackedGraphData{
		Title:           fmt.Sprintf("%s Duration", titleName),
		XTitle:          xAxis,
		YTitle:          "Duration [s]",
		Links:           "",
		Name:            fmt.Sprintf("%sDurations", titleName),
		ReferenceLabels: labels,
		Stacked:         false,
		DataSets:        durationDataSet,
	}

	return createStackedGraph(pageData)
}

func createSummaryBuildStatisticJobGraph(summaries []data.Summary, titleName string, xAxis string) (string, error) {
	statisticsDataSet := getCumulatedJobStatisticsGraphDataSet(summaries)
	labels := getCumulatedJobLabels(summaries)

	pageData := stackedGraphData{
		Title:           fmt.Sprintf("%s Counters", titleName),
		XTitle:          xAxis,
		YTitle:          "Count",
		Links:           "",
		Name:            fmt.Sprintf("%sStatistics", titleName),
		ReferenceLabels: labels,
		Stacked:         false,
		DataSets:        statisticsDataSet,
		AdditionalYAxis: []yAxis{
			{Name: "Count", Title: "Test Count", Stacked: false},
			{Name: "ArtifactSize", Title: "Artifact Size [MB]", Stacked: false},
		},
	}

	return createStackedGraph(pageData)
}

func getCumulatedJobDurationGraphDataSet(data []data.Summary) []stackedGraphDataSets {
	avgData := make([]string, len(data))
	minData := make([]string, len(data))
	maxData := make([]string, len(data))
	for i, d := range data {
		avgData[i] = fmt.Sprintf("%.2f", d.Average)
		minData[i] = fmt.Sprintf("%.2f", d.Min)
		maxData[i] = fmt.Sprintf("%.2f", d.Max)
	}
	avg := strings.Join(avgData, ",")
	min := strings.Join(minData, ",")
	max := strings.Join(maxData, ",")
	return []stackedGraphDataSets{
		{
			Label:     "Average",
			Data:      avg,
			Type:      "line",
			PointSize: 1,
		},
		{
			Label:     "Minimum",
			Data:      min,
			Type:      "line",
			PointSize: 1,
		},
		{
			Label:     "Maximum",
			Data:      max,
			Type:      "line",
			PointSize: 1,
		},
	}
}

func getCumulatedJobStatisticsGraphDataSet(data []data.Summary) []stackedGraphDataSets {
	totalCntData := make([]string, len(data))
	successCntData := make([]string, len(data))
	testCndData := make([]string, len(data))
	artifactsSizeData := make([]string, len(data))
	for i, d := range data {
		totalCntData[i] = fmt.Sprintf("%d", d.TotalCnt)
		successCntData[i] = fmt.Sprintf("%d", d.SuccessCnt)
		testCndData[i] = fmt.Sprintf("%d", d.TestCnt)
		artifactsSizeData[i] = fmt.Sprintf("%.0f", d.ArtifactsSizeAverage/1024./1024.)
	}
	totalCnt := strings.Join(totalCntData, ",")
	successCnt := strings.Join(successCntData, ",")
	testCnt := strings.Join(testCndData, ",")
	artifactsSize := strings.Join(artifactsSizeData, ",")
	return []stackedGraphDataSets{
		{
			Label:     "Total Builds",
			Data:      totalCnt,
			Type:      "line",
			PointSize: 1,
		},
		{
			Label:     "Successful Builds",
			Data:      successCnt,
			Type:      "line",
			PointSize: 1,
		},
		{
			Label:     "Tests Count",
			Data:      testCnt,
			Type:      "line",
			YAxisName: "Count",
			PointSize: 1,
		},
		{
			Label:     "Artifacts Size [MB]",
			Data:      artifactsSize,
			Type:      "line",
			YAxisName: "ArtifactSize",
			PointSize: 1,
		},
	}
}

func getCumulatedJobLabels(data []data.Summary) string {
	days := make([]string, 0, len(data))
	for _, d := range data {
		days = append(days, fmt.Sprintf("%d.%d", d.Date.Day(), d.Date.Month()))
	}
	return strings.Join(days, ",")
}

func getContinuousJobGraphDurationDataSet(entries []data.JobEntry) stackedGraphDataSets {
	values := make([]string, len(entries))

	for i, d := range entries {
		values[i] = fmt.Sprintf("%.2f", d.Duration)
	}

	dur := strings.Join(values, ",")
	return stackedGraphDataSets{
		Label:     "Duration [s]",
		Data:      dur,
		Type:      "bar",
		PointSize: 2,
	}
}

func getContinuousJobQueueDurationDataSet(entries []data.JobEntry) stackedGraphDataSets {
	values := make([]string, len(entries))

	for i, d := range entries {
		values[i] = fmt.Sprintf("%.2f", d.QueuedDuration)
	}

	dur := strings.Join(values, ",")
	return stackedGraphDataSets{
		Label:      "Queued Duration [s]",
		StackGroup: "queued",
		Data:       dur,
		Type:       "line",
		PointSize:  1,
	}
}

func getContinuousJobGraphArtifactsDataSet(entries []data.JobEntry) stackedGraphDataSets {
	values := make([]string, len(entries))

	for i, d := range entries {
		values[i] = fmt.Sprintf("%.f", d.ArtifactsSize/1024./1024.)
	}

	dur := strings.Join(values, ",")
	return stackedGraphDataSets{
		Label:     "Artifacts Size [MB]",
		Data:      dur,
		Type:      "line",
		PointSize: 1,
		YAxisName: "ArtifactsSize",
	}
}

func getContinuousJobSectionGraphDurationDataSet(entries []data.JobEntry) []stackedGraphDataSets {
	values := make(map[string][]string, 0)

	for i, d := range entries {
		for _, s := range d.BuildSections {
			v, ok := values[s.Title]
			if !ok {
				v = make([]string, len(entries))
			}
			v[i] = fmt.Sprintf("%.2f", s.End.Sub(s.Start).Seconds())
			values[s.Title] = v
		}
	}
	dataSets := make([]stackedGraphDataSets, 0, len(values))
	for k, v := range values {
		dur := strings.Join(v, ",")
		dataSets = append(dataSets, stackedGraphDataSets{
			Label:      fmt.Sprintf("%s Section [s]", k),
			Data:       dur,
			Type:       "bar",
			StackGroup: "sections",
			PointSize:  1,
		})
	}
	return dataSets
}

func getContinuousJobGraphDefaultBranchDataSet(entries []data.JobEntry, branchName string) stackedGraphDataSets {
	values := make([]string, len(entries))

	for i, d := range entries {
		if d.Ref == branchName {
			values[i] = fmt.Sprintf("%.2f", d.Duration)
		} else {
			values[i] = nullValue
		}
	}

	dur := strings.Join(values, ",")
	return stackedGraphDataSets{
		Label:     fmt.Sprintf("%s branch Duration [s]", branchName),
		Data:      dur,
		Type:      "line",
		PointSize: 2,
	}
}

func getContinuousJobGraphStatusDataSet(entries []data.JobEntry) stackedGraphDataSets {
	values := make([]string, len(entries))

	for i, d := range entries {
		if strings.ToUpper(d.Status) != data.StatusSuccess {
			values[i] = "0"
		} else {
			values[i] = nullValue
		}
	}

	fail := strings.Join(values, ",")
	return stackedGraphDataSets{
		Label:     "Job Failed",
		Data:      fail,
		Type:      "bubble",
		PointSize: 10,
	}
}

func getContinuousJobLabels(entries []data.JobEntry) string {
	labels := make([]string, len(entries))
	for i, e := range entries {
		labels[i] = fmt.Sprintf("'%s - %d'", e.RunnerName, e.ID)
	}
	return strings.Join(labels, ",")
}

func getContinuousJobLinks(p data.Project, entries []data.JobEntry) string {
	links := make([]string, len(entries))
	for i, e := range entries {
		links[i] = fmt.Sprintf("\"https://%s/%s/-/jobs/%d\"", p.Server, p.FullPath, e.ID)
	}
	return strings.Join(links, ",")
}

func createContinuousStatistics(p data.Project, entries []data.JobEntry) []*jobStatisticsRow {
	gens := make(map[string]*jobRowGenerator)
	for _, e := range entries {
		gen, ok := gens[e.Ref]
		if !ok {
			gen = newJobRowGenerator(e.Ref, p.Server)
		}
		gen.addEntry(e)
		gens[e.Ref] = gen
	}

	statistics := make([]*jobStatisticsRow, 0, len(gens))
	for _, g := range gens {
		statistics = append(statistics, g.getFinalizedRowEntry())
	}

	return statistics
}

type jobBuildSectionRow struct {
	Name             string  `json:"name"`
	MinDuration      float64 `json:"minDuration"`
	MinDurationAgent string  `json:"minDurationAgent"`
	MinDurationLink  string  `json:"minDurationLink"`
	MaxDuration      float64 `json:"maxDuration"`
	MaxDurationAgent string  `json:"maxDurationAgent"`
	MaxDurationLink  string  `json:"maxDurationLink"`
	AvgDuration      float64 `json:"avgDuration"`
}

func createContinuousBuildStepsStatisticsTable(p data.Project, entries []data.JobEntry) (string, error) {
	rows := getBuildSectionRows(p.Server, entries)
	return createJobBuildSectionsTable(rows, "Build Section Data", "buildSections")
}

func createJobBuildSectionsTable(rows []jobBuildSectionRow, title string, name string) (string, error) {
	data := make([]string, 0, len(rows))
	for _, value := range rows {
		out, err := json.Marshal(value)
		if err != nil {
			return "", err
		}
		data = append(data, string(out))
	}

	return createTabulatorTable(tabulatorTableData{
		Title:    title,
		Name:     name,
		TitleRow: getBuildSectionTitleData(),
		Index:    "name",
		DataSets: data,
	})
}

func getBuildSectionRows(server string, entries []data.JobEntry) []jobBuildSectionRow {
	rowsMap := make(map[string]jobBuildSectionRow, 0)
	sectionTotalDuration := make(map[string]float64, 0)
	sectionCount := make(map[string]int64, 0)
	for _, e := range entries {
		for _, s := range e.BuildSections {
			row, ok := rowsMap[s.Title]
			if !ok {
				row = jobBuildSectionRow{
					Name:        s.Title,
					MinDuration: math.Inf(1),
				}
			}
			total := sectionTotalDuration[s.Title]
			count := sectionCount[s.Title]

			dur := s.End.Sub(s.Start).Seconds()
			total += dur
			count++
			if row.MinDuration > dur {
				row.MinDuration = dur
				row.MinDurationAgent = e.RunnerName
				row.MinDurationLink = fmt.Sprintf("https://%s%s", server, e.WebURL)
			}
			if row.MaxDuration < dur {
				row.MaxDuration = dur
				row.MaxDurationAgent = e.RunnerName
				row.MaxDurationLink = fmt.Sprintf("https://%s%s", server, e.WebURL)
			}

			rowsMap[s.Title] = row
			sectionTotalDuration[s.Title] = total
			sectionCount[s.Title] = count
		}
	}

	rows := make([]jobBuildSectionRow, 0, len(rowsMap))
	for key, r := range rowsMap {
		if sectionCount[key] > 0 {
			r.AvgDuration = sectionTotalDuration[key] / float64(sectionCount[key])
		}
		rows = append(rows, r)
	}
	return rows
}

func getBuildSectionTitleData() []tabulatorTitleEntry {
	return []tabulatorTitleEntry{
		{Title: "Name", Field: "name"},
		{Title: "Average Duration", Field: "avgDuration"},
		{Title: "Minimum Duration", Field: "minDuration"},
		{Title: "Maximum Duration", Field: "maxDuration"},
		{Title: "Minimum Agent Name", Field: "minDurationAgent"},
		{Title: "Maximum Agent Name", Field: "maxDurationAgent"},
		{Title: "Minimum Link", Field: "minDurationLink", Formatter: "link"},
		{Title: "Maximum Link", Field: "maxDurationLink", Formatter: "link"},
	}
}
