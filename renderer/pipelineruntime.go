// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package renderer

import (
	"fmt"
	"io"
	"strings"

	"gitlab.com/mathweb/gitlab-pipeline-analyzer/data"
)

func CreateHTMLReportPipelineData(p *data.PipelineDataCollection, o io.Writer) error {
	continuous, err := createContinuousPipelineGraph(p)
	if err != nil {
		return err
	}
	continuousStatistics, err := createContinuousPipelineStatisticsTable(p)
	if err != nil {
		return err
	}

	daily, err := createSummaryRuntimeJobGraph(p.Pipelines.DaySummaries, "Daily", "Days")
	if err != nil {
		return err
	}
	dailyStatistics, err := createSummaryBuildStatisticJobGraph(p.Pipelines.DaySummaries, "Daily", "Day")
	if err != nil {
		return err
	}
	weekly, err := createSummaryRuntimeJobGraph(p.Pipelines.WeekSummaries, "Weekly", "Week")
	if err != nil {
		return err
	}
	weeklyStatistics, err := createSummaryBuildStatisticJobGraph(p.Pipelines.WeekSummaries, "Weekly", "Week")
	if err != nil {
		return err
	}
	monthly, err := createSummaryRuntimeJobGraph(p.Pipelines.MonthSummaries, "Monthly", "Month")
	if err != nil {
		return err
	}
	monthlyStatistics, err := createSummaryBuildStatisticJobGraph(p.Pipelines.MonthSummaries, "Monthly", "Month")
	if err != nil {
		return err
	}

	data := strings.Join([]string{continuous, continuousStatistics, daily, dailyStatistics, weekly, weeklyStatistics, monthly, monthlyStatistics}, "\n")

	title := fmt.Sprintf("%s - Raw Pipeline Data", p.Project.Name)
	return createHTMLReport(title, data, o)
}

func createContinuousPipelineGraph(p *data.PipelineDataCollection) (string, error) {
	durationDataSet := getContinuousPipelineGraphDurationDataSet(p.Pipelines.RawData)
	artifactsSizeDataSet := getContinuousPipelineGraphArtifactsDataSet(p.Pipelines.RawData)
	jobBuildTimes := getContinuesJobsDurationsPerPipeline(p.Pipelines.RawData, p.Jobs)
	statusDataSet := getContinuousPipelineGraphStatusDataSet(p.Pipelines.RawData)
	links := getContinuousPipelineLinks(p.Project, p.Pipelines.RawData)
	labels := getContinuousPipelineLabels(p.Pipelines.RawData)
	dataSets := append([]stackedGraphDataSets{durationDataSet, artifactsSizeDataSet, statusDataSet}, jobBuildTimes...)

	pageData := stackedGraphData{
		Title:           "Continuous",
		XTitle:          "Pipeline ID",
		YTitle:          "Duration [s]",
		Links:           links,
		Name:            "Continuous",
		ReferenceLabels: labels,
		Stacked:         true,
		DataSets:        dataSets,
		AdditionalYAxis: []yAxis{
			{Name: "ArtifactsSize", Title: "Artifacts Size [MB]", Stacked: false},
		},
	}

	return createStackedGraph(pageData)
}

func createContinuousPipelineStatisticsTable(p *data.PipelineDataCollection) (string, error) {
	statistics := createContinuousPipelineStatistics(&p.Project, p.Jobs)

	return createJobStatisticsTable(statistics, "Continuous Statistics", "cstatistics")
}

func createContinuousPipelineStatistics(p *data.Project, jobs map[string]*data.Job) []*jobStatisticsRow {
	statistics := make([]*jobStatisticsRow, 0, len(jobs))
	for name, j := range jobs {
		gen := newJobRowGenerator(name, p.Server)
		for _, e := range j.RawData {
			gen.addEntry(e)
		}

		statistics = append(statistics, gen.getFinalizedRowEntry())
	}
	return statistics
}

func getContinuesJobsDurationsPerPipeline(entries []data.PipelineEntry, jobs map[string]*data.Job) []stackedGraphDataSets {
	entriesMap := make(map[int]int)
	for pos, e := range entries {
		entriesMap[e.ID] = pos
	}
	dataSets := make([]stackedGraphDataSets, 0, len(jobs))
	for name, j := range jobs {
		dataSets = append(dataSets, getContinuesJobDurationsPerPipeline(entriesMap, j, name))
	}
	return dataSets
}

func getContinuesJobDurationsPerPipeline(entries map[int]int, job *data.Job, jobName string) stackedGraphDataSets {
	values := make([]string, len(entries))
	for _, j := range job.RawData {
		if pos, ok := entries[j.PipelineID]; ok {
			values[pos] = fmt.Sprintf("%.2f", j.Duration)
		}
	}
	dur := strings.Join(values, ",")
	return stackedGraphDataSets{
		Label:     jobName,
		Data:      dur,
		Type:      "bar",
		PointSize: 1,
	}
}

func getContinuousPipelineGraphDurationDataSet(entries []data.PipelineEntry) stackedGraphDataSets {
	values := make([]string, len(entries))

	for i, d := range entries {
		values[i] = fmt.Sprintf("%.2f", d.Duration)
	}

	dur := strings.Join(values, ",")
	return stackedGraphDataSets{
		Label:     "Duration [s]",
		Data:      dur,
		Type:      "line",
		PointSize: 2,
	}
}

func getContinuousPipelineGraphArtifactsDataSet(entries []data.PipelineEntry) stackedGraphDataSets {
	values := make([]string, len(entries))

	for i, d := range entries {
		values[i] = fmt.Sprintf("%.f", d.ArtifactsSize/1024./1024.)
	}

	dur := strings.Join(values, ",")
	return stackedGraphDataSets{
		Label:     "Artifacts Size [MB]",
		Data:      dur,
		Type:      "line",
		PointSize: 1,
		YAxisName: "ArtifactsSize",
	}
}

func getContinuousPipelineGraphStatusDataSet(entries []data.PipelineEntry) stackedGraphDataSets {
	values := make([]string, len(entries))

	for i, d := range entries {
		if strings.ToUpper(d.Status) != data.StatusSuccess {
			values[i] = "0"
		} else {
			values[i] = nullValue
		}
	}

	fail := strings.Join(values, ",")
	return stackedGraphDataSets{
		Label:     "Pipeline Failed",
		Data:      fail,
		Type:      "bubble",
		PointSize: 10,
	}
}

func getContinuousPipelineLinks(p data.Project, entries []data.PipelineEntry) string {
	links := make([]string, len(entries))
	for i, e := range entries {
		links[i] = fmt.Sprintf("\"https://%s/%s\"", p.Server, e.WebURL)
	}
	return strings.Join(links, ",")
}

func getContinuousPipelineLabels(entries []data.PipelineEntry) string {
	labels := make([]string, len(entries))
	for i, e := range entries {
		labels[i] = e.PublicID
	}
	return strings.Join(labels, ",")
}
