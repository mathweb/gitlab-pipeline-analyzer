// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package renderer

import (
	"strings"
	"text/template"
)

type yAxis struct {
	Name    string
	Title   string
	Stacked bool
}

type stackedGraphData struct {
	Title           string
	XTitle          string
	YTitle          string
	Links           string
	Name            string
	ReferenceLabels string
	Stacked         bool
	DataSets        []stackedGraphDataSets
	AdditionalYAxis []yAxis
}

type stackedGraphDataSets struct {
	Label      string
	Data       string
	Type       string
	StackGroup string
	YAxisName  string
	PointSize  int
}

func createStackedGraph(data stackedGraphData) (string, error) {
	jobGraphTemplate, err := template.New("webpage").Parse(pipelineGraphBuildsTmpl)
	if err != nil {
		return "", err
	}

	graph := &strings.Builder{}
	err = jobGraphTemplate.Execute(graph, data)
	return graph.String(), err
}

const pipelineGraphBuildsTmpl = `
		<div class="graph">
			<canvas id="{{.Name}}"></canvas>
			<script>
		  		const pipelineLinks{{.Name}}=[{{.Links}}]
				const {{.Name}} = document.getElementById('{{.Name}}');
				const chart{{.Name}} = new Chart({{.Name}}, {
					type: 'line',
					data: {
						labels: [ {{.ReferenceLabels}} ],
						datasets: [
							{{range .DataSets}} 
							{
								type: '{{.Type}}',
								label: '{{ .Label }}',
								data: [ {{ .Data }} ],
								{{ if ne .StackGroup "" }}
								stack: '{{.StackGroup}}',
								{{ end }}
								yAxisID: 'y{{.YAxisName}}',
								pointRadius: {{.PointSize}},
							},
							{{end}}
						]
					},
					options: {
						responsive: true,
						spanGaps: true,
						interaction: {
							mode: 'index',
							intersect: false
						},
						plugins: {
							title: {
								display: true,
								text: '{{.Title}}',
							},
						},
						scales: {
							y: {
								beginAtZero: true,
								stacked: {{.Stacked}},
								title: {
									display: true,
									text: '{{.YTitle}}'
								}
							},
							{{range .AdditionalYAxis}}
							y{{.Name}}: {
								beginAtZero: true,
								stacked: {{.Stacked}},
								grid: {
									drawOnChartArea: false // only want the grid lines for the first axis to show up
								},
								title: {
									display: true,
									text: '{{.Title}}',
								},
							},
							{{end}}
							x: {
								stacked: true,
								title: {
									display: {{.Stacked}},
									text: '{{.XTitle}}'
								},
							},
						},
						{{ if ne .Links "" }}
						onClick: (e) => {
							const canvasPosition = Chart.helpers.getRelativePosition(e, chart{{.Name}});
		
							// Substitute the appropriate scale IDs
							const dataX = chart{{.Name}}.scales.x.getValueForPixel(canvasPosition.x);
							console.log("Got: " + dataX);
							console.log("Got: " + pipelineLinks{{.Name}}[dataX]);
							window.open(pipelineLinks{{.Name}}[dataX], '_blank');
						}
						{{ end }}
					}	
				});
			</script>
		</div>
`
