// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package renderer

import (
	"regexp"
	"sort"
	"strings"
	"testing"

	"github.com/fatih/structs"
	"gitlab.com/mathweb/gitlab-pipeline-analyzer/data"
	"gitlab.com/mathweb/gitlab-pipeline-analyzer/internal/testhelper"
)

func Test_getJobRowTitleData(t *testing.T) {
	// test that all json elements have an associated row title
	titles := getJobRowTitleData()
	rowEntry := &jobStatisticsRow{}

	rowFields := structs.Fields(rowEntry)

	rowFieldTags := make([]string, 0, len(rowFields))
	titleFields := make([]string, 0, len(titles))

	for _, f := range rowFields {
		rowFieldTags = append(rowFieldTags, f.Tag("json"))
	}
	for _, e := range titles {
		titleFields = append(titleFields, e.Field)
	}

	sort.SliceStable(rowFieldTags, func(i, j int) bool {
		return strings.Compare(rowFieldTags[i], rowFieldTags[j]) <= 0
	})
	sort.SliceStable(titleFields, func(i, j int) bool {
		return strings.Compare(titleFields[i], titleFields[j]) <= 0
	})

	testhelper.DeepEqual(t, rowFieldTags, titleFields)
}

func Test_createJobStatisticsTable(t *testing.T) {
	type args struct {
		rows  []*jobStatisticsRow
		title string
		name  string
	}
	tests := []struct {
		name        string
		args        args
		wantMatches []string
		wantErr     bool
	}{
		{
			name: "empty row list",
			args: args{
				rows:  []*jobStatisticsRow{},
				title: "Demo Table",
				name:  "demotable-id",
			},
			wantMatches: []string{
				"<h2>Demo Table",
				"#demotable-id",
				"data:\\[\\],",
			},
			wantErr: false,
		},
		{
			name: "with given data rows",
			args: args{
				rows: []*jobStatisticsRow{
					{
						Name:                   "Demo",
						MaxDurationSuccessLink: "https://linkMaxSuccess",
						MaxDurationFailedLink:  "https://linkMaxSuccess",
					},
				},
				title: "Demo Table",
				name:  "dtable",
			},
			wantMatches: []string{
				"<h2>Demo Table",
				"\\{\"name\":\"Demo\"",
				"\"maxDurationSuccessLink\":\"https://linkMaxSuccess\"",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := createJobStatisticsTable(tt.args.rows, tt.args.title, tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("createJobStatisticsTable() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			for _, m := range tt.wantMatches {
				r := regexp.MustCompile(m)
				if !r.MatchString(got) {
					t.Errorf("createJobStatisticsTable() the regex '%s' did not match\n%s", m, got)
				}
			}
		})
	}
}

func Test_jobRowGenerator(t *testing.T) {
	type args struct {
		name      string
		serverURL string
		entries   []data.JobEntry
	}
	tests := []struct {
		name string
		args args
		want *jobStatisticsRow
	}{
		{
			name: "Add no entries and receive empty result list",
			args: args{
				name:      "Demo",
				serverURL: "demo.ch",
				entries:   []data.JobEntry{},
			},
			want: &jobStatisticsRow{
				Name:                   "Demo",
				TotalBuilds:            0,
				FailedBuilds:           0,
				FailedRate:             0,
				AverageDurationSuccess: 0,
				MinDurationSuccess:     0,
				MaxDurationSuccess:     0,
				MaxDurationSuccessLink: "",
				AverageDurationFailed:  0,
				MinDurationFailed:      0,
				MaxDurationFailed:      0,
				MaxDurationFailedLink:  "",
				RetrySuccessRate:       0,
				AvgRetryOnFailedCnt:    0,
				AvgQueueTime:           0,
				MinQueueTime:           0,
				MaxQueueTime:           0,
				MaxQueueTimeLink:       "",
				MinArtifactSize:        0,
				MaxArtifactSize:        0,
				MaxArtifactSizeLink:    "",
			},
		},
		{
			name: "Create row for given job entries",
			args: args{
				name:      "JobName",
				serverURL: "demo.gitlab.com",
				entries: []data.JobEntry{
					{
						Status:         data.StatusSuccess,
						PipelineID:     1,
						Duration:       100,
						QueuedDuration: 20,
						Retried:        true,
						ArtifactsSize:  42000000.,
						WebURL:         "/irelevant",
					},
					{
						Status:         data.StatusSuccess,
						PipelineID:     2,
						Duration:       50,
						QueuedDuration: 30,
						ArtifactsSize:  32000000.,
						Retried:        true,
						WebURL:         "/irelevant",
					},
					{
						Status:         data.StatusSuccess,
						PipelineID:     3,
						Duration:       150,
						QueuedDuration: 10,
						ArtifactsSize:  52000000.,
						Retried:        false,
						WebURL:         "/url-max-success",
					},
					{
						Status:         data.StatusFailed,
						PipelineID:     3,
						Duration:       200,
						QueuedDuration: 30,
						ArtifactsSize:  10000,
						Retried:        true,
						WebURL:         "/irelevant",
					},
					{
						Status:         data.StatusFailed,
						PipelineID:     3,
						Duration:       150,
						QueuedDuration: 20,
						ArtifactsSize:  5550000000,
						Retried:        true,
						WebURL:         "/irelevant",
					},
					{
						Status:         data.StatusFailed,
						PipelineID:     4,
						Duration:       250,
						QueuedDuration: 40,
						ArtifactsSize:  0,
						Retried:        true,
						WebURL:         "/url-max-failed",
					},
				},
			},
			want: &jobStatisticsRow{
				Name:                   "JobName",
				TotalBuilds:            6,
				FailedBuilds:           3,
				FailedRate:             50.,
				MinDurationSuccess:     50,
				MaxDurationSuccess:     150,
				MaxDurationSuccessLink: "https://demo.gitlab.com/url-max-success",
				RetrySuccessRate:       100. / 3,
				AverageDurationSuccess: 100,
				MinDurationFailed:      150,
				MaxDurationFailed:      250,
				MaxDurationFailedLink:  "https://demo.gitlab.com/url-max-failed",
				AverageDurationFailed:  200,
				AvgRetryOnFailedCnt:    1.5,
				AvgQueueTime:           25,
				MinQueueTime:           10,
				MaxQueueTime:           40,
				MaxQueueTimeLink:       "https://demo.gitlab.com/url-max-failed",
				MinArtifactSize:        32000000. / 1024 / 1024,
				MaxArtifactSize:        52000000. / 1024 / 1024,
				MaxArtifactSizeLink:    "https://demo.gitlab.com/url-max-success",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gen := newJobRowGenerator(tt.args.name, tt.args.serverURL)
			for _, e := range tt.args.entries {
				gen.addEntry(e)
			}
			res := gen.getFinalizedRowEntry()
			testhelper.DeepEqual(t, res, tt.want)
		})
	}
}
