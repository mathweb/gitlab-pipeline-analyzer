// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package renderer

import (
	"strings"
	"text/template"
)

type tabulatorTableData struct {
	Title    string
	Name     string
	Stacked  bool
	TitleRow []tabulatorTitleEntry
	DataSets []string
	// Index specify the unique id of a data set (Default: id)
	Index string
}

type tabulatorTitleEntry struct {
	Title     string
	Field     string
	Formatter string
}

func createTabulatorTable(data tabulatorTableData) (string, error) {
	if data.Index == "" {
		data.Index = "id"
	}

	tableTemplate, err := template.New("tabulator").Parse(tabulatorTableTmpl)
	if err != nil {
		return "", err
	}

	graph := &strings.Builder{}
	err = tableTemplate.Execute(graph, data)
	return graph.String(), err
}

const tabulatorTableTmpl = `
		<h2>{{.Title}}</h2>
		<div id="{{.Name}}-table"></div>
		<button id="{{.Name}}-download-csv">Download CSV</button>
		<script>
		var {{.Name}}_table = new Tabulator("#{{.Name}}-table", {
			height:500, // set height of table (in CSS or here), this enables the Virtual DOM and improves render speed dramatically (can be any valid css height value)
			data:[{{range .DataSets}}
				{{.}},
			{{end}}], 
			layout:"fitColumns", //fit columns to width of table (optional)
			columns:[ 
				{{range .TitleRow}}
				{
					title:"{{.Title}}", 
					field:"{{.Field}}",
					{{ if ne .Formatter "" }}
					formatter:"{{.Formatter}}",
					{{end}}
				},
				{{end}}
			],
			index:"{{.Index}}",
		});

		document.getElementById("{{.Name}}-download-csv").addEventListener("click", function(){
			{{.Name}}_table.download("csv", "{{.Title}}-{{.Name}}-data.csv");
		});
		</script>
`
