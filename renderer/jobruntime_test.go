// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package renderer

import (
	"regexp"
	"sort"
	"strings"
	"testing"
	"time"

	"github.com/fatih/structs"
	"gitlab.com/mathweb/gitlab-pipeline-analyzer/data"
	"gitlab.com/mathweb/gitlab-pipeline-analyzer/internal/testhelper"
)

func Test_getContinuousJobLinks(t *testing.T) {
	type args struct {
		p       data.Project
		entries []data.JobEntry
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "Empty Data",
			args: args{
				p: data.Project{
					Server:   "code.demoserver.com",
					FullPath: "path/name",
				},
				entries: []data.JobEntry{},
			},
			want: "",
		},
		{
			name: "Link is properly created from one job entry",
			args: args{
				p: data.Project{
					Server:   "code.demoserver.com",
					FullPath: "path/name",
				},
				entries: []data.JobEntry{
					{
						ID: 123,
					},
				},
			},
			want: "\"https://code.demoserver.com/path/name/-/jobs/123\"",
		},
		{
			name: "Links are properly created from multiple job entries",
			args: args{
				p: data.Project{
					Server:   "code.demoserver.com",
					FullPath: "path/name",
				},
				entries: []data.JobEntry{
					{
						ID: 123,
					},
					{
						ID: 231,
					},
					{
						ID: 312,
					},
				},
			},
			want: "\"https://code.demoserver.com/path/name/-/jobs/123\",\"https://code.demoserver.com/path/name/-/jobs/231\",\"https://code.demoserver.com/path/name/-/jobs/312\"",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getContinuousJobLinks(tt.args.p, tt.args.entries); got != tt.want {
				t.Errorf("getContinuousJobLinks() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getContinuousJobLabels(t *testing.T) {
	type args struct {
		entries []data.JobEntry
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "Empty Data",
			args: args{
				entries: []data.JobEntry{},
			},
			want: "",
		},
		{
			name: "Link is properly created from one job entry",
			args: args{
				entries: []data.JobEntry{
					{
						ID:         123,
						RunnerName: "runner",
					},
				},
			},
			want: "'runner - 123'",
		},
		{
			name: "Links are properly created from multiple job entries",
			args: args{
				entries: []data.JobEntry{
					{
						ID:         123,
						RunnerName: "demo",
					},
					{
						ID:         231,
						RunnerName: "other",
					},
					{
						ID:         312,
						RunnerName: "",
					},
				},
			},
			want: "'demo - 123','other - 231',' - 312'",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getContinuousJobLabels(tt.args.entries); got != tt.want {
				t.Errorf("getContinuousJobLabels() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getContinuousJobGraphStatusDataSet(t *testing.T) {
	type args struct {
		entries []data.JobEntry
	}
	tests := []struct {
		name string
		args args
		want stackedGraphDataSets
	}{
		{
			name: "Empty list",
			args: args{
				entries: []data.JobEntry{},
			},
			want: stackedGraphDataSets{
				Label:     "Job Failed",
				Data:      "",
				Type:      "bubble",
				PointSize: 10,
			},
		},
		{
			name: "Create entry with one entry (success)",
			args: args{
				entries: []data.JobEntry{
					{
						Status: "success",
					},
				},
			},
			want: stackedGraphDataSets{
				Label:     "Job Failed",
				Data:      "null",
				Type:      "bubble",
				PointSize: 10,
			},
		},
		{
			name: "Create entry with one entry (fail)",
			args: args{
				entries: []data.JobEntry{
					{
						Status: "failed",
					},
				},
			},
			want: stackedGraphDataSets{
				Label:     "Job Failed",
				Data:      "0",
				Type:      "bubble",
				PointSize: 10,
			},
		},
		{
			name: "Create entry with multiple entries",
			args: args{
				entries: []data.JobEntry{
					{
						Status: "failed",
					},
					{
						Status: "success",
					},
					{
						Status: "failed",
					},
					{
						Status: "success",
					},
				},
			},
			want: stackedGraphDataSets{
				Label:     "Job Failed",
				Data:      "0,null,0,null",
				Type:      "bubble",
				PointSize: 10,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := getContinuousJobGraphStatusDataSet(tt.args.entries)
			testhelper.DeepEqual(t, got, tt.want)
		})
	}
}

func Test_getContinuousJobGraphDefaultBranchDataSet(t *testing.T) {
	type args struct {
		entries    []data.JobEntry
		branchName string
	}
	tests := []struct {
		name string
		args args
		want stackedGraphDataSets
	}{
		{
			name: "Empty list",
			args: args{
				entries:    []data.JobEntry{},
				branchName: "demo",
			},
			want: stackedGraphDataSets{
				Label:     "demo branch Duration [s]",
				Data:      "",
				Type:      "line",
				PointSize: 2,
			},
		},
		{
			name: "Create from one JobEntry wrong branch",
			args: args{
				entries: []data.JobEntry{
					{
						Duration: 142.1,
						Ref:      "other",
					},
				},
				branchName: "demo",
			},
			want: stackedGraphDataSets{
				Label:     "demo branch Duration [s]",
				Data:      "null",
				Type:      "line",
				PointSize: 2,
			},
		},
		{
			name: "Create from one JobEntry with a matching branch",
			args: args{
				entries: []data.JobEntry{
					{
						Duration: 142.1,
						Ref:      "demo",
					},
				},
				branchName: "demo",
			},
			want: stackedGraphDataSets{
				Label:     "demo branch Duration [s]",
				Data:      "142.10",
				Type:      "line",
				PointSize: 2,
			},
		},
		{
			name: "Create from multiple JobEntries no branch match",
			args: args{
				entries: []data.JobEntry{
					{
						Duration: 142.1,
						Ref:      "other",
					},
					{
						Duration: 143.1,
						Ref:      "demo1",
					},
					{
						Duration: 144.1,
						Ref:      "other",
					},
				},
				branchName: "demo",
			},
			want: stackedGraphDataSets{
				Label:     "demo branch Duration [s]",
				Data:      "null,null,null",
				Type:      "line",
				PointSize: 2,
			},
		},
		{
			name: "Create from multiple JobEntries partial matches",
			args: args{
				entries: []data.JobEntry{
					{
						Duration: 142.1,
						Ref:      "other",
					},
					{
						Duration: 143.1,
						Ref:      "demo",
					},
					{
						Duration: 144.1,
						Ref:      "other",
					},
					{
						Duration: 145.1,
						Ref:      "demo",
					},
					{
						Duration: 146.1,
						Ref:      "demo",
					},
					{
						Duration: 147.1,
						Ref:      "other",
					},
				},
				branchName: "demo",
			},
			want: stackedGraphDataSets{
				Label:     "demo branch Duration [s]",
				Data:      "null,143.10,null,145.10,146.10,null",
				Type:      "line",
				PointSize: 2,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := getContinuousJobGraphDefaultBranchDataSet(tt.args.entries, tt.args.branchName)
			testhelper.DeepEqual(t, got, tt.want)
		})
	}
}

func Test_getContinuousJobGraphDurationDataSet(t *testing.T) {
	type args struct {
		entries []data.JobEntry
	}
	tests := []struct {
		name string
		args args
		want stackedGraphDataSets
	}{
		{
			name: "Empty list",
			args: args{
				entries: []data.JobEntry{},
			},
			want: stackedGraphDataSets{
				Label:     "Duration [s]",
				Data:      "",
				Type:      "bar",
				PointSize: 2,
			},
		},
		{
			name: "Create from one job entry",
			args: args{
				entries: []data.JobEntry{
					{
						Duration: 142.1,
					},
				},
			},
			want: stackedGraphDataSets{
				Label:     "Duration [s]",
				Data:      "142.10",
				Type:      "bar",
				PointSize: 2,
			},
		},
		{
			name: "Create from multiple job entries",
			args: args{
				entries: []data.JobEntry{
					{
						Duration: 142.1,
					},
					{
						Duration: 143.111,
					},
					{
						Duration: 144.2,
					},
				},
			},
			want: stackedGraphDataSets{
				Label:     "Duration [s]",
				Data:      "142.10,143.11,144.20",
				Type:      "bar",
				PointSize: 2,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := getContinuousJobGraphDurationDataSet(tt.args.entries)
			testhelper.DeepEqual(t, got, tt.want)
		})
	}
}

func Test_getCumulatedJobLabels(t *testing.T) {
	type args struct {
		data []data.Summary
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "empty list",
			args: args{
				data: []data.Summary{},
			},
			want: "",
		},
		{
			name: "multiple elements",
			args: args{
				data: []data.Summary{
					{Date: time.Date(2021, 11, 30, 12, 0, 0, 0, time.UTC)},
					{Date: time.Date(2021, 12, 24, 12, 0, 0, 0, time.UTC)},
					{Date: time.Date(2021, 12, 25, 12, 0, 0, 0, time.UTC)},
					{Date: time.Date(2022, 1, 2, 12, 0, 0, 0, time.UTC)},
				},
			},
			want: "30.11,24.12,25.12,2.1",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getCumulatedJobLabels(tt.args.data); got != tt.want {
				t.Errorf("getCumulatedJobLabels() = %+v, want %+v", got, tt.want)
			}
		})
	}
}

func Test_getCumulatedJobDurationGraphDataSet(t *testing.T) {
	type args struct {
		data []data.Summary
	}
	tests := []struct {
		name string
		args args
		want []stackedGraphDataSets
	}{
		{
			name: "empty summary list",
			args: args{
				data: []data.Summary{},
			},
			want: []stackedGraphDataSets{
				{
					Label:     "Average",
					Data:      "",
					Type:      "line",
					PointSize: 1,
				},
				{
					Label:     "Minimum",
					Data:      "",
					Type:      "line",
					PointSize: 1,
				},
				{
					Label:     "Maximum",
					Data:      "",
					Type:      "line",
					PointSize: 1,
				},
			},
		},
		{
			name: "generate data set from summary list",
			args: args{
				data: []data.Summary{
					{
						Date:    time.Date(2021, 11, 30, 12, 0, 0, 0, time.UTC),
						Average: 1142,
						Min:     1110,
						Max:     1150,
					},
					{
						Date:    time.Date(2021, 12, 24, 12, 0, 0, 0, time.UTC),
						Average: 2442,
						Min:     2410,
						Max:     2450,
					},
					{
						Date:    time.Date(2021, 12, 25, 12, 0, 0, 0, time.UTC),
						Average: 2542,
						Min:     2510,
						Max:     2550,
					},
					{
						Date:    time.Date(2022, 1, 2, 12, 0, 0, 0, time.UTC),
						Average: 242,
						Min:     210,
						Max:     250,
					},
				},
			},
			want: []stackedGraphDataSets{
				{
					Label:     "Average",
					Data:      "1142.00,2442.00,2542.00,242.00",
					Type:      "line",
					PointSize: 1,
				},
				{
					Label:     "Minimum",
					Data:      "1110.00,2410.00,2510.00,210.00",
					Type:      "line",
					PointSize: 1,
				},
				{
					Label:     "Maximum",
					Data:      "1150.00,2450.00,2550.00,250.00",
					Type:      "line",
					PointSize: 1,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := getCumulatedJobDurationGraphDataSet(tt.args.data)
			testhelper.DeepEqual(t, got, tt.want)
		})
	}
}

func Test_getCumulatedJobStatisticsGraphDataSet(t *testing.T) {
	type args struct {
		data []data.Summary
	}
	tests := []struct {
		name string
		args args
		want []stackedGraphDataSets
	}{
		{
			name: "empty data",
			args: args{data: []data.Summary{}},
			want: []stackedGraphDataSets{
				{
					Label:     "Total Builds",
					Type:      "line",
					Data:      "",
					PointSize: 1,
				},
				{
					Label:     "Successful Builds",
					Type:      "line",
					Data:      "",
					PointSize: 1,
				},
				{
					Label:     "Tests Count",
					Type:      "line",
					Data:      "",
					YAxisName: "Count",
					PointSize: 1,
				},
				{
					Label:     "Artifacts Size [MB]",
					Type:      "line",
					Data:      "",
					YAxisName: "ArtifactSize",
					PointSize: 1,
				},
			},
		},
		{
			name: "with summary data",
			args: args{data: []data.Summary{
				{
					Date:                 testhelper.TestDateDay1,
					TotalCnt:             10,
					SuccessCnt:           8,
					TestCnt:              12,
					ArtifactsSizeAverage: 11. * 1024 * 1024,
				},
				{
					Date:                 testhelper.TestDateDay2,
					TotalCnt:             11,
					SuccessCnt:           18,
					TestCnt:              112,
					ArtifactsSizeAverage: 42. * 1024 * 1024,
				},
			}},
			want: []stackedGraphDataSets{
				{
					Label:     "Total Builds",
					Type:      "line",
					Data:      "10,11",
					PointSize: 1,
				},
				{
					Label:     "Successful Builds",
					Type:      "line",
					Data:      "8,18",
					PointSize: 1,
				},
				{
					Label:     "Tests Count",
					Type:      "line",
					Data:      "12,112",
					YAxisName: "Count",
					PointSize: 1,
				},
				{
					Label:     "Artifacts Size [MB]",
					Type:      "line",
					Data:      "11,42",
					YAxisName: "ArtifactSize",
					PointSize: 1,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := getCumulatedJobStatisticsGraphDataSet(tt.args.data)
			testhelper.DeepEqual(t, got, tt.want)
		})
	}
}

func Test_createContinuousStatistics(t *testing.T) {
	type args struct {
		p       data.Project
		entries []data.JobEntry
	}

	tests := []struct {
		name string
		args args
		want []*jobStatisticsRow
	}{
		{
			name: "empty data",
			args: args{
				p: data.Project{
					Server: "demo.gitlab.com",
				},
				entries: []data.JobEntry{},
			},
			want: []*jobStatisticsRow{},
		},
		{
			name: "data for only one branch",
			args: args{
				p: data.Project{
					Server: "demo.gitlab.com",
				},
				entries: []data.JobEntry{
					{
						Ref:      "main",
						Status:   data.StatusSuccess,
						Duration: 100,
						WebURL:   "/irelevant",
					},
					{
						Ref:      "main",
						Status:   data.StatusFailed,
						Duration: 180,
						WebURL:   "/url-max-failed",
					},
					{
						Ref:      "main",
						Status:   data.StatusSuccess,
						Duration: 50,
						WebURL:   "/irelevant",
					},
					{
						Ref:      "main",
						Status:   data.StatusSuccess,
						Duration: 150,
						WebURL:   "/url-max-success",
					},
				},
			},
			want: []*jobStatisticsRow{
				{
					Name:                   "main",
					TotalBuilds:            4,
					FailedBuilds:           1,
					FailedRate:             100 / 4.,
					MinDurationSuccess:     50,
					MaxDurationSuccess:     150,
					MaxDurationSuccessLink: "https://demo.gitlab.com/url-max-success",
					AverageDurationSuccess: 100,
					MinDurationFailed:      180,
					MaxDurationFailed:      180,
					MaxDurationFailedLink:  "https://demo.gitlab.com/url-max-failed",
					AverageDurationFailed:  180,
				},
			},
		},
		{
			name: "data for only one branch no successful build",
			args: args{
				p: data.Project{
					Server: "demo.gitlab.com",
				},
				entries: []data.JobEntry{
					{
						Ref:      "main",
						Status:   data.StatusFailed,
						Duration: 100,
						WebURL:   "/irelevant",
					},
					{
						Ref:      "main",
						Status:   data.StatusFailed,
						Duration: 50,
						WebURL:   "/irelevant",
					},
					{
						Ref:      "main",
						Status:   data.StatusFailed,
						Duration: 150,
						WebURL:   "/url-max-failed",
					},
				},
			},
			want: []*jobStatisticsRow{
				{
					Name:                   "main",
					TotalBuilds:            3,
					FailedBuilds:           3,
					FailedRate:             100,
					MinDurationSuccess:     0,
					MaxDurationSuccess:     0,
					MaxDurationSuccessLink: "",
					AverageDurationSuccess: 0,
					MinDurationFailed:      50,
					MaxDurationFailed:      150,
					MaxDurationFailedLink:  "https://demo.gitlab.com/url-max-failed",
					AverageDurationFailed:  100,
				},
			},
		},
		{
			name: "data for only one branch no failed build",
			args: args{
				p: data.Project{
					Server: "demo.gitlab.com",
				},
				entries: []data.JobEntry{
					{
						Ref:      "main",
						Status:   data.StatusSuccess,
						Duration: 100,
						WebURL:   "/irelevant",
					},
					{
						Ref:      "main",
						Status:   data.StatusSuccess,
						Duration: 50,
						WebURL:   "/irelevant",
					},
					{
						Ref:      "main",
						Status:   data.StatusSuccess,
						Duration: 150,
						WebURL:   "/url-max-success",
					},
				},
			},
			want: []*jobStatisticsRow{
				{
					Name:                   "main",
					TotalBuilds:            3,
					FailedBuilds:           0,
					FailedRate:             0,
					MinDurationSuccess:     50,
					MaxDurationSuccess:     150,
					MaxDurationSuccessLink: "https://demo.gitlab.com/url-max-success",
					AverageDurationSuccess: 100,
					MinDurationFailed:      0,
					MaxDurationFailed:      0,
					MaxDurationFailedLink:  "",
					AverageDurationFailed:  0,
				},
			},
		},
		{
			name: "data for three branches",
			args: args{
				p: data.Project{
					Server: "demo.gitlab.com",
				},
				entries: []data.JobEntry{
					{
						Ref:      "main",
						Status:   data.StatusSuccess,
						Duration: 100,
						WebURL:   "/main-success",
					},
					{
						Ref:      "main",
						Status:   data.StatusFailed,
						Duration: 50,
						WebURL:   "/main-failed",
					},
					{
						Ref:      "bug/demo",
						Status:   data.StatusSuccess,
						Duration: 100,
						WebURL:   "/bug-success",
					},
					{
						Ref:      "bug/demo",
						Status:   data.StatusFailed,
						Duration: 50,
						WebURL:   "/bug-failed",
					},
					{
						Ref:      "feature/demo",
						Status:   data.StatusSuccess,
						Duration: 100,
						WebURL:   "/feature-success",
					},
					{
						Ref:      "feature/demo",
						Status:   data.StatusFailed,
						Duration: 50,
						WebURL:   "/feature-failed",
					},
				},
			},
			want: []*jobStatisticsRow{
				{
					Name:                   "main",
					TotalBuilds:            2,
					FailedBuilds:           1,
					FailedRate:             50,
					MinDurationSuccess:     100,
					MaxDurationSuccess:     100,
					MaxDurationSuccessLink: "https://demo.gitlab.com/main-success",
					AverageDurationSuccess: 100,
					MinDurationFailed:      50,
					MaxDurationFailed:      50,
					MaxDurationFailedLink:  "https://demo.gitlab.com/main-failed",
					AverageDurationFailed:  50,
				},
				{
					Name:                   "bug/demo",
					TotalBuilds:            2,
					FailedBuilds:           1,
					FailedRate:             50,
					MinDurationSuccess:     100,
					MaxDurationSuccess:     100,
					MaxDurationSuccessLink: "https://demo.gitlab.com/bug-success",
					AverageDurationSuccess: 100,
					MinDurationFailed:      50,
					MaxDurationFailed:      50,
					MaxDurationFailedLink:  "https://demo.gitlab.com/bug-failed",
					AverageDurationFailed:  50,
				},
				{
					Name:                   "feature/demo",
					TotalBuilds:            2,
					FailedBuilds:           1,
					FailedRate:             50,
					MinDurationSuccess:     100,
					MaxDurationSuccess:     100,
					MaxDurationSuccessLink: "https://demo.gitlab.com/feature-success",
					AverageDurationSuccess: 100,
					MinDurationFailed:      50,
					MaxDurationFailed:      50,
					MaxDurationFailedLink:  "https://demo.gitlab.com/feature-failed",
					AverageDurationFailed:  50,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := createContinuousStatistics(tt.args.p, tt.args.entries)
			sort.SliceStable(got, func(i, j int) bool {
				return strings.Compare(got[i].Name, got[j].Name) <= 0
			})
			sort.SliceStable(tt.want, func(i, j int) bool {
				return strings.Compare(tt.want[i].Name, tt.want[j].Name) <= 0
			})
			testhelper.DeepEqual(t, got, tt.want)
		})
	}
}

func Test_getContinuousJobSectionGraphDurationDataSet(t *testing.T) {
	type args struct {
		entries []data.JobEntry
	}
	tests := []struct {
		name string
		args args
		want []stackedGraphDataSets
	}{
		{
			name: "empty entries list",
			args: args{entries: []data.JobEntry{}},
			want: []stackedGraphDataSets{},
		},
		{
			name: "generate stacked data sets for given entry list",
			args: args{entries: []data.JobEntry{
				{
					BuildSections: []data.BuildSection{
						{
							Start: time.Unix(1, 0),
							End:   time.Unix(2, 0),
							Title: "1SectionOne",
						},
						{
							Start: time.Unix(4, 0),
							End:   time.Unix(20, 0),
							Title: "2SectionTwo",
						},
					},
				},
				{
					BuildSections: []data.BuildSection{
						{
							Start: time.Unix(10, 0),
							End:   time.Unix(21, 0),
							Title: "1SectionOne",
						},
						{
							Start: time.Unix(4, 0),
							End:   time.Unix(20, 0),
							Title: "3SectionThree",
						},
					},
				},
			}},
			want: []stackedGraphDataSets{
				{
					Label:      "1SectionOne Section [s]",
					Data:       "1.00,11.00",
					Type:       "bar",
					StackGroup: "sections",
					PointSize:  1,
				},
				{
					Label:      "2SectionTwo Section [s]",
					Data:       "16.00,",
					Type:       "bar",
					StackGroup: "sections",
					PointSize:  1,
				},
				{
					Label:      "3SectionThree Section [s]",
					Data:       ",16.00",
					Type:       "bar",
					StackGroup: "sections",
					PointSize:  1,
				},
			},
		},
		{
			name: "generate empty data list for job entries list with no section data",
			args: args{entries: []data.JobEntry{
				{}, {},
			}},
			want: []stackedGraphDataSets{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := getContinuousJobSectionGraphDurationDataSet(tt.args.entries)
			sort.SliceStable(got, func(i, j int) bool {
				return strings.Compare(got[i].Label, got[j].Label) <= 0
			})
			testhelper.DeepEqual(t, got, tt.want)
		})
	}
}

func Test_getContinuousJobQueueDurationDataSet(t *testing.T) {
	type args struct {
		entries []data.JobEntry
	}
	tests := []struct {
		name string
		args args
		want stackedGraphDataSets
	}{
		{
			name: "empty entry list",
			args: args{
				entries: []data.JobEntry{},
			},
			want: stackedGraphDataSets{
				Label:      "Queued Duration [s]",
				Data:       "",
				Type:       "line",
				StackGroup: "queued",
				PointSize:  1,
			},
		},
		{
			name: "with job elements",
			args: args{
				entries: []data.JobEntry{
					{QueuedDuration: 100},
					{QueuedDuration: 200},
					{QueuedDuration: 300},
				},
			},
			want: stackedGraphDataSets{
				Label:      "Queued Duration [s]",
				Data:       "100.00,200.00,300.00",
				Type:       "line",
				StackGroup: "queued",
				PointSize:  1,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := getContinuousJobQueueDurationDataSet(tt.args.entries)
			testhelper.DeepEqual(t, got, tt.want)
		})
	}
}

func Test_getContinuousJobGraphArtifactsDataSet(t *testing.T) {
	type args struct {
		entries []data.JobEntry
	}
	tests := []struct {
		name string
		args args
		want stackedGraphDataSets
	}{
		{
			name: "empty data set",
			args: args{
				entries: []data.JobEntry{},
			},
			want: stackedGraphDataSets{
				Label:      "Artifacts Size [MB]",
				Type:       "line",
				Data:       "",
				StackGroup: "",
				YAxisName:  "ArtifactsSize",
				PointSize:  1,
			},
		},
		{
			name: "with one data element",
			args: args{
				[]data.JobEntry{
					{ArtifactsSize: 12 * 1024 * 1024},
				},
			},
			want: stackedGraphDataSets{
				Label:      "Artifacts Size [MB]",
				Type:       "line",
				Data:       "12",
				StackGroup: "",
				YAxisName:  "ArtifactsSize",
				PointSize:  1,
			},
		},
		{
			name: "with multiple data element",
			args: args{
				[]data.JobEntry{
					{ArtifactsSize: 12 * 1024 * 1024},
					{ArtifactsSize: 13 * 1024 * 1024},
					{ArtifactsSize: 14 * 1024 * 1024},
				},
			},
			want: stackedGraphDataSets{
				Label:      "Artifacts Size [MB]",
				Type:       "line",
				Data:       "12,13,14",
				StackGroup: "",
				YAxisName:  "ArtifactsSize",
				PointSize:  1,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := getContinuousJobGraphArtifactsDataSet(tt.args.entries)
			testhelper.DeepEqual(t, got, tt.want)
		})
	}
}

func Test_getBuildSectionTitleData(t *testing.T) {
	// test that all json elements have an associated row title
	titles := getBuildSectionTitleData()
	rowEntry := &jobBuildSectionRow{}

	rowFields := structs.Fields(rowEntry)

	rowFieldTags := make([]string, 0, len(rowFields))
	titleFields := make([]string, 0, len(titles))

	for _, f := range rowFields {
		rowFieldTags = append(rowFieldTags, f.Tag("json"))
	}
	for _, e := range titles {
		titleFields = append(titleFields, e.Field)
	}

	sort.SliceStable(rowFieldTags, func(i, j int) bool {
		return strings.Compare(rowFieldTags[i], rowFieldTags[j]) <= 0
	})
	sort.SliceStable(titleFields, func(i, j int) bool {
		return strings.Compare(titleFields[i], titleFields[j]) <= 0
	})

	testhelper.DeepEqual(t, rowFieldTags, titleFields)
}

func Test_getBuildSectionRows(t *testing.T) {
	now := time.Now()
	type args struct {
		server  string
		entries []data.JobEntry
	}
	tests := []struct {
		name string
		args args
		want []jobBuildSectionRow
	}{
		{
			name: "empty data set",
			args: args{
				server:  "Demo",
				entries: []data.JobEntry{},
			},
			want: []jobBuildSectionRow{},
		},
		{
			name: "with data set but no build section times",
			args: args{
				server: "Demo",
				entries: []data.JobEntry{
					{
						RunnerName:    "Runner1",
						WebURL:        "/irelevant",
						BuildSections: []data.BuildSection{},
					},
				},
			},
			want: []jobBuildSectionRow{},
		},
		{
			name: "with multiple data sets for one section row",
			args: args{
				server: "demo.gitlab.com",
				entries: []data.JobEntry{
					{
						RunnerName: "Runner1Avg",
						WebURL:     "/irelevant",
						BuildSections: []data.BuildSection{
							{Title: "sec1", Start: now, End: now.Add(time.Second * 70)},
						},
					},
					{
						RunnerName: "Runner1Max",
						WebURL:     "/url-max",
						BuildSections: []data.BuildSection{
							{Title: "sec1", Start: now, End: now.Add(time.Second * 100)},
						},
					},
					{
						RunnerName: "Runner1Min",
						WebURL:     "/url-min",
						BuildSections: []data.BuildSection{
							{Title: "sec1", Start: now, End: now.Add(time.Second * 40)},
						},
					},
				},
			},
			want: []jobBuildSectionRow{
				{
					Name:             "sec1",
					MinDuration:      40,
					MinDurationAgent: "Runner1Min",
					MinDurationLink:  "https://demo.gitlab.com/url-min",
					MaxDuration:      100,
					MaxDurationAgent: "Runner1Max",
					MaxDurationLink:  "https://demo.gitlab.com/url-max",
					AvgDuration:      70,
				},
			},
		},
		{
			name: "with multiple data sets for one section row within one entry",
			args: args{
				server: "demo.gitlab.com",
				entries: []data.JobEntry{
					{
						RunnerName: "Runner1",
						WebURL:     "/min-max",
						BuildSections: []data.BuildSection{
							{Title: "sec1", Start: now, End: now.Add(time.Second * 70)},
							{Title: "sec1", Start: now, End: now.Add(time.Second * 100)},
							{Title: "sec1", Start: now, End: now.Add(time.Second * 40)},
						},
					},
				},
			},
			want: []jobBuildSectionRow{
				{
					Name:             "sec1",
					MinDuration:      40,
					MinDurationAgent: "Runner1",
					MinDurationLink:  "https://demo.gitlab.com/min-max",
					MaxDuration:      100,
					MaxDurationAgent: "Runner1",
					MaxDurationLink:  "https://demo.gitlab.com/min-max",
					AvgDuration:      70,
				},
			},
		},
		{
			name: "with data sets for multiple section rows",
			args: args{
				server: "demo.gitlab.com",
				entries: []data.JobEntry{
					{
						RunnerName: "Runner1",
						WebURL:     "/sec1",
						BuildSections: []data.BuildSection{
							{Title: "sec1", Start: now, End: now.Add(time.Second * 70)},
						},
					},
					{
						RunnerName: "Runner2",
						WebURL:     "/sec2",
						BuildSections: []data.BuildSection{
							{Title: "sec2", Start: now, End: now.Add(time.Second * 100)},
						},
					},
				},
			},
			want: []jobBuildSectionRow{
				{
					Name:             "sec1",
					MinDuration:      70,
					MinDurationAgent: "Runner1",
					MinDurationLink:  "https://demo.gitlab.com/sec1",
					MaxDuration:      70,
					MaxDurationAgent: "Runner1",
					MaxDurationLink:  "https://demo.gitlab.com/sec1",
					AvgDuration:      70,
				},
				{
					Name:             "sec2",
					MinDuration:      100,
					MinDurationAgent: "Runner2",
					MinDurationLink:  "https://demo.gitlab.com/sec2",
					MaxDuration:      100,
					MaxDurationAgent: "Runner2",
					MaxDurationLink:  "https://demo.gitlab.com/sec2",
					AvgDuration:      100,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := getBuildSectionRows(tt.args.server, tt.args.entries)
			sort.SliceStable(got, func(i, j int) bool {
				return strings.Compare(got[i].Name, got[j].Name) <= 0
			})
			testhelper.DeepEqual(t, got, tt.want)
		})
	}
}

func Test_createJobBuildSectionsTable(t *testing.T) {
	type args struct {
		rows  []jobBuildSectionRow
		title string
		name  string
	}
	tests := []struct {
		name        string
		args        args
		wantMatches []string
		wantErr     bool
	}{
		{
			name: "empty row list",
			args: args{
				rows:  []jobBuildSectionRow{},
				title: "Build Title",
				name:  "sectionID",
			},
			wantMatches: []string{
				"<h2>Build Title",
				"#sectionID-table",
				"data:\\[\\],",
			},
			wantErr: false,
		},
		{
			name: "with given data rows",
			args: args{
				rows: []jobBuildSectionRow{
					{
						Name:             "Demo",
						MinDuration:      10,
						MinDurationAgent: "Ag1",
						MinDurationLink:  "demo.git/min",
						MaxDuration:      12,
						MaxDurationAgent: "Ag2",
						MaxDurationLink:  "demo.git/max",
						AvgDuration:      42,
					},
				},
				title: "Build Title",
				name:  "sectionID",
			},
			wantMatches: []string{
				"<h2>Build Title",
				"#sectionID-table",
				"data:\\[\n",
				"\"maxDuration\":12",
				"\"maxDurationLink\":\"demo.git/max\"",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := createJobBuildSectionsTable(tt.args.rows, tt.args.title, tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("createJobBuildSectionsTable() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			for _, m := range tt.wantMatches {
				r := regexp.MustCompile(m)
				if !r.MatchString(got) {
					t.Errorf("createJobBuildSectionsTable() the regex '%s' did not match\n%s", m, got)
				}
			}
		})
	}
}
