// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package renderer

import (
	"bytes"
	"regexp"
	"testing"
)

func Test_createHTMLReport(t *testing.T) {
	type args struct {
		title string
		data  string
	}
	tests := []struct {
		name        string
		args        args
		wantMatches []string
		wantErr     bool
	}{
		{
			name: "Use template with some data",
			args: args{
				title: "THIS IS THE TITLE",
				data:  "WE NEED TO FIND THIS",
			},
			wantMatches: []string{
				"<h1>THIS IS THE TITLE</h1>",
				"<title>THIS IS THE TITLE</title>",
				`WE NEED TO FIND THIS\n`,
			},
			wantErr: false,
		},
		{
			name: "Use template with some html encoded data",
			args: args{
				title: "TITLE",
				data: `<p>
    <b>Text</b>
</p>`,
			},
			wantMatches: []string{
				`<p>
    <b>Text</b>
</p>`,
			},
			wantErr: false,
		},
		{
			name: "Use template with some java script data",
			args: args{
				title: "TITLE",
				data: `<p id="demo">A Paragraph.</p>

				<button type="button" onclick="myFunction()">Try it</button>

				<script>
				function myFunction() {
				  document.getElementById("demo").innerHTML = "Paragraph changed.";
				}
				</script>`,
			},
			wantMatches: []string{
				`<p id="demo">A Paragraph\.</p>

				<button type="button" onclick="myFunction\(\)">Try it</button>

				<script>
				function myFunction\(\) {
				  document.getElementById\("demo"\)\.innerHTML = "Paragraph changed.";
				}
				</script>`,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := &bytes.Buffer{}
			if err := createHTMLReport(tt.args.title, tt.args.data, o); (err != nil) != tt.wantErr {
				t.Errorf("createHTMLReport() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			for _, m := range tt.wantMatches {
				r := regexp.MustCompile(m)
				if !r.Match(o.Bytes()) {
					t.Errorf("createHTMLReport() the regex '%s' did not match\n%s", m, o.String())
				}
			}
		})
	}
}
