// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package storage

import (
	"compress/gzip"
	"encoding/json"
	"fmt"
	"os"

	"gitlab.com/mathweb/gitlab-pipeline-analyzer/data"
)

func SavePipelineData(name string, js *data.PipelineDataCollection) error {
	f, err := os.Create(name)
	if err != nil {
		return fmt.Errorf("failed to create pipeline data file %s got err: %v", name, err)
	}
	defer f.Close()
	if js == nil {
		return fmt.Errorf("could not save empty pipeline data collection to file: %s", name)
	}
	zf := gzip.NewWriter(f)
	defer zf.Close()
	enc := json.NewEncoder(zf)
	enc.SetIndent("", "    ")
	return enc.Encode(js)
}

func ReadPipelineData(name string) (*data.PipelineDataCollection, error) {
	exists, err := doesFileExist(name)
	if err != nil {
		return nil, err
	}
	if !exists {
		return nil, fmt.Errorf("specified file does not exist: %s", name)
	}

	f, err := os.Open(name)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	zf, err := gzip.NewReader(f)
	if err != nil {
		return nil, err
	}
	defer zf.Close()
	dec := json.NewDecoder(zf)
	js := &data.PipelineDataCollection{}
	err = dec.Decode(js)
	return js, err
}
