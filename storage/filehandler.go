// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package storage

import (
	"fmt"
	"os"
)

// FileExists checks if the file exists, does not differentiate between a file,
// link or folder.
func FileExists(name string) bool {
	_, err := os.Stat(name)
	return !os.IsNotExist(err)
}

// DirectoryExists checks if the directory exists and that it is a directory.
func DirectoryExists(name string) bool {
	info, err := os.Stat(name)
	if os.IsNotExist(err) {
		return false
	}
	return info.IsDir()
}

func doesFileExist(name string) (bool, error) {
	s, err := os.Stat(name)

	// check if error is "file not exists"
	if os.IsNotExist(err) {
		f, err := os.Create(name)
		if err != nil {
			return false, err
		}
		_ = f.Close()
		return false, nil
	}
	if s.IsDir() {
		return false, fmt.Errorf("specified file is a directory: %s", name)
	}
	return true, nil
}
