// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package storage

import (
	"compress/gzip"
	"encoding/json"
	"fmt"
	"os"

	"gitlab.com/mathweb/gitlab-pipeline-analyzer/data"
)

func SaveJobsData(name string, js *data.JobDataCollection) error {
	f, err := os.Create(name)
	if err != nil {
		return err
	}
	defer f.Close()
	if js == nil {
		return nil
	}
	zf := gzip.NewWriter(f)
	defer zf.Close()
	enc := json.NewEncoder(zf)
	enc.SetIndent("", "    ")
	return enc.Encode(js)
}

func ReadJobsData(name string) (*data.JobDataCollection, error) {
	exists, err := doesFileExist(name)
	if err != nil {
		return nil, err
	}
	if !exists {
		return nil, fmt.Errorf("specified file does not exist: %s", name)
	}

	f, err := os.Open(name)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	zf, err := gzip.NewReader(f)
	if err != nil {
		return nil, err
	}
	defer zf.Close()
	dec := json.NewDecoder(zf)
	js := &data.JobDataCollection{}
	err = dec.Decode(js)
	return js, err
}
