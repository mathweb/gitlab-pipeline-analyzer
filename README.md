# gitlab-pipeline-analyzer

The gitlab-pipeline-analyzer generates an overview graph of the build pipelines
to better understand your build environment. There are other ways to access this
information, but most of these tools require several other tools to be
configured, and they do not provide the same level of detail as the
gitlab-pipeline-analyzer.

IMPORTANT this tool is still highly experimental and was first implemented with
a specific pipeline in mind. This is a first prototype; some data might not be
as valuable as I might have thought, and others are more valuable than expected.
At the end this is an experiment that we use to analyze our own pipeline, and it
might be helpful to others.

## Usage

There are two main use cases one is getting information about a pipeline, and
the other is retrieving details about a single job. It will always show several
pipelines or jobs in a row to see changes over time. If the analyzer is used to
improve a pipeline/job, the change should be visible within the generated
graphs.

### Pipelines

WARNING: Call these commands with caution, especially with projects like GitLab,
which have huge pipelines and require a lot of data to be fetched.

To fetch the data for a pipeline, call the command like this:

    gitlab-pipeline-analyzer project update data.gba.gz -p "gitlab-org/gitlab" -d5 -u gitlab.org -k GITLAB-API-KEY

For big projects, this call might take a while to get all the data. It is
possible to limit the pipelines to a scheduled trigger to limit the amount of
data to fetch. This is helpful if the pipelines differ quite a bit depending on
the trigger, since the graphs have the highest values if the data from the
different pipelines are comparable to each other. The project update will always
fetch the main branch of the project and limit all pipeline builds to this
branch. It is currently not possible to fetch older data.

The parameter for the scheduler filter is specified with -s or --filter-schedule
and expects the ID of the schedule.

To reduce the amount of data it is possible to not fetch the job trace data with
the parameter --ignoreJobTrace. This is especially helpful for huge projects
where there are hundreds of jobs. Since without this parameter the job trace
will be parsed for each job within a pipeline and this might take quite some
time. The job traces might be more valuable for job analysis than pipeline
analysis.

The -d parameter specifies the number of days to fetch. If a previous data file
is available, the analyzer will only add new data and not fetch or update
already fetched data. This is helpful to track a pipeline over a long period of
time and only fetch new updates. Further, it is important that the analyzer only
fetches the data until the day before today; it will always ignore the current
day since the current day has the tendency of having rebuilds, which might lead
to wrong data, especially after partial upgrades.

To update the data, it is possible to use a simplified command:

    gitlab-pipeline-analyzer project update data.gba.gz -d5 -k GITLAB-API-KEY

For the project data file, it is possible to print two different kinds of
graphs. One is the pipeline graph, which gives an overview of an entire pipeline
and its run times, and the second graph generates graphs for all jobs within a
pipeline and creates a report for each of the jobs.

To generate the pipeline graph, call it like this:

    gitlab-pipeline-analyzer project html data.gba.gz -o pipeline.html

To generate all the job reports for the pipeline data, use the following
command:

    gitlab-pipeline-analyzer project jobreports data.gba.gz -o reports/

### Jobs

The second type of data that can be collected is the data about a specific job.
This can be limited to a specific branch, or it is possible to get all the
information for all branches. This is mostly helpful to see if a specific change
to a job has the expected effect on the run time.

    gitlab-pipeline-analyzer job update job-data.gba.gz -p "gitlab-org/gitlab" -d5 -u gitlab.org -j JOBNAME  -k GITLAB-API-KEY

With the parameter --branch, it is possible to specify a branch to limit the job
data to this branch. To update the data, it is possible to use a simplified
command:

    gitlab-pipeline-analyzer job update job-data.gba.gz -d5 -k GITLAB-API-KEY

To generate the job report, use the following command:

    gitlab-pipeline-analyzer job html job-data.gba.gz -o job-data.html

## Build

On Linux, just run the following commands to setup the development environment
and build the binary:

    make ci

The only requirements are Make and curl; everything else should be fetched
during the build process. For a convenient way to work, it is recommended to
install [direnv](https://direnv.net/) to setup the proper environment.

## Roadmap

- Add better documentation and examples of how to use this to analyze a long
  build pipeline.
- Add documentation on how to generate these graphs daily to have a long-term
  trend analysis of the pipelines/jobs.
- Code clean-up and improved tests
- Speed improvements
- improved graphs with the ability to filter
- Better-looking reports
- Create a release and provide a Docker container so that it can be used within
  a GitLab pipeline.
