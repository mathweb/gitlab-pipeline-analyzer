module gitlab.com/mathweb/gitlab-pipeline-analyzer

go 1.21.3

require (
	9fans.net/go v0.0.4 // indirect
	github.com/acroca/go-symbols v0.1.1 // indirect
	github.com/boumenot/gocover-cobertura v1.2.0 // indirect
	github.com/fatih/structs v1.1.0 // indirect
	github.com/go-test/deep v1.1.0 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/hasura/go-graphql-client v0.10.0 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/klauspost/compress v1.16.7 // indirect
	github.com/nsf/gocode v0.0.0-20230322162601-b672b49f3818 // indirect
	github.com/ramya-rao-a/go-outline v0.0.0-20210608161538-9736a4bde949 // indirect
	github.com/rogpeppe/godef v1.1.2 // indirect
	github.com/spf13/cobra v1.7.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/yuin/goldmark v1.6.0 // indirect
	golang.org/x/mod v0.14.0 // indirect
	golang.org/x/net v0.18.0 // indirect
	golang.org/x/oauth2 v0.13.0 // indirect
	golang.org/x/sys v0.14.0 // indirect
	golang.org/x/tools v0.15.0 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.31.0 // indirect
	nhooyr.io/websocket v1.8.7 // indirect
)
