// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package data

import (
	"math"
	"sort"
	"time"
)

type Summary struct {
	Date                 time.Time `json:"key"`
	Min                  float64   `json:"min"`
	Max                  float64   `json:"max"`
	Average              float64   `json:"average"`
	SuccessCnt           int64     `json:"successCnt"`
	TotalCnt             int64     `json:"totalCnt"`
	ArtifactsSizeAverage float64   `json:"artifactsSizeAverage"`
	Coverage             float64   `json:"coverage"`
	TestCnt              int64     `json:"testCnt"`
	TestSuccessCnt       int64     `json:"testSuccessCnt"`
}

func SortSummaries(s []Summary) {
	sort.Slice(s, func(i, j int) bool {
		return s[i].Date.Before(s[j].Date)
	})
}

func generateMonthlySummaries(summaries []Summary, month time.Month, year int) Summary {
	sum := Summary{
		Date: time.Date(year, month, 1, 0, 0, 0, 0, time.UTC),
		Min:  math.Inf(1),
	}

	var totalAvg float64
	var totalCoverage float64
	var totalTestCnt int64
	var totalTestSuccessCnt int64
	var totalArtifactSize float64

	for _, s := range summaries {
		if s.Date.Year() != year || s.Date.Month() != month {
			// skip wrong date
			continue
		}
		if sum.Min > s.Min {
			sum.Min = s.Min
		}
		if sum.Max < s.Max {
			sum.Max = s.Max
		}
		totalAvg += s.Average * float64(s.SuccessCnt)
		sum.SuccessCnt += s.SuccessCnt
		sum.TotalCnt += s.TotalCnt
		totalArtifactSize += s.ArtifactsSizeAverage * float64(s.SuccessCnt)
		totalCoverage += s.Coverage * float64(s.SuccessCnt)
		totalTestCnt += s.TestCnt * s.SuccessCnt
		totalTestSuccessCnt += s.TestSuccessCnt * s.SuccessCnt
	}
	if sum.SuccessCnt > 0 {
		sum.Average = totalAvg / float64(sum.SuccessCnt)
		sum.Coverage = totalCoverage / float64(sum.SuccessCnt)
		sum.TestCnt = totalTestCnt / sum.SuccessCnt
		sum.TestSuccessCnt = totalTestSuccessCnt / sum.SuccessCnt
		sum.ArtifactsSizeAverage = totalArtifactSize / float64(sum.SuccessCnt)
	} else {
		sum.Min = 0
	}

	return sum
}
