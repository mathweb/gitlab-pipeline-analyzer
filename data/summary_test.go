// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package data

import (
	"testing"
	"time"

	"gitlab.com/mathweb/gitlab-pipeline-analyzer/internal/testhelper"
)

func Test_generateMonthlySummaries(t *testing.T) {
	type args struct {
		summaries []Summary
		month     time.Month
		year      int
	}
	tests := []struct {
		name string
		args args
		want Summary
	}{
		{
			name: "generate Summary for empty list",
			args: args{
				summaries: []Summary{},
				month:     12,
				year:      2002,
			},
			want: Summary{
				Date:                 time.Date(2002, 12, 1, 0, 0, 0, 0, time.UTC),
				Min:                  0,
				Max:                  0,
				Average:              0,
				SuccessCnt:           0,
				TotalCnt:             0,
				ArtifactsSizeAverage: 0,
				Coverage:             0,
				TestCnt:              0,
				TestSuccessCnt:       0,
			},
		},
		{
			name: "generate Summary for a list with entries",
			args: args{
				summaries: []Summary{
					{
						Date:                 time.Date(2002, 12, 3, 0, 0, 0, 0, time.UTC),
						Min:                  100,
						Max:                  300,
						Average:              200,
						SuccessCnt:           5,
						TotalCnt:             10,
						ArtifactsSizeAverage: 42,
						Coverage:             50,
						TestCnt:              12,
						TestSuccessCnt:       10,
					},
					{
						Date:                 time.Date(2002, 12, 4, 0, 0, 0, 0, time.UTC),
						Min:                  70,
						Max:                  250,
						Average:              150,
						SuccessCnt:           2,
						TotalCnt:             20,
						ArtifactsSizeAverage: 32,
						Coverage:             60,
						TestCnt:              22,
						TestSuccessCnt:       20,
					},
					{
						Date:                 time.Date(2002, 11, 4, 0, 0, 0, 0, time.UTC),
						Min:                  7,
						Max:                  3000,
						Average:              550,
						SuccessCnt:           500,
						TotalCnt:             1000,
						ArtifactsSizeAverage: 320,
						Coverage:             20,
						TestCnt:              122,
						TestSuccessCnt:       102,
					},
					{
						Date:                 time.Date(2003, 12, 4, 0, 0, 0, 0, time.UTC),
						Min:                  7,
						Max:                  3000,
						Average:              450,
						SuccessCnt:           500,
						TotalCnt:             1000,
						ArtifactsSizeAverage: 3200,
						Coverage:             10,
						TestCnt:              120,
						TestSuccessCnt:       100,
					},
				},
				month: 12,
				year:  2002,
			},
			want: Summary{
				Date:                 time.Date(2002, 12, 1, 0, 0, 0, 0, time.UTC),
				Min:                  70,
				Max:                  300,
				Average:              (5*200 + 2*150) / 7.,
				SuccessCnt:           7,
				TotalCnt:             30,
				ArtifactsSizeAverage: (5*42. + 2*32.) / 7.,
				Coverage:             (5*50. + 2*60.) / 7.,
				TestCnt:              (5*12 + 2*22) / 7,
				TestSuccessCnt:       (5*10 + 2*20) / 7,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := generateMonthlySummaries(tt.args.summaries, tt.args.month, tt.args.year)
			testhelper.DeepEqual(t, got, tt.want)
		})
	}
}
