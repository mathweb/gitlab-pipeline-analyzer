// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package data

import "time"

// JobDataCollection holds the all the required data for one Job from one
// Project
type JobDataCollection struct {
	Project `json:"project"`
	Updated time.Time `json:"updated"`
	JobName string    `json:"name"`
	Job     `json:"job"`
}
