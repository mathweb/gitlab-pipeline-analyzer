// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package data

import (
	"fmt"
	"time"
)

// PipelineDataCollection holds the all the required data for pipeline
// information from one project
type PipelineDataCollection struct {
	Project          Project         `json:"project"`
	Updated          time.Time       `json:"updated"`
	Pipelines        Pipelines       `json:"pipelines"`
	Jobs             map[string]*Job `json:"jobs"`
	FilterScheduleID int             `json:"filterForScheduleId"`
}

func (p *PipelineDataCollection) ProcessAndCleanUpData() {
	p.Pipelines.UpdateSummaries()
	p.Pipelines.CleanUp(200, 30, 8, 12)
	for _, j := range p.Jobs {
		j.UpdateSummaries()
		j.CleanUp(400, 30, 8, 12)
	}
}

func (p *PipelineDataCollection) AddPipelineData(pipelines []PipelineEntry) {
	for _, pip := range pipelines {
		p.Pipelines.AddEntry(pip)
	}
}

func (p *PipelineDataCollection) AddJobData(jobs []JobEntry, f jobTraceFetcher) {
	if p.Jobs == nil {
		p.Jobs = make(map[string]*Job, 0)
	}
	for _, job := range jobs {
		je := p.Jobs[job.Name]
		if je == nil {
			je = &Job{}
			p.Jobs[job.Name] = je
		}
		je.AddEntry(job, f)
	}
}

func (p *PipelineDataCollection) AllDatesValid() {
	if p.Updated.Year() > 9999 && p.Updated.Year() < 0 {
		fmt.Printf("UPDATED OUT OF SCOPE: %s\n", p.Updated)
		p.Updated = time.Date(1900, 1, 1, 1, 1, 1, 0, time.UTC)
	}
	for i, k := range p.Pipelines.RawData {
		if k.StartedAt.Year() > 9999 && k.StartedAt.Year() < 0 {
			fmt.Printf("STARTED AT OUT OF SCOPE: %s, %d\n", k.StartedAt, k.ID)
			p.Pipelines.RawData[i].StartedAt = time.Date(1900, 1, 1, 1, 1, 1, 0, time.UTC)
		}
	}
}
