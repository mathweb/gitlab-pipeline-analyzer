// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package data

import (
	"math"
	"sort"
	"strings"
	"time"
)

type PipelineEntry struct {
	ID                 int                `json:"id"`
	PublicID           string             `json:"publicId"`
	Status             string             `json:"status"`
	Branch             string             `json:"branch"`
	StartedAt          time.Time          `json:"startedAt"`
	Duration           float64            `json:"duration"`
	Coverage           float64            `json:"coverage"`
	ArtifactsSize      float64            `json:"artifactsSize"`
	WebURL             string             `json:"webUrl"`
	PipelineTestReport TestReport         `json:"testReport"`
	TestReports        map[int]TestReport `json:"-"`
}

type Pipelines struct {
	RawData        []PipelineEntry `json:"rawData"`
	DaySummaries   []Summary       `json:"daySummaries"`
	WeekSummaries  []Summary       `json:"weekSummaries"`
	MonthSummaries []Summary       `json:"monthSummaries"`
}

func (p *Pipelines) AddEntry(pipeline PipelineEntry) {
	// this does not use a cache for speedup since it is expected to be in a
	// reasonable size < few thousand elements
	for _, d := range p.RawData {
		if d.ID == pipeline.ID {
			// skip already in there no update of data supported
			return
		}
	}
	p.RawData = append(p.RawData, pipeline)
}

func (p *Pipelines) CleanUp(maxRawElements, maxDayElements, maxWeekElements, maxMonthElements int) {
	p.sortRawData()
	if len(p.RawData) > maxRawElements {
		p.RawData = p.RawData[len(p.RawData)-maxRawElements:]
	}
	SortSummaries(p.DaySummaries)
	if len(p.DaySummaries) > maxDayElements {
		p.DaySummaries = p.DaySummaries[len(p.DaySummaries)-maxDayElements:]
	}
	SortSummaries(p.WeekSummaries)
	if len(p.WeekSummaries) > maxWeekElements {
		p.WeekSummaries = p.WeekSummaries[len(p.WeekSummaries)-maxWeekElements:]
	}
	SortSummaries(p.MonthSummaries)
	if len(p.MonthSummaries) > maxMonthElements {
		p.MonthSummaries = p.MonthSummaries[len(p.MonthSummaries)-maxMonthElements:]
	}
}

func (p *Pipelines) UpdateSummaries() {
	p.generateDaySummaries()
	p.generateWeekSummaries()
	p.generateMonthSummaries()
}

func (p *Pipelines) generateDaySummaries() {
	lastDay := time.Unix(0, 0)
	p.sortRawData()
	SortSummaries(p.DaySummaries)
	if len(p.DaySummaries) > 0 {
		lastDay = p.DaySummaries[len(p.DaySummaries)-1].Date
	}

	days := mapPipelineEntriesIntoDayBuckets(p.RawData, lastDay)

	for key := range days {
		p.DaySummaries = append(p.DaySummaries, getPipelineSummary(key, days[key]))
	}
	SortSummaries(p.DaySummaries)
}

func (p *Pipelines) generateMonthSummaries() {
	baseDate := time.Now().AddDate(0, -1, 0)

	if len(p.MonthSummaries) > 0 {
		lastDate := p.MonthSummaries[len(p.MonthSummaries)-1].Date
		if lastDate.Month() == baseDate.Month() && lastDate.Year() == baseDate.Year() {
			// skip since already generated
			return
		}
	}

	p.MonthSummaries = append(p.MonthSummaries, generateMonthlySummaries(p.DaySummaries, baseDate.Month(), baseDate.Year()))
}

func (p *Pipelines) generateWeekSummaries() {
	lastYear := 0
	lastWeek := 0
	p.sortRawData()
	SortSummaries(p.WeekSummaries)
	if len(p.WeekSummaries) > 0 {
		lastYear, lastWeek = p.WeekSummaries[len(p.WeekSummaries)-1].Date.ISOWeek()
	}
	weeks := mapPipelineEntriesIntoWeekBuckets(p.RawData, lastYear, lastWeek)
	for key := range weeks {
		p.WeekSummaries = append(p.WeekSummaries, getPipelineSummary(key, weeks[key]))
	}
	SortSummaries(p.WeekSummaries)
}

func (p *Pipelines) sortRawData() {
	sort.Slice(p.RawData, func(i, k int) bool {
		return p.RawData[i].StartedAt.Before(p.RawData[k].StartedAt)
	})
}

func mapPipelineEntriesIntoDayBuckets(pipelines []PipelineEntry, after time.Time) map[time.Time][]PipelineEntry {
	days := make(map[time.Time][]PipelineEntry)
	after = after.AddDate(0, 0, 1)
	for _, entry := range pipelines {
		if entry.StartedAt.Before(after) {
			// skip already processed
			continue
		}
		y, m, d := entry.StartedAt.Date()
		dayEntry := time.Date(y, m, d, 0, 0, 0, 0, time.UTC)
		days[dayEntry] = append(days[dayEntry], entry)
	}
	return days
}

// mapPipelineEntriesIntoWeekBuckets maps each raw pipeline entry to a week to
// accumulate the data for the summary. Only finished weeks are accumulated and
// as the sorting date the monday of the week is used.
func mapPipelineEntriesIntoWeekBuckets(pipelines []PipelineEntry, afterYear int, afterWeek int) map[time.Time][]PipelineEntry {
	weeks := make(map[time.Time][]PipelineEntry)
	for _, entry := range pipelines {
		y, w := entry.StartedAt.ISOWeek()
		if y < afterYear || y == afterYear && w <= afterWeek {
			// skip already processed
			continue
		}

		weekEntry := getMondayDate(entry.StartedAt)
		weeks[weekEntry] = append(weeks[weekEntry], entry)
	}
	return weeks
}

func getPipelineSummary(key time.Time, entries []PipelineEntry) Summary {
	min := math.Inf(1)
	max := 0.0
	totalDuration := 0.0
	totalCoverage := 0.0
	totalArtifactsSize := 0.0

	var successFullCnt int64
	var testCnt int64
	var testSuccessCnt int64
	var buildsWithTestCnt int64

	for _, e := range entries {
		if strings.ToUpper(e.Status) == StatusSuccess {
			if e.Duration < min {
				min = e.Duration
			}
			if e.Duration > max {
				max = e.Duration
			}
			successFullCnt++
			totalDuration += e.Duration
			totalCoverage += e.Coverage
			totalArtifactsSize += e.ArtifactsSize
			if e.PipelineTestReport.Count > 0 {
				testCnt += e.PipelineTestReport.Count
				testSuccessCnt += e.PipelineTestReport.Success
				buildsWithTestCnt++
			}
		}
	}

	durationAvg := 0.
	covAvg := 0.
	artifactsSizeAvg := 0.
	if successFullCnt > 0 {
		durationAvg = totalDuration / float64(successFullCnt)
		covAvg = totalCoverage / float64(successFullCnt)
		artifactsSizeAvg = totalArtifactsSize / float64(successFullCnt)
	}
	if math.IsInf(min, 1) {
		min = 0
	}
	var testAvg int64
	var testSuccessAvg int64
	if buildsWithTestCnt > 0 {
		testAvg = testCnt / buildsWithTestCnt
		testSuccessAvg = testSuccessCnt / buildsWithTestCnt
	}
	return Summary{
		Date:                 key,
		Min:                  min,
		Max:                  max,
		Average:              durationAvg,
		SuccessCnt:           successFullCnt,
		TotalCnt:             int64(len(entries)),
		ArtifactsSizeAverage: artifactsSizeAvg,
		Coverage:             covAvg,
		TestCnt:              testAvg,
		TestSuccessCnt:       testSuccessAvg,
	}
}
