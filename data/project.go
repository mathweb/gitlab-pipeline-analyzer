// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package data

// Project represent a gitlab project with all the required information to
// access the project.
type Project struct {
	Name           string `json:"name"`
	ID             string `json:"id"`
	FullPath       string `json:"fullPath"`
	Server         string `json:"server"`
	DefaultBranch  string `json:"defaultBranch"`
	IgnoreJobTrace bool   `json:"ignoreJobTrace"`
}

func NewProject(id string, server string, fullPath string, name string, branch string, ignoreJobTrace bool) *Project {
	return &Project{
		ID:             id,
		FullPath:       fullPath,
		Server:         server,
		Name:           name,
		DefaultBranch:  branch,
		IgnoreJobTrace: ignoreJobTrace,
	}
}
