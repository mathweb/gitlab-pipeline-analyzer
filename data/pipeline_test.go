// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package data

import (
	"testing"
	"time"

	"gitlab.com/mathweb/gitlab-pipeline-analyzer/internal/testhelper"
)

func TestPipeline_CleanUp(t *testing.T) {
	type fields struct {
		rawData        []PipelineEntry
		daySummaries   []Summary
		weekSummaries  []Summary
		monthSummaries []Summary
	}
	type args struct {
		maxRawElements   int
		maxDayElements   int
		maxWeekElements  int
		maxMonthElements int
	}

	tests := []struct {
		name     string
		fields   fields
		args     args
		expected Pipelines
	}{
		{
			name: "Empty field, not changes expected",
			fields: fields{
				rawData:        []PipelineEntry{},
				daySummaries:   []Summary{},
				weekSummaries:  []Summary{},
				monthSummaries: []Summary{},
			},
			args: args{
				maxRawElements:   30,
				maxDayElements:   30,
				maxWeekElements:  8,
				maxMonthElements: 10,
			},
			expected: Pipelines{
				RawData:        []PipelineEntry{},
				DaySummaries:   []Summary{},
				WeekSummaries:  []Summary{},
				MonthSummaries: []Summary{},
			},
		},
		{
			name: "Non empty field but not above limit, no changes expected",
			fields: fields{
				rawData:        []PipelineEntry{{ID: 42}},
				daySummaries:   []Summary{{Date: testhelper.TestDateDay1}},
				weekSummaries:  []Summary{{Date: testhelper.TestDateWeek1}},
				monthSummaries: []Summary{{Date: testhelper.TestDateMonth1}},
			},
			args: args{
				maxRawElements:   10,
				maxDayElements:   10,
				maxWeekElements:  2,
				maxMonthElements: 1,
			},
			expected: Pipelines{
				RawData:        []PipelineEntry{{ID: 42}},
				DaySummaries:   []Summary{{Date: testhelper.TestDateDay1}},
				WeekSummaries:  []Summary{{Date: testhelper.TestDateWeek1}},
				MonthSummaries: []Summary{{Date: testhelper.TestDateMonth1}},
			},
		},
		{
			name: "Non empty field and all above limit, the first elements are expected to be removed.",
			fields: fields{
				rawData: []PipelineEntry{
					{ID: 42, StartedAt: time.Date(2021, 12, 24, 12, 0, 0, 0, time.UTC)},
					{ID: 42, StartedAt: time.Date(2021, 12, 23, 12, 0, 0, 0, time.UTC)},
					{ID: 42, StartedAt: time.Date(2021, 12, 25, 12, 0, 0, 0, time.UTC)},
					{ID: 42, StartedAt: time.Date(2021, 12, 21, 12, 0, 0, 0, time.UTC)},
				},
				daySummaries:   []Summary{{Date: testhelper.TestDateDay3}, {Date: testhelper.TestDateDay2}, {Date: testhelper.TestDateDay1}, {Date: testhelper.TestDateDay4}},
				weekSummaries:  []Summary{{Date: testhelper.TestDateWeek2}, {Date: testhelper.TestDateWeek4}, {Date: testhelper.TestDateWeek3}, {Date: testhelper.TestDateWeek2}},
				monthSummaries: []Summary{{Date: testhelper.TestDateMonth4}, {Date: testhelper.TestDateMonth2}, {Date: testhelper.TestDateMonth1}, {Date: testhelper.TestDateMonth3}},
			},
			args: args{
				maxRawElements:   3,
				maxDayElements:   1,
				maxWeekElements:  3,
				maxMonthElements: 2,
			},
			expected: Pipelines{
				RawData: []PipelineEntry{
					{ID: 42, StartedAt: time.Date(2021, 12, 23, 12, 0, 0, 0, time.UTC)},
					{ID: 42, StartedAt: time.Date(2021, 12, 24, 12, 0, 0, 0, time.UTC)},
					{ID: 42, StartedAt: time.Date(2021, 12, 25, 12, 0, 0, 0, time.UTC)},
				},
				DaySummaries:   []Summary{{Date: testhelper.TestDateDay4}},
				WeekSummaries:  []Summary{{Date: testhelper.TestDateWeek2}, {Date: testhelper.TestDateWeek3}, {Date: testhelper.TestDateWeek4}},
				MonthSummaries: []Summary{{Date: testhelper.TestDateMonth3}, {Date: testhelper.TestDateMonth4}},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			j := Pipelines{
				RawData:        tt.fields.rawData,
				DaySummaries:   tt.fields.daySummaries,
				WeekSummaries:  tt.fields.weekSummaries,
				MonthSummaries: tt.fields.monthSummaries,
			}
			j.CleanUp(tt.args.maxRawElements, tt.args.maxDayElements, tt.args.maxWeekElements, tt.args.maxMonthElements)
			testhelper.DeepEqual(t, j, tt.expected)
		})
	}
}

func TestPipeline_AddEntry(t *testing.T) {
	type fields struct {
		RawData        []PipelineEntry
		DaySummaries   []Summary
		WeekSummaries  []Summary
		MonthSummaries []Summary
	}
	type args struct {
		pipeline PipelineEntry
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		expect fields
	}{
		{
			name: "add new entry into empty list",
			fields: fields{
				RawData:        []PipelineEntry{},
				DaySummaries:   []Summary{},
				WeekSummaries:  []Summary{},
				MonthSummaries: []Summary{},
			},
			args: args{
				pipeline: PipelineEntry{
					ID:        10,
					StartedAt: time.Date(2021, 12, 24, 12, 00, 42, 0, time.UTC),
					Duration:  142.0,
				},
			},
			expect: fields{
				RawData: []PipelineEntry{
					{
						ID:        10,
						StartedAt: time.Date(2021, 12, 24, 12, 00, 42, 0, time.UTC),
						Duration:  142.0,
					},
				},
				DaySummaries:   []Summary{},
				WeekSummaries:  []Summary{},
				MonthSummaries: []Summary{},
			},
		},
		{
			name: "add new entry into not empty list",
			fields: fields{
				RawData: []PipelineEntry{
					{
						ID:        10,
						StartedAt: time.Date(2021, 12, 24, 13, 00, 42, 0, time.UTC),
					},
					{
						ID:        11,
						StartedAt: time.Date(2021, 12, 24, 10, 00, 42, 0, time.UTC),
					},
				},
				DaySummaries:   []Summary{},
				WeekSummaries:  []Summary{},
				MonthSummaries: []Summary{},
			},
			args: args{
				pipeline: PipelineEntry{
					ID:        12,
					StartedAt: time.Date(2021, 12, 24, 12, 00, 42, 0, time.UTC),
				},
			},
			expect: fields{
				RawData: []PipelineEntry{
					{
						ID:        10,
						StartedAt: time.Date(2021, 12, 24, 13, 00, 42, 0, time.UTC),
					},
					{
						ID:        11,
						StartedAt: time.Date(2021, 12, 24, 10, 00, 42, 0, time.UTC),
					},
					{
						ID:        12,
						StartedAt: time.Date(2021, 12, 24, 12, 00, 42, 0, time.UTC),
					},
				},
				DaySummaries:   []Summary{},
				WeekSummaries:  []Summary{},
				MonthSummaries: []Summary{},
			},
		},
		{
			name: "add existing entry into not empty list, will be ignored",
			fields: fields{
				RawData: []PipelineEntry{
					{
						ID:        10,
						StartedAt: time.Date(2021, 12, 24, 13, 00, 42, 0, time.UTC),
					},
					{
						ID:        11,
						StartedAt: time.Date(2021, 12, 24, 10, 00, 42, 0, time.UTC),
					},
				},
				DaySummaries:   []Summary{},
				WeekSummaries:  []Summary{},
				MonthSummaries: []Summary{},
			},
			args: args{
				pipeline: PipelineEntry{
					ID:        11,
					StartedAt: time.Date(2021, 12, 24, 12, 00, 42, 0, time.UTC),
				},
			},
			expect: fields{
				RawData: []PipelineEntry{
					{
						ID:        10,
						StartedAt: time.Date(2021, 12, 24, 13, 00, 42, 0, time.UTC),
					},
					{
						ID:        11,
						StartedAt: time.Date(2021, 12, 24, 10, 00, 42, 0, time.UTC),
					},
				},
				DaySummaries:   []Summary{},
				WeekSummaries:  []Summary{},
				MonthSummaries: []Summary{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			j := &Pipelines{
				RawData:        tt.fields.RawData,
				DaySummaries:   tt.fields.DaySummaries,
				WeekSummaries:  tt.fields.WeekSummaries,
				MonthSummaries: tt.fields.MonthSummaries,
			}
			j.AddEntry(tt.args.pipeline)
			testhelper.DeepEqual(t, j.RawData, tt.expect.RawData)
			testhelper.DeepEqual(t, j.DaySummaries, tt.expect.DaySummaries)
			testhelper.DeepEqual(t, j.WeekSummaries, tt.expect.WeekSummaries)
			testhelper.DeepEqual(t, j.MonthSummaries, tt.expect.MonthSummaries)
		})
	}
}

func Test_getPipelineSummary(t *testing.T) {
	type args struct {
		key     time.Time
		entries []PipelineEntry
	}
	tests := []struct {
		name string
		args args
		want Summary
	}{
		{
			name: "generate Summary for empty list",
			args: args{
				key:     testhelper.TestDateDay1,
				entries: []PipelineEntry{},
			},
			want: Summary{
				Date:                 testhelper.TestDateDay1,
				Min:                  0.0,
				Max:                  0.0,
				Average:              0.0,
				SuccessCnt:           0,
				TotalCnt:             0,
				ArtifactsSizeAverage: 0.0,
				Coverage:             0.0,
				TestCnt:              0,
				TestSuccessCnt:       0,
			},
		},
		{
			name: "generate Summary for list with one element",
			args: args{
				key: testhelper.TestDateDay1,
				entries: []PipelineEntry{
					{
						Status:        "success",
						StartedAt:     testhelper.TestDateDay1,
						Duration:      100.0,
						Coverage:      10.34,
						ArtifactsSize: 42.2,
						PipelineTestReport: TestReport{
							Count:   10,
							Success: 8,
						},
					},
				},
			},
			want: Summary{
				Date:                 testhelper.TestDateDay1,
				Min:                  100.0,
				Max:                  100.0,
				Average:              100.0,
				SuccessCnt:           1,
				TotalCnt:             1,
				ArtifactsSizeAverage: 42.2,
				Coverage:             10.34,
				TestCnt:              10,
				TestSuccessCnt:       8,
			},
		},
		{
			name: "generate Summary for list with multiple elements",
			args: args{
				key: testhelper.TestDateDay1,
				entries: []PipelineEntry{
					{
						Status:        "success",
						StartedAt:     testhelper.TestDateDay1,
						Duration:      100.0,
						Coverage:      10.,
						ArtifactsSize: 20.0,
						PipelineTestReport: TestReport{
							Count:   10,
							Success: 8,
						},
					},
					{
						Status:        "success",
						StartedAt:     testhelper.TestDateDay1,
						Duration:      50.0,
						Coverage:      20.,
						ArtifactsSize: 10.0,
						PipelineTestReport: TestReport{
							Count:   12,
							Success: 6,
						},
					},
					{
						Status:        "failure",
						StartedAt:     testhelper.TestDateDay1,
						Duration:      20.0,
						Coverage:      14.,
						ArtifactsSize: 100.0,
						PipelineTestReport: TestReport{
							Count:   115,
							Success: 7,
						},
					},
				},
			},
			want: Summary{
				Date:                 testhelper.TestDateDay1,
				Min:                  50.0,
				Max:                  100.0,
				Average:              75.0,
				SuccessCnt:           2,
				TotalCnt:             3,
				ArtifactsSizeAverage: 15.0,
				Coverage:             15.,
				TestCnt:              11,
				TestSuccessCnt:       7,
			},
		},
		{
			name: "generate Summary for list with multiple elements all failed",
			args: args{
				key: testhelper.TestDateDay1,
				entries: []PipelineEntry{
					{
						Status:    "failed",
						StartedAt: testhelper.TestDateDay1,
						Duration:  100.0,
					},
					{
						Status:    "failed",
						StartedAt: testhelper.TestDateDay1,
						Duration:  50.0,
					},
					{
						Status:    "failure",
						StartedAt: testhelper.TestDateDay1,
						Duration:  20.0,
					},
				},
			},
			want: Summary{
				Date:                 testhelper.TestDateDay1,
				Min:                  0.0,
				Max:                  0.0,
				Average:              0.0,
				SuccessCnt:           0,
				TotalCnt:             3,
				ArtifactsSizeAverage: 0.0,
				Coverage:             0.0,
				TestCnt:              0,
				TestSuccessCnt:       0,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := getPipelineSummary(tt.args.key, tt.args.entries)
			testhelper.DeepEqual(t, got, tt.want)
		})
	}
}

func Test_mapPipelineEntriesIntoDayBuckets(t *testing.T) {
	type args struct {
		jobs  []PipelineEntry
		after time.Time
	}
	tests := []struct {
		name string
		args args
		want map[time.Time][]PipelineEntry
	}{
		{
			name: "test with empty list",
			args: args{
				jobs:  []PipelineEntry{},
				after: testhelper.TestDateDay1,
			},
			want: map[time.Time][]PipelineEntry{},
		},
		{
			name: "test with all elements before the after date",
			args: args{
				jobs: []PipelineEntry{
					{StartedAt: testhelper.TestDateDay1.Add(1 * time.Hour)},
					{StartedAt: testhelper.TestDateDay1.Add(2 * time.Hour)},
					{StartedAt: testhelper.TestDateDay1.Add(3 * time.Hour)},
				},
				after: testhelper.TestDateDay1,
			},
			want: map[time.Time][]PipelineEntry{},
		},
		{
			name: "test with all elements before and after the after date",
			args: args{
				jobs: []PipelineEntry{
					{StartedAt: testhelper.TestDateDay1.Add(1 * time.Hour)},
					{StartedAt: testhelper.TestDateDay1.Add(2 * time.Hour)},
					{StartedAt: testhelper.TestDateDay1.Add(3 * time.Hour)},
					{StartedAt: testhelper.TestDateDay2.Add(1 * time.Hour)},
					{StartedAt: testhelper.TestDateDay2.Add(2 * time.Hour)},
					{StartedAt: testhelper.TestDateDay2.Add(3 * time.Hour)},
					{StartedAt: testhelper.TestDateDay3.Add(1 * time.Hour)},
					{StartedAt: testhelper.TestDateDay3.Add(2 * time.Hour)},
					{StartedAt: testhelper.TestDateDay3.Add(3 * time.Hour)},
				},
				after: testhelper.TestDateDay1,
			},
			want: map[time.Time][]PipelineEntry{
				testhelper.TestDateDay2: {
					{StartedAt: testhelper.TestDateDay2.Add(1 * time.Hour)},
					{StartedAt: testhelper.TestDateDay2.Add(2 * time.Hour)},
					{StartedAt: testhelper.TestDateDay2.Add(3 * time.Hour)},
				},
				testhelper.TestDateDay3: {
					{StartedAt: testhelper.TestDateDay3.Add(1 * time.Hour)},
					{StartedAt: testhelper.TestDateDay3.Add(2 * time.Hour)},
					{StartedAt: testhelper.TestDateDay3.Add(3 * time.Hour)},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := mapPipelineEntriesIntoDayBuckets(tt.args.jobs, tt.args.after)
			testhelper.DeepEqual(t, got, tt.want)
		})
	}
}

func TestPipeline_generateDaySummaries(t *testing.T) {
	type fields struct {
		RawData      []PipelineEntry
		DaySummaries []Summary
	}
	tests := []struct {
		name   string
		fields fields
		expect fields
	}{
		{
			name: "generate day summaries for empty raw and day list",
			fields: fields{
				RawData:      []PipelineEntry{},
				DaySummaries: []Summary{},
			},
			expect: fields{
				RawData:      []PipelineEntry{},
				DaySummaries: []Summary{},
			},
		},
		{
			name: "generate day summaries for raw list with empty day list",
			fields: fields{
				RawData: []PipelineEntry{
					{StartedAt: testhelper.TestDateDay1.Add(3 * time.Hour), Duration: 100.0, Status: "success"},
					{StartedAt: testhelper.TestDateDay1.Add(2 * time.Hour), Duration: 50.0, Status: "success"},
					{StartedAt: testhelper.TestDateDay1.Add(1 * time.Hour), Duration: 100.0, Status: "success"},
					{StartedAt: testhelper.TestDateDay1.Add(5 * time.Hour), Duration: 50.0, Status: "success"},
					{StartedAt: testhelper.TestDateDay2.Add(3 * time.Hour), Duration: 10.0, Status: "failed"},
					{StartedAt: testhelper.TestDateDay2.Add(2 * time.Hour), Duration: 10.0, Status: "success"},
					{StartedAt: testhelper.TestDateDay2.Add(1 * time.Hour), Duration: 10.0, Status: "success"},
					{StartedAt: testhelper.TestDateDay2.Add(5 * time.Hour), Duration: 7.0, Status: "success"},
					{StartedAt: testhelper.TestDateDay3.Add(3 * time.Hour), Duration: 1000.0, Status: "failed"},
					{StartedAt: testhelper.TestDateDay3.Add(2 * time.Hour), Duration: 500.0, Status: "failed"},
					{StartedAt: testhelper.TestDateDay3.Add(1 * time.Hour), Duration: 1000.0, Status: "success"},
					{StartedAt: testhelper.TestDateDay3.Add(5 * time.Hour), Duration: 500.0, Status: "success"},
				},
				DaySummaries: []Summary{},
			},
			expect: fields{
				RawData: []PipelineEntry{
					{StartedAt: testhelper.TestDateDay1.Add(1 * time.Hour), Duration: 100.0, Status: "success"},
					{StartedAt: testhelper.TestDateDay1.Add(2 * time.Hour), Duration: 50.0, Status: "success"},
					{StartedAt: testhelper.TestDateDay1.Add(3 * time.Hour), Duration: 100.0, Status: "success"},
					{StartedAt: testhelper.TestDateDay1.Add(5 * time.Hour), Duration: 50.0, Status: "success"},
					{StartedAt: testhelper.TestDateDay2.Add(1 * time.Hour), Duration: 10.0, Status: "success"},
					{StartedAt: testhelper.TestDateDay2.Add(2 * time.Hour), Duration: 10.0, Status: "success"},
					{StartedAt: testhelper.TestDateDay2.Add(3 * time.Hour), Duration: 10.0, Status: "failed"},
					{StartedAt: testhelper.TestDateDay2.Add(5 * time.Hour), Duration: 7.0, Status: "success"},
					{StartedAt: testhelper.TestDateDay3.Add(1 * time.Hour), Duration: 1000.0, Status: "success"},
					{StartedAt: testhelper.TestDateDay3.Add(2 * time.Hour), Duration: 500.0, Status: "failed"},
					{StartedAt: testhelper.TestDateDay3.Add(3 * time.Hour), Duration: 1000.0, Status: "failed"},
					{StartedAt: testhelper.TestDateDay3.Add(5 * time.Hour), Duration: 500.0, Status: "success"},
				},
				DaySummaries: []Summary{
					{Date: testhelper.TestDateDay1, Min: 50, Max: 100, Average: 75, SuccessCnt: 4, TotalCnt: 4},
					{Date: testhelper.TestDateDay2, Min: 7, Max: 10, Average: 9, SuccessCnt: 3, TotalCnt: 4},
					{Date: testhelper.TestDateDay3, Min: 500, Max: 1000, Average: 750, SuccessCnt: 2, TotalCnt: 4},
				},
			},
		},
		{
			name: "generate day summaries for raw list with previous elements within day list",
			fields: fields{
				RawData: []PipelineEntry{
					{StartedAt: testhelper.TestDateDay1.Add(3 * time.Hour), Duration: 100.0, Status: "success"},
					{StartedAt: testhelper.TestDateDay1.Add(2 * time.Hour), Duration: 50.0, Status: "success"},
					{StartedAt: testhelper.TestDateDay1.Add(1 * time.Hour), Duration: 100.0, Status: "success"},
					{StartedAt: testhelper.TestDateDay1.Add(5 * time.Hour), Duration: 50.0, Status: "success"},
					{StartedAt: testhelper.TestDateDay2.Add(3 * time.Hour), Duration: 10.0, Status: "failed"},
					{StartedAt: testhelper.TestDateDay2.Add(2 * time.Hour), Duration: 10.0, Status: "success"},
					{StartedAt: testhelper.TestDateDay2.Add(1 * time.Hour), Duration: 10.0, Status: "success"},
					{StartedAt: testhelper.TestDateDay2.Add(5 * time.Hour), Duration: 7.0, Status: "success"},
					{StartedAt: testhelper.TestDateDay3.Add(3 * time.Hour), Duration: 1000.0, Status: "failed"},
					{StartedAt: testhelper.TestDateDay3.Add(2 * time.Hour), Duration: 500.0, Status: "failed"},
					{StartedAt: testhelper.TestDateDay3.Add(1 * time.Hour), Duration: 1000.0, Status: "success"},
					{StartedAt: testhelper.TestDateDay3.Add(5 * time.Hour), Duration: 500.0, Status: "success"},
				},
				DaySummaries: []Summary{
					{Date: testhelper.TestDateDay1, Min: 1.0, Max: 100.8, Average: 8.9, SuccessCnt: 2, TotalCnt: 3},
				},
			},
			expect: fields{
				RawData: []PipelineEntry{
					{StartedAt: testhelper.TestDateDay1.Add(1 * time.Hour), Duration: 100.0, Status: "success"},
					{StartedAt: testhelper.TestDateDay1.Add(2 * time.Hour), Duration: 50.0, Status: "success"},
					{StartedAt: testhelper.TestDateDay1.Add(3 * time.Hour), Duration: 100.0, Status: "success"},
					{StartedAt: testhelper.TestDateDay1.Add(5 * time.Hour), Duration: 50.0, Status: "success"},
					{StartedAt: testhelper.TestDateDay2.Add(1 * time.Hour), Duration: 10.0, Status: "success"},
					{StartedAt: testhelper.TestDateDay2.Add(2 * time.Hour), Duration: 10.0, Status: "success"},
					{StartedAt: testhelper.TestDateDay2.Add(3 * time.Hour), Duration: 10.0, Status: "failed"},
					{StartedAt: testhelper.TestDateDay2.Add(5 * time.Hour), Duration: 7.0, Status: "success"},
					{StartedAt: testhelper.TestDateDay3.Add(1 * time.Hour), Duration: 1000.0, Status: "success"},
					{StartedAt: testhelper.TestDateDay3.Add(2 * time.Hour), Duration: 500.0, Status: "failed"},
					{StartedAt: testhelper.TestDateDay3.Add(3 * time.Hour), Duration: 1000.0, Status: "failed"},
					{StartedAt: testhelper.TestDateDay3.Add(5 * time.Hour), Duration: 500.0, Status: "success"},
				},
				DaySummaries: []Summary{
					{Date: testhelper.TestDateDay1, Min: 1.0, Max: 100.8, Average: 8.9, SuccessCnt: 2, TotalCnt: 3},
					{Date: testhelper.TestDateDay2, Min: 7, Max: 10, Average: 9, SuccessCnt: 3, TotalCnt: 4},
					{Date: testhelper.TestDateDay3, Min: 500, Max: 1000, Average: 750, SuccessCnt: 2, TotalCnt: 4},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Pipelines{
				RawData:        tt.fields.RawData,
				DaySummaries:   tt.fields.DaySummaries,
				WeekSummaries:  []Summary{},
				MonthSummaries: []Summary{},
			}
			p.generateDaySummaries()
			testhelper.DeepEqual(t, p.RawData, tt.expect.RawData)
			testhelper.DeepEqual(t, p.DaySummaries, tt.expect.DaySummaries)
		})
	}
}

func TestPipelines_generateWeekSummaries(t *testing.T) {
	type fields struct {
		RawData       []PipelineEntry
		WeekSummaries []Summary
	}
	tests := []struct {
		name   string
		fields fields
		expect fields
	}{
		{
			name: "empty lists",
			fields: fields{
				RawData:       []PipelineEntry{},
				WeekSummaries: []Summary{},
			},
			expect: fields{
				RawData:       []PipelineEntry{},
				WeekSummaries: []Summary{},
			},
		},
		{
			name: "generate week summaries for raw list with empty week list",
			fields: fields{
				RawData: []PipelineEntry{
					{StartedAt: testhelper.TestDateWeek1.Add(2 * time.Hour), Duration: 50.0, Status: StatusSuccess},
					{StartedAt: testhelper.TestDateWeek1.Add(3 * time.Hour), Duration: 100.0, Status: StatusSuccess},
					{StartedAt: testhelper.TestDateWeek1.Add(5 * time.Hour), Duration: 50.0, Status: StatusSuccess},
					{StartedAt: testhelper.TestDateWeek1.Add(1 * time.Hour), Duration: 100.0, Status: StatusSuccess},
					{StartedAt: testhelper.TestDateWeek2.Add(3 * time.Hour), Duration: 10.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateWeek2.Add(2 * time.Hour), Duration: 10.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek2.Add(1 * time.Hour), Duration: 10.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek2.Add(5 * time.Hour), Duration: 7.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek3.Add(3 * time.Hour), Duration: 1000.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateWeek3.Add(2 * time.Hour), Duration: 500.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateWeek3.Add(1 * time.Hour), Duration: 1000.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek3.Add(5 * time.Hour), Duration: 500.0, Status: "SUCCESS"},
				},
				WeekSummaries: []Summary{},
			},
			expect: fields{
				RawData: []PipelineEntry{
					{StartedAt: testhelper.TestDateWeek1.Add(1 * time.Hour), Duration: 100.0, Status: StatusSuccess},
					{StartedAt: testhelper.TestDateWeek1.Add(2 * time.Hour), Duration: 50.0, Status: StatusSuccess},
					{StartedAt: testhelper.TestDateWeek1.Add(3 * time.Hour), Duration: 100.0, Status: StatusSuccess},
					{StartedAt: testhelper.TestDateWeek1.Add(5 * time.Hour), Duration: 50.0, Status: StatusSuccess},
					{StartedAt: testhelper.TestDateWeek2.Add(1 * time.Hour), Duration: 10.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek2.Add(2 * time.Hour), Duration: 10.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek2.Add(3 * time.Hour), Duration: 10.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateWeek2.Add(5 * time.Hour), Duration: 7.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek3.Add(1 * time.Hour), Duration: 1000.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek3.Add(2 * time.Hour), Duration: 500.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateWeek3.Add(3 * time.Hour), Duration: 1000.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateWeek3.Add(5 * time.Hour), Duration: 500.0, Status: "SUCCESS"},
				},
				WeekSummaries: []Summary{
					{Date: testhelper.TestDateWeek1, Min: 50, Max: 100, Average: 75, SuccessCnt: 4, TotalCnt: 4},
					{Date: testhelper.TestDateWeek2, Min: 7, Max: 10, Average: 9, SuccessCnt: 3, TotalCnt: 4},
					{Date: testhelper.TestDateWeek3, Min: 500, Max: 1000, Average: 750, SuccessCnt: 2, TotalCnt: 4},
				},
			},
		},
		{
			name: "generate week summaries for raw list with previous elements within week list",
			fields: fields{
				RawData: []PipelineEntry{
					{StartedAt: testhelper.TestDateWeek1.Add(3 * time.Hour), Duration: 100.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek1.Add(2 * time.Hour), Duration: 50.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek1.Add(1 * time.Hour), Duration: 100.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek1.Add(5 * time.Hour), Duration: 50.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek2.Add(3 * time.Hour), Duration: 10.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateWeek2.Add(2 * time.Hour), Duration: 10.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek2.Add(1 * time.Hour), Duration: 10.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek2.Add(5 * time.Hour), Duration: 7.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek3.Add(3 * time.Hour), Duration: 1000.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateWeek3.Add(2 * time.Hour), Duration: 500.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateWeek3.Add(1 * time.Hour), Duration: 1000.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek3.Add(5 * time.Hour), Duration: 500.0, Status: "SUCCESS"},
				},
				WeekSummaries: []Summary{
					{Date: testhelper.TestDateWeek1, Min: 1.0, Max: 100.8, Average: 8.9, SuccessCnt: 2, TotalCnt: 3},
				},
			},
			expect: fields{
				RawData: []PipelineEntry{
					{StartedAt: testhelper.TestDateWeek1.Add(1 * time.Hour), Duration: 100.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek1.Add(2 * time.Hour), Duration: 50.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek1.Add(3 * time.Hour), Duration: 100.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek1.Add(5 * time.Hour), Duration: 50.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek2.Add(1 * time.Hour), Duration: 10.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek2.Add(2 * time.Hour), Duration: 10.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek2.Add(3 * time.Hour), Duration: 10.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateWeek2.Add(5 * time.Hour), Duration: 7.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek3.Add(1 * time.Hour), Duration: 1000.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek3.Add(2 * time.Hour), Duration: 500.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateWeek3.Add(3 * time.Hour), Duration: 1000.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateWeek3.Add(5 * time.Hour), Duration: 500.0, Status: "SUCCESS"},
				},
				WeekSummaries: []Summary{
					{Date: testhelper.TestDateWeek1, Min: 1.0, Max: 100.8, Average: 8.9, SuccessCnt: 2, TotalCnt: 3},
					{Date: testhelper.TestDateWeek2, Min: 7, Max: 10, Average: 9, SuccessCnt: 3, TotalCnt: 4},
					{Date: testhelper.TestDateWeek3, Min: 500, Max: 1000, Average: 750, SuccessCnt: 2, TotalCnt: 4},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Pipelines{
				RawData:        tt.fields.RawData,
				DaySummaries:   []Summary{},
				WeekSummaries:  tt.fields.WeekSummaries,
				MonthSummaries: []Summary{},
			}
			p.generateWeekSummaries()
			testhelper.DeepEqual(t, p.RawData, tt.expect.RawData)
			testhelper.DeepEqual(t, p.WeekSummaries, tt.expect.WeekSummaries)
		})
	}
}

func TestPipelines_generateMonthSummaries(t *testing.T) {
	curMonth := time.Date(time.Now().Year(), time.Now().Month(), 1, 0, 0, 0, 0, time.UTC)
	prevMonth := time.Date(time.Now().Year(), time.Now().Month()-1, 1, 0, 0, 0, 0, time.UTC)

	type fields struct {
		DaySummaries   []Summary
		MonthSummaries []Summary
	}
	tests := []struct {
		name   string
		fields fields
		expect fields
	}{
		{
			name: "empty day summaries create empty month entry",
			fields: fields{
				DaySummaries:   []Summary{},
				MonthSummaries: []Summary{},
			},
			expect: fields{
				DaySummaries: []Summary{},
				MonthSummaries: []Summary{
					{
						Date:                 prevMonth,
						Min:                  0,
						Max:                  0,
						Average:              0,
						SuccessCnt:           0,
						TotalCnt:             0,
						ArtifactsSizeAverage: 0,
						Coverage:             0,
						TestCnt:              0,
						TestSuccessCnt:       0,
					},
				},
			},
		},
		{
			name: "empty day summaries with predefined month summary (no change)",
			fields: fields{
				DaySummaries: []Summary{},
				MonthSummaries: []Summary{
					{
						Date:                 prevMonth,
						Min:                  10,
						Max:                  20,
						Average:              12,
						SuccessCnt:           10,
						TotalCnt:             10,
						ArtifactsSizeAverage: 42,
						Coverage:             20,
						TestCnt:              10,
						TestSuccessCnt:       10,
					},
				},
			},
			expect: fields{
				DaySummaries: []Summary{},
				MonthSummaries: []Summary{
					{
						Date:                 prevMonth,
						Min:                  10,
						Max:                  20,
						Average:              12,
						SuccessCnt:           10,
						TotalCnt:             10,
						ArtifactsSizeAverage: 42,
						Coverage:             20,
						TestCnt:              10,
						TestSuccessCnt:       10,
					},
				},
			},
		},
		{
			name: "day summaries with no month summary",
			fields: fields{
				DaySummaries: []Summary{
					{
						Date:                 curMonth.AddDate(0, 0, -10),
						Min:                  15,
						Max:                  35,
						Average:              20,
						SuccessCnt:           5,
						TotalCnt:             3,
						ArtifactsSizeAverage: 42,
						Coverage:             30,
						TestCnt:              12,
						TestSuccessCnt:       12,
					},
					{
						Date:                 curMonth.AddDate(0, 0, 10),
						Min:                  105,
						Max:                  305,
						Average:              200,
						SuccessCnt:           50,
						TotalCnt:             32,
						ArtifactsSizeAverage: 22,
					},
					{
						Date:                 curMonth.AddDate(0, -1, -10),
						Min:                  105,
						Max:                  305,
						Average:              200,
						SuccessCnt:           50,
						TotalCnt:             32,
						ArtifactsSizeAverage: 32,
					},
				},
				MonthSummaries: []Summary{},
			},
			expect: fields{
				DaySummaries: []Summary{
					{
						Date:                 curMonth.AddDate(0, 0, -10),
						Min:                  15,
						Max:                  35,
						Average:              20,
						SuccessCnt:           5,
						TotalCnt:             3,
						Coverage:             30,
						TestCnt:              12,
						TestSuccessCnt:       12,
						ArtifactsSizeAverage: 42,
					},
					{
						Date:                 curMonth.AddDate(0, 0, 10),
						Min:                  105,
						Max:                  305,
						Average:              200,
						SuccessCnt:           50,
						TotalCnt:             32,
						ArtifactsSizeAverage: 22,
					},
					{
						Date:                 curMonth.AddDate(0, -1, -10),
						Min:                  105,
						Max:                  305,
						Average:              200,
						SuccessCnt:           50,
						TotalCnt:             32,
						ArtifactsSizeAverage: 32,
					},
				},
				MonthSummaries: []Summary{
					{
						Date:                 prevMonth,
						Min:                  15,
						Max:                  35,
						Average:              20,
						SuccessCnt:           5,
						TotalCnt:             3,
						ArtifactsSizeAverage: 42,
						Coverage:             30,
						TestCnt:              12,
						TestSuccessCnt:       12,
					},
				},
			},
		},
		{
			name: "day summaries with previous month summaries",
			fields: fields{
				DaySummaries: []Summary{
					{
						Date:                 curMonth.AddDate(0, 0, -10),
						Min:                  15,
						Max:                  35,
						Average:              20,
						SuccessCnt:           5,
						TotalCnt:             3,
						Coverage:             30,
						ArtifactsSizeAverage: 22,
						TestCnt:              12,
						TestSuccessCnt:       12,
					},
					{
						Date:       curMonth.AddDate(0, 0, 10),
						Min:        105,
						Max:        305,
						Average:    200,
						SuccessCnt: 50,
						TotalCnt:   32,
					},
					{
						Date:       curMonth.AddDate(0, -1, -10),
						Min:        105,
						Max:        305,
						Average:    200,
						SuccessCnt: 50,
						TotalCnt:   32,
					},
				},
				MonthSummaries: []Summary{
					{
						Date:                 prevMonth.AddDate(0, -1, 0),
						Min:                  150,
						Max:                  350,
						Average:              200,
						SuccessCnt:           50,
						TotalCnt:             30,
						Coverage:             300,
						ArtifactsSizeAverage: 220,
						TestCnt:              120,
						TestSuccessCnt:       120,
					},
				},
			},
			expect: fields{
				DaySummaries: []Summary{
					{
						Date:                 curMonth.AddDate(0, 0, -10),
						Min:                  15,
						Max:                  35,
						Average:              20,
						SuccessCnt:           5,
						TotalCnt:             3,
						ArtifactsSizeAverage: 22,
						Coverage:             30,
						TestCnt:              12,
						TestSuccessCnt:       12,
					},
					{
						Date:       curMonth.AddDate(0, 0, 10),
						Min:        105,
						Max:        305,
						Average:    200,
						SuccessCnt: 50,
						TotalCnt:   32,
					},
					{
						Date:       curMonth.AddDate(0, -1, -10),
						Min:        105,
						Max:        305,
						Average:    200,
						SuccessCnt: 50,
						TotalCnt:   32,
					},
				},
				MonthSummaries: []Summary{
					{
						Date:                 prevMonth.AddDate(0, -1, 0),
						Min:                  150,
						Max:                  350,
						Average:              200,
						SuccessCnt:           50,
						TotalCnt:             30,
						ArtifactsSizeAverage: 220,
						Coverage:             300,
						TestCnt:              120,
						TestSuccessCnt:       120,
					},
					{
						Date:                 prevMonth,
						Min:                  15,
						Max:                  35,
						Average:              20,
						SuccessCnt:           5,
						TotalCnt:             3,
						ArtifactsSizeAverage: 22,
						Coverage:             30,
						TestCnt:              12,
						TestSuccessCnt:       12,
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Pipelines{
				RawData:        []PipelineEntry{},
				DaySummaries:   tt.fields.DaySummaries,
				WeekSummaries:  []Summary{},
				MonthSummaries: tt.fields.MonthSummaries,
			}
			p.generateMonthSummaries()
			testhelper.DeepEqual(t, p.DaySummaries, tt.expect.DaySummaries)
			testhelper.DeepEqual(t, p.MonthSummaries, tt.expect.MonthSummaries)
		})
	}
}
