// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package data

import (
	"sort"
	"testing"
	"time"

	"gitlab.com/mathweb/gitlab-pipeline-analyzer/internal/testhelper"
)

type dummyJobTraceFetcher struct {
	log string
	err error

	expectedCalls int
	calls         int
}

func (d *dummyJobTraceFetcher) GetJobTrace(int) (string, error) {
	d.calls++
	return d.log, d.err
}

func (d *dummyJobTraceFetcher) IgnoreJobTrace() bool {
	return false
}

func TestJob_CleanUp(t *testing.T) {
	type fields struct {
		rawData        []JobEntry
		daySummaries   []Summary
		weekSummaries  []Summary
		monthSummaries []Summary
	}
	type args struct {
		maxRawElements   int
		maxDayElements   int
		maxWeekElements  int
		maxMonthElements int
	}

	tests := []struct {
		name     string
		fields   fields
		args     args
		expected Job
	}{
		{
			name: "Empty field, not changes expected",
			fields: fields{
				rawData:        []JobEntry{},
				daySummaries:   []Summary{},
				weekSummaries:  []Summary{},
				monthSummaries: []Summary{},
			},
			args: args{
				maxRawElements:   30,
				maxDayElements:   30,
				maxWeekElements:  8,
				maxMonthElements: 10,
			},
			expected: Job{
				RawData:        []JobEntry{},
				DaySummaries:   []Summary{},
				WeekSummaries:  []Summary{},
				MonthSummaries: []Summary{},
			},
		},
		{
			name: "Non empty field but not above limit, no changes expected",
			fields: fields{
				rawData:        []JobEntry{{Name: "test"}},
				daySummaries:   []Summary{{Date: testhelper.TestDateDay1}},
				weekSummaries:  []Summary{{Date: testhelper.TestDateWeek1}},
				monthSummaries: []Summary{{Date: testhelper.TestDateMonth1}},
			},
			args: args{
				maxRawElements:   10,
				maxDayElements:   10,
				maxWeekElements:  2,
				maxMonthElements: 1,
			},
			expected: Job{
				RawData:        []JobEntry{{Name: "test"}},
				DaySummaries:   []Summary{{Date: testhelper.TestDateDay1}},
				WeekSummaries:  []Summary{{Date: testhelper.TestDateWeek1}},
				MonthSummaries: []Summary{{Date: testhelper.TestDateMonth1}},
			},
		},
		{
			name: "Non empty field and all above limit, the first elements are expected to be removed.",
			fields: fields{
				rawData: []JobEntry{
					{Name: "test1", StartedAt: time.Date(2021, 12, 24, 12, 0, 0, 0, time.UTC)},
					{Name: "test2", StartedAt: time.Date(2021, 12, 23, 12, 0, 0, 0, time.UTC)},
					{Name: "test3", StartedAt: time.Date(2021, 12, 25, 12, 0, 0, 0, time.UTC)},
					{Name: "test4", StartedAt: time.Date(2021, 12, 21, 12, 0, 0, 0, time.UTC)},
				},
				daySummaries:   []Summary{{Date: testhelper.TestDateDay3}, {Date: testhelper.TestDateDay2}, {Date: testhelper.TestDateDay1}, {Date: testhelper.TestDateDay4}},
				weekSummaries:  []Summary{{Date: testhelper.TestDateWeek2}, {Date: testhelper.TestDateWeek4}, {Date: testhelper.TestDateWeek3}, {Date: testhelper.TestDateWeek2}},
				monthSummaries: []Summary{{Date: testhelper.TestDateMonth4}, {Date: testhelper.TestDateMonth2}, {Date: testhelper.TestDateMonth1}, {Date: testhelper.TestDateMonth3}},
			},
			args: args{
				maxRawElements:   3,
				maxDayElements:   1,
				maxWeekElements:  3,
				maxMonthElements: 2,
			},
			expected: Job{
				RawData: []JobEntry{
					{Name: "test2", StartedAt: time.Date(2021, 12, 23, 12, 0, 0, 0, time.UTC)},
					{Name: "test1", StartedAt: time.Date(2021, 12, 24, 12, 0, 0, 0, time.UTC)},
					{Name: "test3", StartedAt: time.Date(2021, 12, 25, 12, 0, 0, 0, time.UTC)},
				},
				DaySummaries:   []Summary{{Date: testhelper.TestDateDay4}},
				WeekSummaries:  []Summary{{Date: testhelper.TestDateWeek2}, {Date: testhelper.TestDateWeek3}, {Date: testhelper.TestDateWeek4}},
				MonthSummaries: []Summary{{Date: testhelper.TestDateMonth3}, {Date: testhelper.TestDateMonth4}},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			j := Job{
				RawData:        tt.fields.rawData,
				DaySummaries:   tt.fields.daySummaries,
				WeekSummaries:  tt.fields.weekSummaries,
				MonthSummaries: tt.fields.monthSummaries,
			}
			j.CleanUp(tt.args.maxRawElements, tt.args.maxDayElements, tt.args.maxWeekElements, tt.args.maxMonthElements)
			testhelper.DeepEqual(t, j, tt.expected)
		})
	}
}

func TestJob_AddEntry(t *testing.T) {
	type fields struct {
		RawData        []JobEntry
		DaySummaries   []Summary
		WeekSummaries  []Summary
		MonthSummaries []Summary
	}
	type args struct {
		job     JobEntry
		fetcher dummyJobTraceFetcher
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		expect fields
	}{
		{
			name: "add new entry into empty list",
			fields: fields{
				RawData:        []JobEntry{},
				DaySummaries:   []Summary{},
				WeekSummaries:  []Summary{},
				MonthSummaries: []Summary{},
			},
			args: args{
				job: JobEntry{
					ID:             1,
					StartedAt:      time.Date(2021, 12, 24, 12, 00, 42, 0, time.UTC),
					QueuedDuration: 142.0,
					LogWebURL:      "demo",
				},
				fetcher: dummyJobTraceFetcher{
					log:           "demo",
					err:           nil,
					expectedCalls: 1,
				},
			},
			expect: fields{
				RawData: []JobEntry{
					{
						ID:             1,
						StartedAt:      time.Date(2021, 12, 24, 12, 00, 42, 0, time.UTC),
						QueuedDuration: 142.0,
						LogWebURL:      "demo",
					},
				},
				DaySummaries:   []Summary{},
				WeekSummaries:  []Summary{},
				MonthSummaries: []Summary{},
			},
		},
		{
			name: "add new entry into empty list with no log information",
			fields: fields{
				RawData:        []JobEntry{},
				DaySummaries:   []Summary{},
				WeekSummaries:  []Summary{},
				MonthSummaries: []Summary{},
			},
			args: args{
				job: JobEntry{
					ID:             1,
					StartedAt:      time.Date(2021, 12, 24, 12, 00, 42, 0, time.UTC),
					QueuedDuration: 142.0,
					LogWebURL:      "",
				},
				fetcher: dummyJobTraceFetcher{
					log:           "demo",
					err:           nil,
					expectedCalls: 0,
				},
			},
			expect: fields{
				RawData: []JobEntry{
					{
						ID:             1,
						StartedAt:      time.Date(2021, 12, 24, 12, 00, 42, 0, time.UTC),
						QueuedDuration: 142.0,
					},
				},
				DaySummaries:   []Summary{},
				WeekSummaries:  []Summary{},
				MonthSummaries: []Summary{},
			},
		},
		{
			name: "add new entry into not empty list",
			fields: fields{
				RawData: []JobEntry{
					{
						ID:        1,
						StartedAt: time.Date(2021, 12, 24, 13, 00, 42, 0, time.UTC),
					},
					{
						ID:        2,
						StartedAt: time.Date(2021, 12, 24, 10, 00, 42, 0, time.UTC),
					},
				},
				DaySummaries:   []Summary{},
				WeekSummaries:  []Summary{},
				MonthSummaries: []Summary{},
			},
			args: args{
				job: JobEntry{
					ID:        3,
					StartedAt: time.Date(2021, 12, 24, 12, 00, 42, 0, time.UTC),
					LogWebURL: "demo",
				},
				fetcher: dummyJobTraceFetcher{
					log:           "demo",
					err:           nil,
					expectedCalls: 1,
				},
			},
			expect: fields{
				RawData: []JobEntry{
					{
						ID:        1,
						StartedAt: time.Date(2021, 12, 24, 13, 00, 42, 0, time.UTC),
					},
					{
						ID:        2,
						StartedAt: time.Date(2021, 12, 24, 10, 00, 42, 0, time.UTC),
					},
					{
						ID:        3,
						StartedAt: time.Date(2021, 12, 24, 12, 00, 42, 0, time.UTC),
						LogWebURL: "demo",
					},
				},
				DaySummaries:   []Summary{},
				WeekSummaries:  []Summary{},
				MonthSummaries: []Summary{},
			},
		},
		{
			name: "add duplicated entry into not empty list",
			fields: fields{
				RawData: []JobEntry{
					{
						ID:        1,
						StartedAt: time.Date(2021, 12, 24, 13, 00, 42, 0, time.UTC),
					},
					{
						ID:        2,
						StartedAt: time.Date(2021, 12, 24, 10, 00, 42, 0, time.UTC),
					},
				},
				DaySummaries:   []Summary{},
				WeekSummaries:  []Summary{},
				MonthSummaries: []Summary{},
			},
			args: args{
				job: JobEntry{
					ID:        2,
					StartedAt: time.Date(2021, 12, 24, 12, 00, 42, 0, time.UTC),
					LogWebURL: "demo",
				},
				fetcher: dummyJobTraceFetcher{
					log:           "demo",
					err:           nil,
					expectedCalls: 0,
				},
			},
			expect: fields{
				RawData: []JobEntry{
					{
						ID:        1,
						StartedAt: time.Date(2021, 12, 24, 13, 00, 42, 0, time.UTC),
					},
					{
						ID:        2,
						StartedAt: time.Date(2021, 12, 24, 10, 00, 42, 0, time.UTC),
					},
				},
				DaySummaries:   []Summary{},
				WeekSummaries:  []Summary{},
				MonthSummaries: []Summary{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			j := &Job{
				RawData:        tt.fields.RawData,
				DaySummaries:   tt.fields.DaySummaries,
				WeekSummaries:  tt.fields.WeekSummaries,
				MonthSummaries: tt.fields.MonthSummaries,
			}
			j.AddEntry(tt.args.job, &tt.args.fetcher) //nolint: gosec //ignore memory aliasing in for loop in test code
			testhelper.DeepEqual(t, j.RawData, tt.expect.RawData)
			testhelper.DeepEqual(t, j.DaySummaries, tt.expect.DaySummaries)
			testhelper.DeepEqual(t, j.WeekSummaries, tt.expect.WeekSummaries)
			testhelper.DeepEqual(t, j.MonthSummaries, tt.expect.MonthSummaries)
			if tt.args.fetcher.expectedCalls != tt.args.fetcher.calls {
				t.Errorf("The trace fetcher was not called the expected number of times: want=%d got=%d", tt.args.fetcher.expectedCalls, tt.args.fetcher.calls)
			}
		})
	}
}

func Test_getSummary(t *testing.T) {
	type args struct {
		key     time.Time
		entries []JobEntry
	}
	tests := []struct {
		name string
		args args
		want Summary
	}{
		{
			name: "generate summary for empty list",
			args: args{
				key:     testhelper.TestDateDay1,
				entries: []JobEntry{},
			},
			want: Summary{
				Date:                 testhelper.TestDateDay1,
				Min:                  0.0,
				Max:                  0.0,
				Average:              0.0,
				SuccessCnt:           0,
				TotalCnt:             0,
				ArtifactsSizeAverage: 0.0,
				Coverage:             0,
				TestCnt:              0,
				TestSuccessCnt:       0,
			},
		},
		{
			name: "generate summary for list with one element",
			args: args{
				key: testhelper.TestDateDay1,
				entries: []JobEntry{
					{
						Status:        StatusSuccess,
						StartedAt:     testhelper.TestDateDay1,
						Duration:      100.0,
						ArtifactsSize: 42.,
					},
				},
			},
			want: Summary{
				Date:                 testhelper.TestDateDay1,
				Min:                  100.0,
				Max:                  100.0,
				Average:              100.0,
				SuccessCnt:           1,
				TotalCnt:             1,
				ArtifactsSizeAverage: 42.,
			},
		},
		{
			name: "generate summary for list with multiple elements",
			args: args{
				key: testhelper.TestDateDay1,
				entries: []JobEntry{
					{
						Status:        StatusSuccess,
						StartedAt:     testhelper.TestDateDay1,
						Duration:      100.0,
						ArtifactsSize: 10.,
					},
					{
						Status:        StatusSuccess,
						StartedAt:     testhelper.TestDateDay1,
						Duration:      50.0,
						ArtifactsSize: 20.,
					},
					{
						Status:        StatusFailed,
						StartedAt:     testhelper.TestDateDay1,
						Duration:      20.0,
						ArtifactsSize: 30.,
					},
				},
			},
			want: Summary{
				Date:                 testhelper.TestDateDay1,
				Min:                  50.0,
				Max:                  100.0,
				Average:              75.0,
				SuccessCnt:           2,
				TotalCnt:             3,
				ArtifactsSizeAverage: 15.,
			},
		},
		{
			name: "generate summary for list with multiple elements all failed",
			args: args{
				key: testhelper.TestDateDay1,
				entries: []JobEntry{
					{
						Status:    StatusFailed,
						StartedAt: testhelper.TestDateDay1,
						Duration:  100.0,
					},
					{
						Status:    StatusFailed,
						StartedAt: testhelper.TestDateDay1,
						Duration:  50.0,
					},
					{
						Status:    StatusFailed,
						StartedAt: testhelper.TestDateDay1,
						Duration:  20.0,
					},
				},
			},
			want: Summary{
				Date:                 testhelper.TestDateDay1,
				Min:                  0.0,
				Max:                  0.0,
				Average:              0.0,
				SuccessCnt:           0,
				TotalCnt:             3,
				ArtifactsSizeAverage: 0.0,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := getSummary(tt.args.key, tt.args.entries)
			testhelper.DeepEqual(t, got, tt.want)
		})
	}
}

func Test_mapJobEntriesIntoDayBuckets(t *testing.T) {
	type args struct {
		jobs  []JobEntry
		after time.Time
	}
	tests := []struct {
		name string
		args args
		want map[time.Time][]JobEntry
	}{
		{
			name: "test with empty list",
			args: args{
				jobs:  []JobEntry{},
				after: testhelper.TestDateDay1,
			},
			want: map[time.Time][]JobEntry{},
		},
		{
			name: "test with all elements before the after date",
			args: args{
				jobs: []JobEntry{
					{StartedAt: testhelper.TestDateDay1.Add(1 * time.Hour)},
					{StartedAt: testhelper.TestDateDay1.Add(2 * time.Hour)},
					{StartedAt: testhelper.TestDateDay1.Add(3 * time.Hour)},
				},
				after: testhelper.TestDateDay1,
			},
			want: map[time.Time][]JobEntry{},
		},
		{
			name: "test with all elements before and after the after date",
			args: args{
				jobs: []JobEntry{
					{StartedAt: testhelper.TestDateDay1.Add(1 * time.Hour)},
					{StartedAt: testhelper.TestDateDay1.Add(2 * time.Hour)},
					{StartedAt: testhelper.TestDateDay1.Add(3 * time.Hour)},
					{StartedAt: testhelper.TestDateDay2.Add(1 * time.Hour)},
					{StartedAt: testhelper.TestDateDay2.Add(2 * time.Hour)},
					{StartedAt: testhelper.TestDateDay2.Add(3 * time.Hour)},
					{StartedAt: testhelper.TestDateDay3.Add(1 * time.Hour)},
					{StartedAt: testhelper.TestDateDay3.Add(2 * time.Hour)},
					{StartedAt: testhelper.TestDateDay3.Add(3 * time.Hour)},
				},
				after: testhelper.TestDateDay1,
			},
			want: map[time.Time][]JobEntry{
				testhelper.TestDateDay2: {
					{StartedAt: testhelper.TestDateDay2.Add(1 * time.Hour)},
					{StartedAt: testhelper.TestDateDay2.Add(2 * time.Hour)},
					{StartedAt: testhelper.TestDateDay2.Add(3 * time.Hour)},
				},
				testhelper.TestDateDay3: {
					{StartedAt: testhelper.TestDateDay3.Add(1 * time.Hour)},
					{StartedAt: testhelper.TestDateDay3.Add(2 * time.Hour)},
					{StartedAt: testhelper.TestDateDay3.Add(3 * time.Hour)},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := mapJobEntriesIntoDayBuckets(tt.args.jobs, tt.args.after)
			testhelper.DeepEqual(t, got, tt.want)
		})
	}
}

func TestJob_generateDaySummaries(t *testing.T) {
	type fields struct {
		RawData      []JobEntry
		DaySummaries []Summary
	}
	tests := []struct {
		name   string
		fields fields
		expect fields
	}{
		{
			name: "generate day summaries for empty raw and day list",
			fields: fields{
				RawData:      []JobEntry{},
				DaySummaries: []Summary{},
			},
			expect: fields{
				RawData:      []JobEntry{},
				DaySummaries: []Summary{},
			},
		},
		{
			name: "generate day summaries for raw list with empty day list",
			fields: fields{
				RawData: []JobEntry{
					{StartedAt: testhelper.TestDateDay1.Add(3 * time.Hour), Duration: 100.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateDay1.Add(2 * time.Hour), Duration: 50.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateDay1.Add(1 * time.Hour), Duration: 100.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateDay1.Add(5 * time.Hour), Duration: 50.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateDay2.Add(3 * time.Hour), Duration: 10.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateDay2.Add(2 * time.Hour), Duration: 10.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateDay2.Add(1 * time.Hour), Duration: 10.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateDay2.Add(5 * time.Hour), Duration: 7.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateDay3.Add(3 * time.Hour), Duration: 1000.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateDay3.Add(2 * time.Hour), Duration: 500.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateDay3.Add(1 * time.Hour), Duration: 1000.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateDay3.Add(5 * time.Hour), Duration: 500.0, Status: "SUCCESS"},
				},
				DaySummaries: []Summary{},
			},
			expect: fields{
				RawData: []JobEntry{
					{StartedAt: testhelper.TestDateDay1.Add(1 * time.Hour), Duration: 100.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateDay1.Add(2 * time.Hour), Duration: 50.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateDay1.Add(3 * time.Hour), Duration: 100.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateDay1.Add(5 * time.Hour), Duration: 50.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateDay2.Add(1 * time.Hour), Duration: 10.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateDay2.Add(2 * time.Hour), Duration: 10.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateDay2.Add(3 * time.Hour), Duration: 10.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateDay2.Add(5 * time.Hour), Duration: 7.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateDay3.Add(1 * time.Hour), Duration: 1000.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateDay3.Add(2 * time.Hour), Duration: 500.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateDay3.Add(3 * time.Hour), Duration: 1000.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateDay3.Add(5 * time.Hour), Duration: 500.0, Status: "SUCCESS"},
				},
				DaySummaries: []Summary{
					{Date: testhelper.TestDateDay1, Min: 50, Max: 100, Average: 75, SuccessCnt: 4, TotalCnt: 4},
					{Date: testhelper.TestDateDay2, Min: 7, Max: 10, Average: 9, SuccessCnt: 3, TotalCnt: 4},
					{Date: testhelper.TestDateDay3, Min: 500, Max: 1000, Average: 750, SuccessCnt: 2, TotalCnt: 4},
				},
			},
		},
		{
			name: "generate day summaries for raw list with previous elements within day list",
			fields: fields{
				RawData: []JobEntry{
					{StartedAt: testhelper.TestDateDay1.Add(3 * time.Hour), Duration: 100.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateDay1.Add(2 * time.Hour), Duration: 50.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateDay1.Add(1 * time.Hour), Duration: 100.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateDay1.Add(5 * time.Hour), Duration: 50.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateDay2.Add(3 * time.Hour), Duration: 10.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateDay2.Add(2 * time.Hour), Duration: 10.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateDay2.Add(1 * time.Hour), Duration: 10.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateDay2.Add(5 * time.Hour), Duration: 7.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateDay3.Add(3 * time.Hour), Duration: 1000.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateDay3.Add(2 * time.Hour), Duration: 500.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateDay3.Add(1 * time.Hour), Duration: 1000.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateDay3.Add(5 * time.Hour), Duration: 500.0, Status: "SUCCESS"},
				},
				DaySummaries: []Summary{
					{Date: testhelper.TestDateDay1, Min: 1.0, Max: 100.8, Average: 8.9, SuccessCnt: 2, TotalCnt: 3},
				},
			},
			expect: fields{
				RawData: []JobEntry{
					{StartedAt: testhelper.TestDateDay1.Add(1 * time.Hour), Duration: 100.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateDay1.Add(2 * time.Hour), Duration: 50.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateDay1.Add(3 * time.Hour), Duration: 100.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateDay1.Add(5 * time.Hour), Duration: 50.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateDay2.Add(1 * time.Hour), Duration: 10.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateDay2.Add(2 * time.Hour), Duration: 10.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateDay2.Add(3 * time.Hour), Duration: 10.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateDay2.Add(5 * time.Hour), Duration: 7.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateDay3.Add(1 * time.Hour), Duration: 1000.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateDay3.Add(2 * time.Hour), Duration: 500.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateDay3.Add(3 * time.Hour), Duration: 1000.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateDay3.Add(5 * time.Hour), Duration: 500.0, Status: "SUCCESS"},
				},
				DaySummaries: []Summary{
					{Date: testhelper.TestDateDay1, Min: 1.0, Max: 100.8, Average: 8.9, SuccessCnt: 2, TotalCnt: 3},
					{Date: testhelper.TestDateDay2, Min: 7, Max: 10, Average: 9, SuccessCnt: 3, TotalCnt: 4},
					{Date: testhelper.TestDateDay3, Min: 500, Max: 1000, Average: 750, SuccessCnt: 2, TotalCnt: 4},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			j := &Job{
				RawData:        tt.fields.RawData,
				DaySummaries:   tt.fields.DaySummaries,
				WeekSummaries:  []Summary{},
				MonthSummaries: []Summary{},
			}
			j.generateDaySummaries()
			testhelper.DeepEqual(t, j.RawData, tt.expect.RawData)
			testhelper.DeepEqual(t, j.DaySummaries, tt.expect.DaySummaries)
		})
	}
}

func Test_getMondayDate(t *testing.T) {
	type args struct {
		d time.Time
	}
	tests := []struct {
		name string
		args args
		want time.Time
	}{
		{
			name: "get monday for a regular date",
			args: args{
				time.Date(2023, 8, 3, 12, 22, 0, 0, time.UTC),
			},
			want: time.Date(2023, 7, 31, 0, 0, 0, 0, time.UTC),
		},
		{
			name: "get monday for 1 week of 2008 (2007-12-31)",
			args: args{
				time.Date(2008, 1, 3, 12, 22, 0, 0, time.UTC),
			},
			want: time.Date(2007, 12, 31, 0, 0, 0, 0, time.UTC),
		},
		{
			name: "get monday for a monday",
			args: args{
				time.Date(2023, 7, 31, 12, 22, 0, 0, time.UTC),
			},
			want: time.Date(2023, 7, 31, 0, 0, 0, 0, time.UTC),
		},
		{
			name: "get monday for a monday time 0:0:0",
			args: args{
				time.Date(2023, 7, 31, 0, 0, 0, 0, time.UTC),
			},
			want: time.Date(2023, 7, 31, 0, 0, 0, 0, time.UTC),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := getMondayDate(tt.args.d)
			testhelper.DeepEqual(t, got, tt.want)
		})
	}
}

func Test_mapJobEntriesIntoWeekBuckets(t *testing.T) {
	type args struct {
		jobs      []JobEntry
		afterYear int
		afterWeek int
	}
	tests := []struct {
		name string
		args args
		want map[time.Time][]JobEntry
	}{
		{
			name: "empty list",
			args: args{
				jobs:      []JobEntry{},
				afterYear: 0,
				afterWeek: 0,
			},
			want: map[time.Time][]JobEntry{},
		},
		{
			name: "some new job entries",
			args: args{
				jobs: []JobEntry{
					{
						StartedAt: testhelper.TestDateWeek2,
						Duration:  30,
					},
					{
						StartedAt: testhelper.TestDateWeek2.AddDate(0, 0, 1),
						Duration:  40,
					},
					{
						StartedAt: testhelper.TestDateWeek1,
						Duration:  10,
					},
					{
						StartedAt: testhelper.TestDateWeek1.AddDate(0, 0, 1),
						Duration:  20,
					},
					{
						StartedAt: testhelper.TestDateWeek3,
						Duration:  50,
					},
					{
						StartedAt: testhelper.TestDateWeek3.AddDate(0, 0, 1),
						Duration:  60,
					},
				},
				afterWeek: 47,
				afterYear: 2022,
			},
			want: map[time.Time][]JobEntry{
				testhelper.TestDateWeek2: {
					{
						StartedAt: testhelper.TestDateWeek2,
						Duration:  30,
					},
					{
						StartedAt: testhelper.TestDateWeek2.AddDate(0, 0, 1),
						Duration:  40,
					},
				},
				testhelper.TestDateWeek3: {
					{
						StartedAt: testhelper.TestDateWeek3,
						Duration:  50,
					},
					{
						StartedAt: testhelper.TestDateWeek3.AddDate(0, 0, 1),
						Duration:  60,
					},
				},
			},
		},
		{
			name: "skip job entries because of the year",
			args: args{
				jobs: []JobEntry{
					{
						StartedAt: testhelper.TestDateWeek2,
						Duration:  30,
					},
					{
						StartedAt: testhelper.TestDateWeek2.AddDate(0, 0, 1),
						Duration:  40,
					},
					{
						StartedAt: testhelper.TestDateWeek1,
						Duration:  10,
					},
					{
						StartedAt: testhelper.TestDateWeek1.AddDate(0, 0, 1),
						Duration:  20,
					},
					{
						StartedAt: testhelper.TestDateWeek3,
						Duration:  50,
					},
					{
						StartedAt: testhelper.TestDateWeek3.AddDate(0, 0, 1),
						Duration:  60,
					},
				},
				afterWeek: 1,
				afterYear: 2023,
			},
			want: map[time.Time][]JobEntry{},
		},
		{
			name: "do not skip because of week if year is in the past",
			args: args{
				jobs: []JobEntry{
					{
						StartedAt: testhelper.TestDateWeek2,
						Duration:  30,
					},
					{
						StartedAt: testhelper.TestDateWeek2.AddDate(0, 0, 1),
						Duration:  40,
					},
					{
						StartedAt: testhelper.TestDateWeek3,
						Duration:  50,
					},
					{
						StartedAt: testhelper.TestDateWeek3.AddDate(0, 0, 1),
						Duration:  60,
					},
				},
				afterWeek: 51,
				afterYear: 2021,
			},
			want: map[time.Time][]JobEntry{
				testhelper.TestDateWeek2: {
					{
						StartedAt: testhelper.TestDateWeek2,
						Duration:  30,
					},
					{
						StartedAt: testhelper.TestDateWeek2.AddDate(0, 0, 1),
						Duration:  40,
					},
				},
				testhelper.TestDateWeek3: {
					{
						StartedAt: testhelper.TestDateWeek3,
						Duration:  50,
					},
					{
						StartedAt: testhelper.TestDateWeek3.AddDate(0, 0, 1),
						Duration:  60,
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := mapJobEntriesIntoWeekBuckets(tt.args.jobs, tt.args.afterYear, tt.args.afterWeek)
			testhelper.DeepEqual(t, got, tt.want)
		})
	}
}

func TestJob_generateWeekSummaries(t *testing.T) {
	type fields struct {
		RawData       []JobEntry
		WeekSummaries []Summary
	}
	tests := []struct {
		name   string
		fields fields
		expect fields
	}{
		{
			name: "empty lists",
			fields: fields{
				RawData:       []JobEntry{},
				WeekSummaries: []Summary{},
			},
			expect: fields{
				RawData:       []JobEntry{},
				WeekSummaries: []Summary{},
			},
		},
		{
			name: "generate week summaries for raw list with empty week list",
			fields: fields{
				RawData: []JobEntry{
					{StartedAt: testhelper.TestDateWeek1.Add(3 * time.Hour), Duration: 100.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek1.Add(2 * time.Hour), Duration: 50.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek1.Add(1 * time.Hour), Duration: 100.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek1.Add(5 * time.Hour), Duration: 50.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek2.Add(3 * time.Hour), Duration: 10.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateWeek2.Add(2 * time.Hour), Duration: 10.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek2.Add(1 * time.Hour), Duration: 10.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek2.Add(5 * time.Hour), Duration: 7.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek3.Add(3 * time.Hour), Duration: 1000.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateWeek3.Add(2 * time.Hour), Duration: 500.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateWeek3.Add(1 * time.Hour), Duration: 1000.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek3.Add(5 * time.Hour), Duration: 500.0, Status: "SUCCESS"},
				},
				WeekSummaries: []Summary{},
			},
			expect: fields{
				RawData: []JobEntry{
					{StartedAt: testhelper.TestDateWeek1.Add(1 * time.Hour), Duration: 100.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek1.Add(2 * time.Hour), Duration: 50.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek1.Add(3 * time.Hour), Duration: 100.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek1.Add(5 * time.Hour), Duration: 50.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek2.Add(1 * time.Hour), Duration: 10.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek2.Add(2 * time.Hour), Duration: 10.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek2.Add(3 * time.Hour), Duration: 10.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateWeek2.Add(5 * time.Hour), Duration: 7.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek3.Add(1 * time.Hour), Duration: 1000.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek3.Add(2 * time.Hour), Duration: 500.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateWeek3.Add(3 * time.Hour), Duration: 1000.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateWeek3.Add(5 * time.Hour), Duration: 500.0, Status: "SUCCESS"},
				},
				WeekSummaries: []Summary{
					{Date: testhelper.TestDateWeek1, Min: 50, Max: 100, Average: 75, SuccessCnt: 4, TotalCnt: 4},
					{Date: testhelper.TestDateWeek2, Min: 7, Max: 10, Average: 9, SuccessCnt: 3, TotalCnt: 4},
					{Date: testhelper.TestDateWeek3, Min: 500, Max: 1000, Average: 750, SuccessCnt: 2, TotalCnt: 4},
				},
			},
		},
		{
			name: "generate week summaries for raw list with previous elements within week list",
			fields: fields{
				RawData: []JobEntry{
					{StartedAt: testhelper.TestDateWeek1.Add(3 * time.Hour), Duration: 100.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek1.Add(2 * time.Hour), Duration: 50.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek1.Add(1 * time.Hour), Duration: 100.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek1.Add(5 * time.Hour), Duration: 50.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek2.Add(3 * time.Hour), Duration: 10.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateWeek2.Add(2 * time.Hour), Duration: 10.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek2.Add(1 * time.Hour), Duration: 10.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek2.Add(5 * time.Hour), Duration: 7.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek3.Add(3 * time.Hour), Duration: 1000.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateWeek3.Add(2 * time.Hour), Duration: 500.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateWeek3.Add(1 * time.Hour), Duration: 1000.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek3.Add(5 * time.Hour), Duration: 500.0, Status: "SUCCESS"},
				},
				WeekSummaries: []Summary{
					{Date: testhelper.TestDateWeek1, Min: 1.0, Max: 100.8, Average: 8.9, SuccessCnt: 2, TotalCnt: 3},
				},
			},
			expect: fields{
				RawData: []JobEntry{
					{StartedAt: testhelper.TestDateWeek1.Add(1 * time.Hour), Duration: 100.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek1.Add(2 * time.Hour), Duration: 50.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek1.Add(3 * time.Hour), Duration: 100.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek1.Add(5 * time.Hour), Duration: 50.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek2.Add(1 * time.Hour), Duration: 10.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek2.Add(2 * time.Hour), Duration: 10.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek2.Add(3 * time.Hour), Duration: 10.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateWeek2.Add(5 * time.Hour), Duration: 7.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek3.Add(1 * time.Hour), Duration: 1000.0, Status: "SUCCESS"},
					{StartedAt: testhelper.TestDateWeek3.Add(2 * time.Hour), Duration: 500.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateWeek3.Add(3 * time.Hour), Duration: 1000.0, Status: "FAILED"},
					{StartedAt: testhelper.TestDateWeek3.Add(5 * time.Hour), Duration: 500.0, Status: "SUCCESS"},
				},
				WeekSummaries: []Summary{
					{Date: testhelper.TestDateWeek1, Min: 1.0, Max: 100.8, Average: 8.9, SuccessCnt: 2, TotalCnt: 3},
					{Date: testhelper.TestDateWeek2, Min: 7, Max: 10, Average: 9, SuccessCnt: 3, TotalCnt: 4},
					{Date: testhelper.TestDateWeek3, Min: 500, Max: 1000, Average: 750, SuccessCnt: 2, TotalCnt: 4},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			j := &Job{
				RawData:        tt.fields.RawData,
				DaySummaries:   []Summary{},
				WeekSummaries:  tt.fields.WeekSummaries,
				MonthSummaries: []Summary{},
			}
			j.generateWeekSummaries()
			testhelper.DeepEqual(t, j.RawData, tt.expect.RawData)
			testhelper.DeepEqual(t, j.WeekSummaries, tt.expect.WeekSummaries)
		})
	}
}

func TestJobEntry_ParseLog(t *testing.T) {
	type fields struct {
		BuildSections []BuildSection
	}
	type args struct {
		log string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		expect []BuildSection
	}{
		{
			name: "empty log and empty sections",
			fields: fields{
				BuildSections: []BuildSection{},
			},
			args: args{
				log: "",
			},
			expect: []BuildSection{},
		},
		{
			name: "empty log does not change build sections",
			fields: fields{
				BuildSections: []BuildSection{
					{Title: "demo", Start: testhelper.TestDateDay1, End: testhelper.TestDateDay2},
					{Title: "demo2", Start: testhelper.TestDateDay2, End: testhelper.TestDateDay3},
				},
			},
			args: args{
				log: "",
			},
			expect: []BuildSection{
				{Title: "demo", Start: testhelper.TestDateDay1, End: testhelper.TestDateDay2},
				{Title: "demo2", Start: testhelper.TestDateDay2, End: testhelper.TestDateDay3},
			},
		},
		{
			name: "log with two valid start/end tags and not empty initial sections",
			fields: fields{
				BuildSections: []BuildSection{
					{Title: "demo", Start: testhelper.TestDateDay1, End: testhelper.TestDateDay2},
					{Title: "demo2", Start: testhelper.TestDateDay2, End: testhelper.TestDateDay3},
				},
			},
			args: args{
				log: `
				section_start:1:first-section
				RANDOM DATA
				section_end:2:first-section
				section_start:21:second-section
				RANDOM DATA
				section_end:22:second-section
				`,
			},
			expect: []BuildSection{
				{Title: "first-section", Start: time.Unix(1, 0), End: time.Unix(2, 0)},
				{Title: "second-section", Start: time.Unix(21, 0), End: time.Unix(22, 0)},
			},
		},
		{
			name: "log with two valid start/end tags and empty initial sections",
			fields: fields{
				BuildSections: []BuildSection{},
			},
			args: args{
				log: `
				section_start:1:first-section
				RANDOM DATA
				section_end:2:first-section
				section_start:21:second-section
				RANDOM DATA
				section_end:22:second-section
				`,
			},
			expect: []BuildSection{
				{Title: "first-section", Start: time.Unix(1, 0), End: time.Unix(2, 0)},
				{Title: "second-section", Start: time.Unix(21, 0), End: time.Unix(22, 0)},
			},
		},
		{
			name: "log with missing start/end tags and empty initial sections",
			fields: fields{
				BuildSections: []BuildSection{},
			},
			args: args{
				log: `
				section_start:1:first-section
				RANDOM DATA
				RANDOM DATA
				section_end:22:second-section
				`,
			},
			expect: []BuildSection{
				{Title: "first-section", Start: time.Unix(1, 0), End: time.Unix(1, 0)},
				{Title: "second-section", Start: time.Unix(22, 0), End: time.Unix(22, 0)},
			},
		},
		{
			name: "log with overlapping start/end tags and empty initial sections",
			fields: fields{
				BuildSections: []BuildSection{},
			},
			args: args{
				log: `
				section_start:1:first-section
				RANDOM DATA
				section_start:21:second-section
				section_end:2:first-section
				RANDOM DATA
				section_end:22:second-section
				`,
			},
			expect: []BuildSection{
				{Title: "first-section", Start: time.Unix(1, 0), End: time.Unix(2, 0)},
				{Title: "second-section", Start: time.Unix(21, 0), End: time.Unix(22, 0)},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			je := &JobEntry{
				BuildSections: tt.fields.BuildSections,
			}
			je.ParseLog(tt.args.log)
			sort.SliceStable(je.BuildSections, func(i, j int) bool {
				return je.BuildSections[i].Start.Before(je.BuildSections[j].Start)
			})
			testhelper.DeepEqual(t, je.BuildSections, tt.expect)
		})
	}
}

func TestJob_generateMonthSummaries(t *testing.T) {
	curMonth := time.Date(time.Now().Year(), time.Now().Month(), 1, 0, 0, 0, 0, time.UTC)
	prevMonth := time.Date(time.Now().Year(), time.Now().Month()-1, 1, 0, 0, 0, 0, time.UTC)

	type fields struct {
		DaySummaries   []Summary
		MonthSummaries []Summary
	}
	tests := []struct {
		name   string
		fields fields
		expect fields
	}{
		{
			name: "empty day summaries create empty month entry",
			fields: fields{
				DaySummaries:   []Summary{},
				MonthSummaries: []Summary{},
			},
			expect: fields{
				DaySummaries: []Summary{},
				MonthSummaries: []Summary{
					{
						Date:           prevMonth,
						Min:            0,
						Max:            0,
						Average:        0,
						SuccessCnt:     0,
						TotalCnt:       0,
						Coverage:       0,
						TestCnt:        0,
						TestSuccessCnt: 0,
					},
				},
			},
		},
		{
			name: "empty day summaries with predefined month summary (no change)",
			fields: fields{
				DaySummaries: []Summary{},
				MonthSummaries: []Summary{
					{
						Date:           prevMonth,
						Min:            10,
						Max:            20,
						Average:        12,
						SuccessCnt:     10,
						TotalCnt:       10,
						Coverage:       20,
						TestCnt:        10,
						TestSuccessCnt: 10,
					},
				},
			},
			expect: fields{
				DaySummaries: []Summary{},
				MonthSummaries: []Summary{
					{
						Date:           prevMonth,
						Min:            10,
						Max:            20,
						Average:        12,
						SuccessCnt:     10,
						TotalCnt:       10,
						Coverage:       20,
						TestCnt:        10,
						TestSuccessCnt: 10,
					},
				},
			},
		},
		{
			name: "day summaries with no month summary",
			fields: fields{
				DaySummaries: []Summary{
					{
						Date:           curMonth.AddDate(0, 0, -10),
						Min:            15,
						Max:            35,
						Average:        20,
						SuccessCnt:     5,
						TotalCnt:       3,
						Coverage:       30,
						TestCnt:        12,
						TestSuccessCnt: 12,
					},
					{
						Date:       curMonth.AddDate(0, 0, 10),
						Min:        105,
						Max:        305,
						Average:    200,
						SuccessCnt: 50,
						TotalCnt:   32,
					},
					{
						Date:       curMonth.AddDate(0, -1, -10),
						Min:        105,
						Max:        305,
						Average:    200,
						SuccessCnt: 50,
						TotalCnt:   32,
					},
				},
				MonthSummaries: []Summary{},
			},
			expect: fields{
				DaySummaries: []Summary{
					{
						Date:           curMonth.AddDate(0, 0, -10),
						Min:            15,
						Max:            35,
						Average:        20,
						SuccessCnt:     5,
						TotalCnt:       3,
						Coverage:       30,
						TestCnt:        12,
						TestSuccessCnt: 12,
					},
					{
						Date:       curMonth.AddDate(0, 0, 10),
						Min:        105,
						Max:        305,
						Average:    200,
						SuccessCnt: 50,
						TotalCnt:   32,
					},
					{
						Date:       curMonth.AddDate(0, -1, -10),
						Min:        105,
						Max:        305,
						Average:    200,
						SuccessCnt: 50,
						TotalCnt:   32,
					},
				},
				MonthSummaries: []Summary{
					{
						Date:           prevMonth,
						Min:            15,
						Max:            35,
						Average:        20,
						SuccessCnt:     5,
						TotalCnt:       3,
						Coverage:       30,
						TestCnt:        12,
						TestSuccessCnt: 12,
					},
				},
			},
		},
		{
			name: "day summaries with previous month summaries",
			fields: fields{
				DaySummaries: []Summary{
					{
						Date:           curMonth.AddDate(0, 0, -10),
						Min:            15,
						Max:            35,
						Average:        20,
						SuccessCnt:     5,
						TotalCnt:       3,
						Coverage:       30,
						TestCnt:        12,
						TestSuccessCnt: 12,
					},
					{
						Date:       curMonth.AddDate(0, 0, 10),
						Min:        105,
						Max:        305,
						Average:    200,
						SuccessCnt: 50,
						TotalCnt:   32,
					},
					{
						Date:       curMonth.AddDate(0, -1, -10),
						Min:        105,
						Max:        305,
						Average:    200,
						SuccessCnt: 50,
						TotalCnt:   32,
					},
				},
				MonthSummaries: []Summary{
					{
						Date:           prevMonth.AddDate(0, -1, 0),
						Min:            150,
						Max:            350,
						Average:        200,
						SuccessCnt:     50,
						TotalCnt:       30,
						Coverage:       300,
						TestCnt:        120,
						TestSuccessCnt: 120,
					},
				},
			},
			expect: fields{
				DaySummaries: []Summary{
					{
						Date:           curMonth.AddDate(0, 0, -10),
						Min:            15,
						Max:            35,
						Average:        20,
						SuccessCnt:     5,
						TotalCnt:       3,
						Coverage:       30,
						TestCnt:        12,
						TestSuccessCnt: 12,
					},
					{
						Date:       curMonth.AddDate(0, 0, 10),
						Min:        105,
						Max:        305,
						Average:    200,
						SuccessCnt: 50,
						TotalCnt:   32,
					},
					{
						Date:       curMonth.AddDate(0, -1, -10),
						Min:        105,
						Max:        305,
						Average:    200,
						SuccessCnt: 50,
						TotalCnt:   32,
					},
				},
				MonthSummaries: []Summary{
					{
						Date:           prevMonth.AddDate(0, -1, 0),
						Min:            150,
						Max:            350,
						Average:        200,
						SuccessCnt:     50,
						TotalCnt:       30,
						Coverage:       300,
						TestCnt:        120,
						TestSuccessCnt: 120,
					},
					{
						Date:           prevMonth,
						Min:            15,
						Max:            35,
						Average:        20,
						SuccessCnt:     5,
						TotalCnt:       3,
						Coverage:       30,
						TestCnt:        12,
						TestSuccessCnt: 12,
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			j := &Job{
				RawData:        []JobEntry{},
				DaySummaries:   tt.fields.DaySummaries,
				WeekSummaries:  []Summary{},
				MonthSummaries: tt.fields.MonthSummaries,
			}
			j.generateMonthSummaries()
			testhelper.DeepEqual(t, j.DaySummaries, tt.expect.DaySummaries)
			testhelper.DeepEqual(t, j.MonthSummaries, tt.expect.MonthSummaries)
		})
	}
}
