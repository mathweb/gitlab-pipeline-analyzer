// Copyright: (c) 2023 Mathias Weber <mweb@gmx.ch>
//
// SPDX-License-Identifier: MIT

package data

import (
	"fmt"
	"math"
	"regexp"
	"sort"
	"strconv"
	"time"
)

const (
	StatusSuccess = "SUCCESS"
	StatusFailed  = "FAILED"
)

type TestReport struct {
	Duration float64 `json:"duration"`
	Count    int64   `json:"count"`
	Success  int64   `json:"success"`
	Failed   int64   `json:"failed"`
	Skipped  int64   `json:"skipped"`
	Error    int64   `json:"error"`
}

type BuildSection struct {
	Start time.Time `json:"start"`
	End   time.Time `json:"stop"`
	Title string    `json:"title"`
}

type JobEntry struct {
	ID             int         `json:"id"`
	Ref            string      `json:"ref"`
	Status         string      `json:"status"`
	Name           string      `json:"name"`
	StartedAt      time.Time   `json:"startedAt"`
	Duration       float64     `json:"duration"`
	Coverage       float64     `json:"coverage"`
	Retried        bool        `json:"retried"`
	FailureReason  string      `json:"failureReason"`
	QueuedDuration float64     `json:"queuedDuration"`
	WebURL         string      `json:"webUrl"`
	RunnerName     string      `json:"runner"`
	PipelineID     int         `json:"pipelineId"`
	TestReport     *TestReport `json:"testReport"`
	LogWebURL      string      `json:"logWebUrl"`
	ArtifactsSize  float64     `json:"artifactsSize"`

	BuildSections []BuildSection `json:"buildSections"`
}

type Job struct {
	RawData        []JobEntry `json:"rawData"`
	DaySummaries   []Summary  `json:"daySummaries"`
	WeekSummaries  []Summary  `json:"weekSummaries"`
	MonthSummaries []Summary  `json:"monthSummaries"`
}

type jobTraceFetcher interface {
	GetJobTrace(jobID int) (string, error)
	IgnoreJobTrace() bool
}

func (j *Job) AddEntry(job JobEntry, f jobTraceFetcher) {
	// this does not use a cache for speedup since it is expected to be in a
	// reasonable size < few thousand elements
	for _, d := range j.RawData {
		if d.ID == job.ID {
			// skip already in there no update of data supported
			return
		}
	}
	if job.LogWebURL != "" && !f.IgnoreJobTrace() {
		log, err := f.GetJobTrace(job.ID)
		if err != nil {
			fmt.Printf("WARNING: Got error during fetch of log: %v\n", err)
			fmt.Printf("WARNING: Ignore log for Job: %s\n", job.WebURL)
		}
		job.ParseLog(log)
	}

	j.RawData = append(j.RawData, job)
}

func (j *Job) CleanUp(maxRawElements, maxDayElements, maxWeekElements, maxMonthElements int) {
	j.sortRawData()
	if len(j.RawData) > maxRawElements {
		j.RawData = j.RawData[len(j.RawData)-maxRawElements:]
	}
	SortSummaries(j.DaySummaries)
	if len(j.DaySummaries) > maxDayElements {
		j.DaySummaries = j.DaySummaries[len(j.DaySummaries)-maxDayElements:]
	}
	SortSummaries(j.WeekSummaries)
	if len(j.WeekSummaries) > maxWeekElements {
		j.WeekSummaries = j.WeekSummaries[len(j.WeekSummaries)-maxWeekElements:]
	}
	SortSummaries(j.MonthSummaries)
	if len(j.MonthSummaries) > maxMonthElements {
		j.MonthSummaries = j.MonthSummaries[len(j.MonthSummaries)-maxMonthElements:]
	}
}

func (j *Job) UpdateSummaries() {
	j.generateDaySummaries()
	j.generateWeekSummaries()
	j.generateMonthSummaries()
}

// generateDaySummaries collects the data for one week and generates the
// summary data. Each bucket holds the day as the sorting date.
func (j *Job) generateDaySummaries() {
	lastDay := time.Unix(0, 0)
	j.sortRawData()
	SortSummaries(j.DaySummaries)
	if len(j.DaySummaries) > 0 {
		lastDay = j.DaySummaries[len(j.DaySummaries)-1].Date
	}

	days := mapJobEntriesIntoDayBuckets(j.RawData, lastDay)

	for key := range days {
		j.DaySummaries = append(j.DaySummaries, getSummary(key, days[key]))
	}
	SortSummaries(j.DaySummaries)
}

// generateWeekSummaries collects the data for one week and generates the
// summary data. Each bucket holds the monday as the sorting date.
func (j *Job) generateWeekSummaries() {
	lastYear := 0
	lastWeek := 0
	j.sortRawData()
	SortSummaries(j.WeekSummaries)
	if len(j.WeekSummaries) > 0 {
		lastYear, lastWeek = j.WeekSummaries[len(j.WeekSummaries)-1].Date.ISOWeek()
	}
	weeks := mapJobEntriesIntoWeekBuckets(j.RawData, lastYear, lastWeek)
	for key := range weeks {
		j.WeekSummaries = append(j.WeekSummaries, getSummary(key, weeks[key]))
	}
	SortSummaries(j.WeekSummaries)
}

func (j *Job) generateMonthSummaries() {
	baseDate := time.Now().AddDate(0, -1, 0)

	if len(j.MonthSummaries) > 0 {
		lastDate := j.MonthSummaries[len(j.MonthSummaries)-1].Date
		if lastDate.Month() == baseDate.Month() && lastDate.Year() == baseDate.Year() {
			// skip since already generated
			return
		}
	}

	j.MonthSummaries = append(j.MonthSummaries, generateMonthlySummaries(j.DaySummaries, baseDate.Month(), baseDate.Year()))
}

func (j *Job) sortRawData() {
	sort.Slice(j.RawData, func(i, k int) bool {
		return j.RawData[i].StartedAt.Before(j.RawData[k].StartedAt)
	})
}

// mapJobEntriesIntoDayBuckets maps each raw job entry to a day to accumulate the
// data for the summary.
func mapJobEntriesIntoDayBuckets(jobs []JobEntry, after time.Time) map[time.Time][]JobEntry {
	days := make(map[time.Time][]JobEntry)
	after = after.AddDate(0, 0, 1)
	for _, entry := range jobs {
		if entry.StartedAt.Before(after) {
			// skip already processed
			continue
		}
		y, m, d := entry.StartedAt.Date()
		dayEntry := time.Date(y, m, d, 0, 0, 0, 0, time.UTC)
		days[dayEntry] = append(days[dayEntry], entry)
	}
	return days
}

// mapJobEntriesIntoWeekBuckets maps each raw job entry to a week to accumulate
// the data for the summary. Only finished weeks are accumulated and as the
// sorting date the monday of the week is used.
func mapJobEntriesIntoWeekBuckets(jobs []JobEntry, afterYear int, afterWeek int) map[time.Time][]JobEntry {
	weeks := make(map[time.Time][]JobEntry)
	for _, entry := range jobs {
		y, w := entry.StartedAt.ISOWeek()
		if y < afterYear || y == afterYear && w <= afterWeek {
			// skip already processed
			continue
		}

		weekEntry := getMondayDate(entry.StartedAt)
		weeks[weekEntry] = append(weeks[weekEntry], entry)
	}
	return weeks
}

func getMondayDate(d time.Time) time.Time {
	for d.Weekday() != time.Monday {
		d = d.AddDate(0, 0, -1)
	}
	year, month, day := d.Date()
	return time.Date(year, month, day, 0, 0, 0, 0, time.UTC)
}

func getSummary(key time.Time, entries []JobEntry) Summary {
	min := math.Inf(1)
	max := 0.0
	totalDuration := 0.0
	totalCoverage := 0.0
	totalArtifactSize := 0.0

	var successFullCnt int64

	var testCnt int64
	var testSuccessCnt int64
	var testDataCnt int64
	for _, e := range entries {
		if e.Status == StatusSuccess {
			if e.Duration < min {
				min = e.Duration
			}
			if e.Duration > max {
				max = e.Duration
			}
			successFullCnt++
			totalDuration += e.Duration
			totalCoverage += e.Coverage
			totalArtifactSize += e.ArtifactsSize
			if e.TestReport != nil {
				testCnt += e.TestReport.Count
				testSuccessCnt += e.TestReport.Success
				testDataCnt++
			}
		}
	}

	durationAvg := 0.
	covAvg := 0.
	artifactsSizeAvg := 0.
	var testCntAvg int64
	var testSuccessCntAvg int64
	if successFullCnt > 0 {
		durationAvg = totalDuration / float64(successFullCnt)
		covAvg = totalCoverage / float64(successFullCnt)
		artifactsSizeAvg = totalArtifactSize / float64(successFullCnt)
	}
	if testDataCnt > 0 {
		testCntAvg = testCnt / testDataCnt
		testSuccessCntAvg = testSuccessCnt / testDataCnt
	}
	if math.IsInf(min, 1) {
		min = 0
	}
	return Summary{
		Date:                 key,
		Min:                  min,
		Max:                  max,
		Average:              durationAvg,
		SuccessCnt:           successFullCnt,
		TotalCnt:             int64(len(entries)),
		ArtifactsSizeAverage: artifactsSizeAvg,
		Coverage:             covAvg,
		// not available on job level
		TestCnt:        testCntAvg,
		TestSuccessCnt: testSuccessCntAvg,
	}
}

func (je *JobEntry) ParseLog(log string) {
	// See https://docs.gitlab.com/ee/ci/jobs/#custom-collapsible-sections A
	// collapsible log section always consists of a section_start and
	// section_end. Both can be identified by the section name, which is the
	// third part of the colon separated string.
	reStartPoints := regexp.MustCompile(`section_start:([0-9]+):([a-zA-Z_\-]+)`)
	reEndPoints := regexp.MustCompile(`section_end:([0-9]+):([a-zA-Z_\-]+)`)

	startMatches := reStartPoints.FindAllStringSubmatch(log, -1)
	endMatches := reEndPoints.FindAllStringSubmatch(log, -1)

	sections := make(map[string]*BuildSection, 0)

	for _, e := range startMatches {
		timeStamp, err := strconv.ParseInt(e[1], 10, 64)
		if err != nil {
			fmt.Printf("WARNING: Got an error during parsing timestamp for section_start: %s\n", e[0])
		}
		sections[e[2]] = &BuildSection{
			Title: e[2],
			Start: time.Unix(timeStamp, 0),
			End:   time.Unix(timeStamp, 0),
		}
	}

	for _, e := range endMatches {
		timeStamp, err := strconv.ParseInt(e[1], 10, 64)
		if err != nil {
			fmt.Printf("WARNING: Got an error during parsing timestamp for section_end: %s\n", e[0])
		}
		if _, ok := sections[e[2]]; !ok {
			sections[e[2]] = &BuildSection{
				Title: e[2],
				Start: time.Unix(timeStamp, 0),
				End:   time.Unix(timeStamp, 0),
			}
		} else {
			sections[e[2]].End = time.Unix(timeStamp, 0)
		}
	}

	if len(sections) > 0 {
		je.BuildSections = []BuildSection{}
	}
	for _, v := range sections {
		je.BuildSections = append(je.BuildSections, *v)
	}
}
